<?php
ob_start();
require_once('third_party/PHPExcel.php');
add_action('admin_menu', 'register_my_custom_menu_page');
function register_my_custom_menu_page()
{
    // add_submenu_page('testadminpage', 'Thêm Mới', 'Thêm Mới', 'manage_options', 'my-menu' );
    add_menu_page('Thành viên', 'Trưng bày', 'manage_options', 'chienbinhmori_image', 'members_options_pages', '', 1);
    add_menu_page('Thành viên', 'Kiến thức', 'manage_options', 'chienbinhmori_quiz', 'members_quiz_options_pages', '', 1);
}
/** functon action */
$action = isset($_GET['action']) ? $_GET['action'] : '';
if (isset($action) && $action != '') {
    if ($action == 'hideuser') {
        $user_id = isset($_GET['user_id']) ? $_GET['user_id'] : '';
        if ($user_id) {
            $wpdb->query('UPDATE mr_register_image SET status = 2 WHERE info_ID = ' . $user_id);
            wp_redirect(self_admin_url('admin.php?page=chienbinhmori_image&status=1'), 301);
            exit;
        }
    } else if ($action == 'acceptuser') {
        $user_id = isset($_GET['user_id']) ? $_GET['user_id'] : '';
        if ($user_id) {
            $wpdb->query('UPDATE mr_register_image SET status = 1 WHERE info_ID = ' . $user_id);
            wp_redirect(self_admin_url('admin.php?page=chienbinhmori_image&status=2'), 301);
            exit;
        }
    } else if ($action == 'exportexcel_image') {
        $phpexcel = new \PHPExcel();
        $phpexcel->getProperties()
            ->setCreator("The A Team Admin")
            ->setLastModifiedBy("tat")
            ->setTitle("Morinaga")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Morinaga")
            ->setKeywords("Morinaga")
            ->setCategory("Morinaga");
        // set header
        $phpexcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'STT')
            ->setCellValue('B1', 'Họ Tên')
            ->setCellValue('C1', 'Khu vực')
            ->setCellValue('D1', 'Đội')
            ->setCellValue('E1', 'Email')
            ->setCellValue('F1', 'Điện thoại')
            ->setCellValue('G1', 'Album');

        // set data cho column
        $users  =   $wpdb->get_results("select * from mr_register_info");
        foreach ($users as $k => $user) {
            $i = $k + 2;
            $list_album = '';
            $albums = $wpdb->get_results($wpdb->prepare("select * from mr_register_image where info_ID = %d", $user->id));
            foreach ($albums as $album) {
                $list_album = $list_album . "\n" . $album->album_name . ': ' . get_home_url() . '/album-detail/?album_id=' . $album->id;
            }
            $fullname = $user->user_name;
            $email = $user->email;
            $phone = $user->phone;
            $team_area_id = $user->team_area;
            $team_area = $user->area;
            $teamName = getTeamName($team_area_id, $team_area);
            $full_team = $teamName->team_area . ' - ' . $teamName->team;

            $phpexcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $k + 1)
                ->setCellValue('B' . $i, $fullname)
                ->setCellValue('C' . $i, $team_area == 1 ? 'Miền Bắc' : 'Miền Nam')
                ->setCellValue('D' . $i, $full_team)
                ->setCellValue('E' . $i, $email)
                ->setCellValue('F' . $i, "'" . $phone)
                ->setCellValue('G' . $i, $list_album);

            $phpexcel->getActiveSheet()->getStyle('G' . $i)
                ->getAlignment()
                ->setWrapText(true);
        }
        // set tên file
        $name = 'ds_mori_' . date('Y-m-d', time()) . '.xls';
        // set config
        $phpexcel->getActiveSheet()->setTitle($name);
        $phpexcel->setActiveSheetIndex(0);
        //Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $name . '"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($phpexcel, 'Excel5');
        //ob_end_clean();
        $objWriter->save('php://output');
        exit;
    } else if ($action == 'exportexcel_quiz') {
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $phpexcel = new \PHPExcel();
        $phpexcel->getProperties()
            ->setCreator("The A Team Admin")
            ->setLastModifiedBy("tat")
            ->setTitle("Morinaga")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Morinaga")
            ->setKeywords("Morinaga")
            ->setCategory("Morinaga");
        // set header
        $phpexcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'STT')
            ->setCellValue('B1', 'Họ Tên')
            ->setCellValue('C1', 'Chủ cửa hàng')
            ->setCellValue('D1', 'Nhà phân phối')
            ->setCellValue('E1', 'Đối tượng')
            ->setCellValue('F1', 'Khu Vực')
            ->setCellValue('G1', 'Đội')
            ->setCellValue('H1', 'Email')
            ->setCellValue('I1', 'Điện thoại')
            ->setCellValue('J1', 'Số câu đúng')
            ->setCellValue('K1', 'Thời gian hoàn thành');

        // set data cho column
        $users  =   $wpdb->get_results("SELECT mr_quiz_info.*,mr_quiz_user_contest.finish_time,mr_quiz_user_contest.total_correct 
        FROM mr_quiz_info LEFT JOIN mr_quiz_user_contest ON mr_quiz_info.id = mr_quiz_user_contest.user_id WHERE mr_quiz_info.status = 1");
        if ($type != null && $type != '' && $type == 'top_quiz') {
            $users  =   $wpdb->get_results("SELECT mr_quiz_info.*,mr_quiz_user_contest.finish_time,mr_quiz_user_contest.total_correct 
            FROM mr_quiz_info LEFT JOIN mr_quiz_user_contest ON mr_quiz_info.id = mr_quiz_user_contest.user_id 
            WHERE mr_quiz_info.status = 1 ORDER BY mr_quiz_user_contest.total_correct DESC, mr_quiz_user_contest.finish_time_format ASC LIMIT 0,20");
        }
        foreach ($users as $k => $user) {
            $i = $k + 2;
            $fullname = $user->quiz_name;
            $email = $user->quiz_email;
            $phone = $user->quiz_phone;
            $time_quiz = $user->finish_time;
            $correct_answer = $user->total_correct;
            $group_name = "";
            $group = $user->quiz_group;
            $npp_name = $user->npp_name;
            $cch_name = $user->cch_name;
            if ($group == 3) {
                //$team_id = $value->quiz_team == '' ? null : $value->quiz_team;
                $team_area = $user->quiz_area;
                $teamName =  getTeamName(null, $team_area);
                $full_team = $teamName->team_area;
            } else if ($group == 4) {
                $team_area = $user->quiz_area;
                $full_team = '';
            } else {
                $team_id = $user->quiz_team == '' ? null : $user->quiz_team;
                $team_area = $user->quiz_area;
                $teamName =  getTeamName($team_id, $team_area);
                $full_team = $teamName->team_area . ' - ' . $teamName->team;
            }
            if ($group == 1) {
                $group_name = "Nhân viên Công ty Lê Mây";
            } else if ($group == 2) {
                $group_name = "Nhân viên kinh doanh";
            } else if ($group == 3) {
                $group_name = "Nhà phân phối";
            } else if ($group == 4) {
                $group_name = "Chủ cửa hàng";
            }

            $phpexcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $k + 1)
                ->setCellValue('B' . $i, $fullname)
                ->setCellValue('C' . $i, $cch_name)
                ->setCellValue('D' . $i, $npp_name)
                ->setCellValue('E' . $i, $group_name)
                ->setCellValue('F' . $i, $team_area == 1 ? 'Miền Bắc' : 'Miền Nam')
                ->setCellValue('G' . $i, $full_team)
                ->setCellValue('H' . $i, $email)
                ->setCellValue('I' . $i, "'" . $phone)
                ->setCellValue('J' . $i, $correct_answer)
                ->setCellValue('K' . $i, $time_quiz);
        }
        // set tên file
        $name = 'ds_mori_' . date('Y-m-d', time()) . '.xls';
        // set config
        $phpexcel->getActiveSheet()->setTitle($name);
        $phpexcel->setActiveSheetIndex(0);
        //Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $name . '"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($phpexcel, 'Excel5');
        //ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }
}

//setting page member image 
function members_options_pages()
{
    global $wpdb;
    $status = isset($_GET['status']) ? $_GET['status'] : 1;
    $search = isset($_GET['s']) ? $_GET['s'] : '';
    $users_sucess   =   $wpdb->get_results("select * from mr_register_image left join mr_register_info on mr_register_image.info_ID = mr_register_info.id
    WHERE mr_register_image.status = 1 ORDER BY mr_register_image.id DESC");
    $users_deny     =   $wpdb->get_results("select * from mr_register_image left join mr_register_info on mr_register_image.info_ID = mr_register_info.id
    WHERE mr_register_image.status = 2 ORDER BY mr_register_image.id DESC");;
    $number = 20;
    if ($status == 2) {
        $total_users = count($users_deny);
    } else {
        $total_users = count($users_sucess);
    }
    $paged = isset($_GET['offset']) ? $_GET['offset'] : 1;
    $offset = ($paged - 1) * $number;
    //$query = get_users('&offset='.$offset.'&number='.$number);
    $total_pages = intval($total_users / $number) + 1;
    if (!$search) {
        $users  =   $wpdb->get_results("SELECT * FROM mr_register_image 
        LEFT JOIN mr_register_info on mr_register_image.info_ID = mr_register_info.id 
        WHERE mr_register_image.status = " . $status . " ORDER BY mr_register_image.id DESC LIMIT " . $offset . "," . $number);
    } else {
        $users  =   $wpdb->get_results("SELECT * FROM mr_register_image 
        LEFT JOIN mr_register_info ON mr_register_info.id = mr_register_image.info_ID
        WHERE mr_register_image.status = " . $status . " AND mr_register_info.status = " . $status . " AND mr_register_info.user_name like '%" . $search . "%'");
    }
?>
    <div class="wrap">
        <h1 class="wp-heading-inline">Chiến Binh Morinaga</h1>
        <hr class="wp-header-end">
        <ul class="subsubsub">
            <li class="administrator"><a href="<?php echo admin_url('admin.php?page=chienbinhmori_image&status=1'); ?>">Đăng ký thành công <span class="count">(<?php echo count($users_sucess); ?>)</span></a> |</li>
            <li class="subscriber"><a href="<?php echo admin_url('admin.php?page=chienbinhmori_image&status=2'); ?>">Bài không được duyệt <span class="count">(<?php echo count($users_deny); ?>)</span></a> |</li>
            <li class="subscriber"><a href="<?php echo admin_url('admin.php?page=chienbinhmori_image&action=exportexcel_image'); ?>">Export Data</a></li>
        </ul>
        <form method="get" action="<?php echo self_admin_url(); ?>">
            <p class="search-box">
                <label class="screen-reader-text" for="user-search-input">Tìm Kiếm Người Dùng:</label>
                <input type="search" id="user-search-input" name="s" value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>">
                <input type="submit" id="search-submit" class="button" value="Tìm Kiếm Người Dùng">
                <input type="hidden" name="page" value="chienbinhmori_image" />
            </p>
        </form>
        <div class="tablenav top">
            <div class="alignleft actions"></div>
            <h2 class="screen-reader-text">Điều hướng danh sách thành viên</h2>
            <div class="tablenav-pages">
                <?php
                if ($total_users > $number && !$search) {
                    echo '<div id="pagination" class="clearfix">';
                    echo '<span class="pages">Pages:</span>';
                    $current_page = max(1, $paged);
                    $pagination = paginate_links(array(
                        'base'          => '%_%',
                        'format'        => '?offset=%#%',
                        'current'       => $current_page,
                        'total'         => $total_pages,
                        'prev_next'     => false,
                        'type'         => 'array', // list  
                    ));

                    if (!empty($pagination)) {
                ?>
                        <ul class="page-numbers">
                            <?php foreach ($pagination as $key => $page_link) { ?>
                                <li class="paginated_link<?php if (strpos($page_link, 'current') !== false) {
                                                                echo ' active';
                                                            } ?>">
                                    <?php echo $page_link ?>
                                </li>
                            <?php } ?>
                        </ul>
                <?php
                    }
                    echo '</div>';
                }   ?>
            </div>
        </div>
        <?php
        // print_r($users);
        echo '<table class="wp-list-table widefat fixed striped users">';
        echo '
<thead>
<tr>
    <td id="cb" class="manage-column column-cb check-column">
    </td>
    <th scope="col" id="username" class="manage-column column-username column-primary sortable desc">
    <span>Tên Người Chơi</span>
    <span class="sorting-indicator"></span></th>
    <th scope="col" id="album_name" class="manage-column column-name">Tên Album</th>
    <th scope="col" id="area" class="manage-column column-name">Khu vực</th>
    <th scope="col" id="team" class="manage-column column-name">Đội</th>
    <th scope="col" id="email" class="manage-column column-name">Email</th>	
    <th scope="col" id="phone" class="manage-column column-name">Điện thoại</th>	
    <th scope="col" id="img" class="manage-column column-name">Hình 1</th>	
    <th scope="col" id="child_age" class="manage-column column-name">Hình 2</th>
    <th scope="col" id="phone" class="manage-column column-name">Hình 3</th>
    <th scope="col" id="address" class="manage-column column-name">Hình 4</th>
</tr>
</thead>';

        foreach ($users as $value) {
            $status = isset($_GET['status']) ? $_GET['status'] : 1;
            $group_image = explode(';', $value->group_image);
            $team_area = $value->area;
            $team_area_id = $value->team_area;
            $teamName = getTeamName($team_area_id, $team_area);
            $id = $value->id;
        ?>
            <tr>
                <th scope="row" class="check-column">

                </th>
                <td class="username column-username has-row-actions column-primary" data-colname="Tên người dùng">
                    <strong><a href="<?php echo self_admin_url('user-edit.php?user_id=' . $id); ?>">
                            <?php echo $value->user_name; ?></a></strong>
                    <br>
                    <div class="row-actions">
                        <?php if ($status == 2) { ?>
                            <span class="delete"><a href="<?php echo self_admin_url('admin.php?page=chienbinhmori_image&action=acceptuser&user_id=' . $id); ?>">Hiển Thị Bài</a> </span>
                        <?php } else { ?>
                            <span class="delete"><a href="<?php echo self_admin_url('admin.php?page=chienbinhmori_image&action=hideuser&user_id=' . $id); ?>">Ẩn Bài</a> </span>
                        <?php } ?>
                    </div>
                </td>
                <td class="name column-name" data-colname="album_name">
                    <?php echo $value->album_name; ?></td>
                <td class="name column-name" data-colname="area">
                    <?php echo $team_area == 1 ? 'Miền Bắc' : 'Miền Nam'; ?></td>
                <td class="name column-name" data-colname="team">
                    <?php echo $teamName->team; ?></td>
                <td class="name column-name" data-colname="email">
                    <?php echo $value->email; ?></td>
                <td class="name column-name" data-colname="phone">
                    <?php echo $value->phone; ?></td>
                <?php
                foreach ($group_image as $image_id) {
                ?>
                    <td class="column-name" data-colname="img">
                        <a href="<?php echo get_home_url() . '/wp-content/uploads/' . get_post_meta($image_id, '_wp_attached_file', true); ?>" target="_blank">
                            <img src="<?php echo get_home_url() . '/wp-content/uploads/' . get_post_meta($image_id, '_wp_attached_file', true); ?>" alt="" class="avatar avatar-32 photo" height="80" width="80">
                        </a>
                    </td>
                <?php
                }
                ?>
            </tr>

        <?php
        }

        echo '
<tfoot><tr>
    
    </tr>
</tfoot>
';
        echo '</table>';
        echo '</div>';
    }


    //setting page list user quiz 
    function members_quiz_options_pages()
    {
        global $wpdb;
        $status = isset($_GET['status']) ? $_GET['status'] : 1;
        $search = isset($_GET['s']) ? $_GET['s'] : '';
        $users_sucess   =   $wpdb->get_results("SELECT mr_quiz_info.*,mr_quiz_user_contest.finish_time,mr_quiz_user_contest.total_correct 
    FROM mr_quiz_info LEFT JOIN mr_quiz_user_contest ON mr_quiz_info.id = mr_quiz_user_contest.user_id WHERE mr_quiz_info.status = 1  ORDER BY mr_quiz_info.id DESC");
        $users_deny     =   $wpdb->get_results("SELECT mr_quiz_info.*,mr_quiz_user_contest.finish_time,mr_quiz_user_contest.total_correct 
    FROM mr_quiz_info LEFT JOIN mr_quiz_user_contest ON mr_quiz_info.id = mr_quiz_user_contest.user_id WHERE mr_quiz_info.status = 2   ORDER BY mr_quiz_info.id DESC");;
        $number = 20;
        if ($status == 2) {
            $total_users = count($users_deny);
        } else {
            $total_users = count($users_sucess);
        }
        $paged = isset($_GET['offset']) ? $_GET['offset'] : 1;
        $offset = ($paged - 1) * $number;
        //$query = get_users('&offset='.$offset.'&number='.$number);
        $total_pages = intval($total_users / $number) + 1;
        if (!$search) {
            $users  =   $wpdb->get_results("SELECT mr_quiz_info.*,mr_quiz_user_contest.* 
        FROM mr_quiz_info LEFT JOIN mr_quiz_user_contest ON mr_quiz_info.id = mr_quiz_user_contest.user_id WHERE mr_quiz_info.status = " . $status . " 
        ORDER BY mr_quiz_info.id DESC LIMIT " . $offset . "," . $number);
        } else {
            $users  =   $wpdb->get_results("SELECT mr_quiz_info.*,mr_quiz_user_contest.* 
        FROM mr_quiz_info LEFT JOIN mr_quiz_user_contest ON mr_quiz_info.id = mr_quiz_user_contest.user_id 
        WHERE mr_quiz_info.status = " . $status . " AND mr_quiz_info.quiz_name like '%" . $search . "%' ORDER BY mr_quiz_info.id DESC");
        }
        ?>
        <div class="wrap">
            <h1 class="wp-heading-inline">Chiến Binh Morinaga</h1>
            <hr class="wp-header-end">
            <ul class="subsubsub">
                <li class="administrator"><a href="<?php echo admin_url('admin.php?page=chienbinhmori_quiz&status=1'); ?>">Đăng ký thành công <span class="count">(<?php echo count($users_sucess); ?>)</span></a> |</li>
                <!-- <li class="subscriber"><a href="<?php echo admin_url('admin.php?page=chienbinhmori_quiz&status=2'); ?>">Bài không được duyệt <span class="count">(<?php echo count($users_deny); ?>)</span></a> |</li> -->
                <li class="subscriber"><a href="<?php echo admin_url('admin.php?page=chienbinhmori_quiz&action=exportexcel_quiz'); ?>">Export Data</a> | </li>
                <li class="subscriber"><a href="<?php echo admin_url('admin.php?page=chienbinhmori_image&action=exportexcel_quiz&type=top_quiz'); ?>">Export Top Quiz</a></li>
            </ul>
            <form method="get" action="<?php echo self_admin_url(); ?>">
                <p class="search-box">
                    <label class="screen-reader-text" for="user-search-input">Tìm Kiếm Người Dùng:</label>
                    <input type="search" id="user-search-input" name="s" value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>">
                    <input type="submit" id="search-submit" class="button" value="Tìm Kiếm Người Dùng">
                    <input type="hidden" name="page" value="chienbinhmori_quiz" />
                </p>
            </form>
            <div class="tablenav top">
                <div class="alignleft actions"></div>
                <h2 class="screen-reader-text">Điều hướng danh sách thành viên</h2>
                <div class="tablenav-pages">
                    <?php
                    if ($total_users > $number && !$search) {
                        echo '<div id="pagination" class="clearfix">';
                        echo '<span class="pages">Pages:</span>';
                        $current_page = max(1, $paged);
                        $pagination = paginate_links(array(
                            'base'          => '%_%',
                            'format'        => '?offset=%#%',
                            'current'       => $current_page,
                            'total'         => $total_pages,
                            'prev_next'     => false,
                            'type'         => 'array', // list  
                        ));

                        if (!empty($pagination)) {
                    ?>
                            <ul class="page-numbers">
                                <?php foreach ($pagination as $key => $page_link) { ?>
                                    <li class="paginated_link<?php if (strpos($page_link, 'current') !== false) {
                                                                    echo ' active';
                                                                } ?>">
                                        <?php echo $page_link ?>
                                    </li>
                                <?php } ?>
                            </ul>
                    <?php
                        }
                        echo '</div>';
                    }   ?>
                </div>
            </div>
            <?php
            echo '<table class="wp-list-table widefat fixed striped users">';
            echo '
<thead>
<tr>
    <td id="cb" class="manage-column column-cb check-column">
    </td>
    <th scope="col" id="username" class="manage-column column-username column-primary sortable desc">
    <span>Tên Người Chơi</span>
    <span class="sorting-indicator"></span></th>
    <th scope="col" id="cch" class="manage-column column-name">Chủ cửa hàng</th>
    <th scope="col" id="npp" class="manage-column column-name">Nhà phân phối</th>
    <th scope="col" id="group" class="manage-column column-name">Đối tượng</th>
    <th scope="col" id="area" class="manage-column column-name">Khu vực</th>
    <th scope="col" id="team" class="manage-column column-name">Đội</th>
    <th scope="col" id="email" class="manage-column column-name">Email</th>
    <th scope="col" id="phone" class="manage-column column-name">Số điện thoại</th>
    <th scope="col" id="answer" class="manage-column column-name">Số câu trả lời đúng</th>
    <th scope="col" id="time" class="manage-column column-name">Thời gian thi</th>
    <th scope="col" id="created_at" class="manage-column column-name">Created_at</th>
    <th scope="col" id="updated_at" class="manage-column column-name">Updated_at</th>
</tr>
</thead>';

            foreach ($users as $value) {
                $status = isset($_GET['status']) ? $_GET['status'] : 1;
                $id = $value->id;
                $group = $value->quiz_group;
                $cch_name = $value->cch_name;
                $npp_name = $value->npp_name;
                if ($group == 3) {
                    //$team_id = $value->quiz_team == '' ? null : $value->quiz_team;
                    $team_area = $value->quiz_area;
                    $teamName =  getTeamName(null, $team_area);
                    $full_team = $teamName->team_area;
                } else if ($group == 4) {
                    $team_area = $value->quiz_area;
                    $full_team = '';
                } else {
                    $team_id = $value->quiz_team == '' ? null : $value->quiz_team;
                    $team_area = $value->quiz_area;
                    $teamName =  getTeamName($team_id, $team_area);
                    $full_team = $teamName->team_area . ' - ' . $teamName->team;
                }
            ?>
                <tr>
                    <th scope="row" class="check-column">

                    </th>
                    <td class="username column-username has-row-actions column-primary" data-colname="Tên người dùng">
                        <strong><?php echo $value->quiz_name; ?></a></strong>
                    </td>
                    <td class="name column-name" data-colname="cch">
                        <?php echo $cch_name; ?></td>
                    <td class="name column-name" data-colname="npp">
                        <?php echo $npp_name; ?></td>
                    <td class="name column-name" data-colname="group">
                        <?php
                        if ($group == 1) {
                            echo "Nhân viên Công ty Lê Mây";
                        } else if ($group == 2) {
                            echo "Nhân viên kinh doanh";
                        } else if ($group == 3) {
                            echo "Nhà phân phối";
                        } else if ($group == 4) {
                            echo "Chủ cửa hàng";
                        }
                        ?>
                    </td>
                    <td class="name column-name" data-colname="area">
                        <?php echo $team_area == 1 ? 'Miền Bắc' : 'Miền Nam'; ?></td>
                    <td class="name column-name" data-colname="team">
                        <?php echo $full_team; ?></td>
                    <td class="name column-name" data-colname="email">
                        <?php echo $value->quiz_email; ?></td>
                    <td class="name column-name" data-colname="phone">
                        <?php echo $value->quiz_phone; ?></td>
                    <td class="name column-name" data-colname="answer">
                        <?php echo $value->total_correct; ?></td>
                    <td class="name column-name" data-colname="time">
                        <?php echo $value->finish_time; ?></td>
                    <td class="name column-name" data-colname="created_at">
                        <?php echo $value->created_at; ?></td>
                    <td class="name column-name" data-colname="updated_at">
                        <?php echo $value->updated_at; ?></td>
                </tr>

        <?php
            }

            echo '
<tfoot><tr>
    
    </tr>
</tfoot>
';
            echo '</table>';
            echo '</div>';
        }

        function getTeamName($team_id, $team_area)
        {
            global $wpdb;
            if ($team_id == null && !isset($team_id)) {
                $team  =   $wpdb->get_results("select * from mr_teams WHERE area = " . $team_area);
            } else {
                $team  =   $wpdb->get_results("select * from mr_teams WHERE team_id = " . $team_id . " AND area = " . $team_area);
            }
            return $team[0];
        }
        ob_end_flush();
