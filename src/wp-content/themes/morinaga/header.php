<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<!-- Locale -->
<meta http-equiv="Content-Language" content="en">
<!-- To the Future! -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <meta http-equiv="cache-control" content="no-cache" />
   <meta http-equiv="Pragma" content="no-cache" />
   <meta http-equiv="Expires" content="-1" />

<!-- Meta -->
   <meta charset="utf-8">
   <title>Chiến Binh Morinaga</title>
   <meta name="description" content="Chiến Binh Morinaga" />
   <meta name="keywords" content="morinaga, chien binh, giai thuong" />
   <meta name="author" content="Morinaga" />
   <meta name="generator" content="Chiến Binh Morinaga">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<!-- Favicons -->
   <link rel="icon" type="image/x-icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico">
   <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico">
   <link rel="apple-touch-icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico">
<!---->
   <meta property="og:url"           content="h<?php bloginfo('template_directory');?>" />
   <meta property="og:type"          content="website" />
   <meta property="og:title"         content="Morinaga" />
   <meta property="og:description"   content="Chiến Binh Morinaga" />
   <meta property="og:image"         content="<?php bloginfo('template_directory');?>/images/fb-thumb.png" />

<!-- CSS -->
   <link href="https://fonts.googleapis.com/css?family=Nunito:400,700&amp;subset=vietnamese" rel="stylesheet">
   <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/owl.carousel.min.css" />
   <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/owl.theme.default.min.css" />
   <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/styles.css?v=201907301" />
   <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/update.css?v=2019073011" />
<!-- Script -->
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/jquery.min.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/vue/vue.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144999040-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-144999040-1');
</script>
</head>
<body>
<?php 
      $current_page = 'home';
         if($wp->request){
         $current_page =$wp->request;
      }
?>
<div id="app">
<header id="header">
   <div class="container">
      <a class="logo" href="<?php home_url();?>/">
         <img src="<?php bloginfo('template_directory');?>/images/mori/logo.png" alt="">
      </a>
      <a class="nav-control" href="javascript:;">
         <span></span>
      </a>
      <ul class="main-nav header-wrapper cat-nav">
         <li class="tab-kien-thuc" data-href="#cat_ta_quan">
            <a href="javascript:;" data-href="#cuoc-thi-kien-thuc">
               <img src="<?php bloginfo('template_directory');?>/images/mori/cuoc-thi-kien-thuc_icon.png" alt="">
            </a>
         </li>
         <li class="tab-trung-bay active" data-href="#cat_ta_dan">
            <a href="javascript:;" data-href="#thong-tin-the-le">
               <img src="<?php bloginfo('template_directory');?>/images/mori/cuoc-thi-trung-bay_icon.png" alt="">
            </a>
         </li>
         <li>
            <a href="javascript:;" data-href="#thong-tin-the-le">
               <img src="<?php bloginfo('template_directory');?>/images/mori/rule-icon.png" alt="">
            </a>
         </li>
         <li>
            <a href="javascript:;" data-href="#trang-chu">
               <img src="<?php bloginfo('template_directory');?>/images/mori/home-icon.png" alt="">
            </a>
         </li>
      </ul>
   </div>
</header>