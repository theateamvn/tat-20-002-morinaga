<?php get_header();?>
<!-- START MAIN -->
<!-- START section Banner-->
    <section id="banner">
        <ul class="banner-list">
            <li>
                <div class="hide-mb anm">
                    <img src="<?php bloginfo('template_directory');?>/images/mori/banner-bg.png" alt="Banner">
                    <img class="banner-text" src="<?php bloginfo('template_directory');?>/images/mori/banner-library-story.png" alt="">
                    <!-- <img class="banner-products" src="<?php bloginfo('template_directory');?>/images/mori/chien-binh.png" alt=""> -->
                    <div class="btn-wrap">
                    <a href="#" data-toggle="modal" data-target="#registerModal" class="buy-btn" tabindex="0">Tham Gia Ngay</a>
                    </div>
                </div>
                <div class="hide-desk">
                    <img src="<?php bloginfo('template_directory');?>/images/mori/banner-mb.png" alt="Banner">
                    <div class="btn-wrap btn-wrap-mb">
                    <a href="#" data-toggle="modal" data-target="#registerModal" class="buy-btn buy-btn-mb" tabindex="0">Tham Gia Ngay</a>
                    </div>
                </div>
            </li>
            <!-- <img class="banner-cloud hide-mb" src="<?php bloginfo('template_directory');?>/images/cloud.png" alt=""> -->
        </ul>
    </section>
    <!-- END section Banner-->
    <div class="main-bg">
    <!-- START section Library-->
    <section id="thu-vien-bai-thi">
        <div class="container">
            <div class="content-wrap">
                <ul class="cat-nav">
                    <li class="active" data-href="#cat_ta_dan">Thi Trưng Bày</li>
                    <li data-href="#cat_ta_quan">Thi Kiến Thức</li>
                </ul>
                <div class="cat-list">
                    <!-- Tác Phẩm Trưng Bày-->
                    <div id="cat_ta_dan" class="cat-item cat-ta_dan active">
                    <div class="item-list " data-slug="ta_dan">
                        <!--Thi-trung-bay_the-le-->
                        <div class="wrapper-rule" id="about">
                            <h2 class="wrapper-rule__title">
                                <img src="<?php bloginfo('template_directory');?>/images/mori/rule-title.png" alt="">
                            </h2>
                            <img src="<?php bloginfo('template_directory');?>/images/mori/rule-bg.png" alt="Rule Background">
                            <div id="wrapper_rule_content" class="desc">
                                <h2 class="wrapper_rule_content_title">CUỘC THI CHIẾN BINH TRƯNG BÀY</h2>
                                <div class="wrapper_rule_content_wrapper-content">
                                    <b>1. Đối tượng dự thi </b>
                                    <br/>
                                    <div class="content-padding">
                                    Nhân viên bán hàng kênh GT, BS và MT, Y tế, OTC.
                                    </div>
                                    <br/>
                                    <b>2. Thời gian diễn ra</b> <br/>
                                    <div class="content-padding">
                                    Từ ngày 01/08/2019 đến hết 30/09/2019.
                                    </div>
                                    <br/>
                                    <b>3. Cách thức tham gia</b> <br/>
                                        <div class="content-padding">
                                            - Nhân viên đăng ký tham gia tại: https://chienbinh.morinagamilk.com.vn
                                            <br/>
                                            - Hình thức chia đội: 
                                            <br/>
                                            <div class="content-padding">
                                                + Nhân viên kinh doanh Kênh GT: Mỗi NPP là 01 Đội<br/>
                                                + Kênh BS: 01 Đội<br/>
                                                + Kênh MT: 01 Đội<br/>
                                                + Kênh OTC: 1 Đội<br/>
                                                + Kênh Y tế: 1 Đội<br/>
                                            </div>
                                        </div>
                                        <br/>
                                    <b>4. Nội dung cuộc thi</b> <br/>
                                        <div class="content-padding">
                                        - Bài dự thi bao gồm 02 phần: Phần thi Hình ảnh và Phần thi Câu chuyện
                                        <br/>
                                        <div class="content-padding">
                                        + Phần thi Hình ảnh: Nhân viên Upload trực tiếp lên Website của Cuộc thi 04 hình ảnh bao gồm
                                        <br/>
                                        • Hình ảnh cửa hàng.
                                        <br/>
                                        • Hình ảnh nhân viên làm việc tại cửa hàng.
                                        <br/>
                                        • Hình ảnh trưng bày sản phẩm (góc gần).
                                        <br/>
                                        • Hình ảnh trưng bày sản phẩm (góc xa).
                                        <br/>
                                        + Phần thi Câu chuyện: Nhân viên gửi câu chuyện dự thi với nội dung xoay quanh công việc làm trưng bày sản phẩm Morinaga tại cửa hàng. 
                                        </div>
                                        <br/>
                                        </div><br/>
                                    <b>5. Quy định chung</b> <br/>
                                        <div class="content-padding">
                                        - Bài dự thi không vi phạm thuần phong mỹ tục Việt Nam.
                                        <br/>
                                        - Bài dự thi không được sao chép từ nguồn khác. 
                                        <br/>
                                        - Bài dự thi phải là tác phẩm của người đăng ký dự thi, không bị tranh chấp tác quyền thương hiệu, quyền riêng tư và quyền sở hữu trí tuệ với bất kỳ ai.
                                        <br/>
                                        - Ban tổ chức được quyền sử dụng bài thi của người dự thi với mục đích quảng bá mà không phải trả bất kỳ khoản phí nào.
                                        </div>
                                        <br/>
                                    <b>6. Hình thức chấm giải</b> <br/>
                                        <div class="content-padding">
                                        - Bài thi sẽ được xét giải bằng thời gian làm bài và tổng số điểm của Phần thi Hình ảnh và Phần thi Câu chuyện. Tỷ lệ đóng góp điểm số của Phần thi Hình ảnh/Phần thi Câu chuyện là 70/30.
                                        <br/>
                                        - Phần thi Hình Ảnh sẽ được tổng hợp, lọc ra những phần dự thi đúng và đủ theo quy định và chấm điểm theo 03 tiêu chí sau đây:
                                        <br/>
                                        <div class="content-padding">
                                        • Vị trí dễ tiếp cận được khách hàng nhiều nhất, vị trí trưng bày tốt hơn so với đối thủ cạnh tranh, không có vật che khuất nhìn từ cửa chính. Đảm bảo dễ nhìn, dễ thấy, dễ lấy. Hàng cuối cùng của khối trưng bày thấp hơn 1,5m tính từ mặt đất. 
                                        <br/>
                                        • Cách thức trưng bày: Dựa theo Planogram.
                                        <br/>
                                        • Hình thức: Chỉn chu, sáng tạo trong cách trưng bày và bài dự thi (Đồng phục nhân viên, sử dụng POSM nhãn hàng cho kệ trưng bày, …).    
                                        <br/>
                                        • Phần thi Câu chuyện sẽ được chấm điểm theo tiêu chí: Nội dung thể hiện được tinh thần của cuộc thi và đem lại ấn tượng sâu sắc cho người đọc.
                                        <br/>
                                        </div>
                                        <i>Lưu ý:</i> Các bài thi có cùng điểm số sẽ được xét giải dựa trên điểm cộng. Điểm cộng được tính dựa trên lượt tương tác của nhân viên với các bài viết tại Fanpage “Chiến binh Morinaga”, cụ thể:
                                        <br/>
                                        <div class="content-padding">
                                        + 1 lượt chia sẻ: 2 điểm
                                        <br/>
                                        + 1 lượt thích: 1 điểm
                                        </div>
                                        <br/>
                                        </div>
                                    <b>7. Cơ cấu giải thưởng</b> <br/>
                                        <div class="content-padding">
                                            - GIẢI CÁ NHÂN: Top 60 nhân viên dự thi có điểm số cao nhất sẽ nhận được phần thưởng là 200.000 VNĐ và Combo quà tặng: 01 Mũ bảo hiểm Morinaga + 01 Balo Morinaga.
                                            <br/>
                                            - GIẢI ĐỒNG ĐỘI: Chung cuộc sẽ chọn ra 03 Đội xuất sắc có điểm trung bình toàn đội cao nhất.
                                            <br/>
                                            <div class="content-padding">
                                            + Phần thưởng dành cho ĐỘI GIẢI NHẤT là 3.000.000 VNĐ và mỗi thành viên trong đội được nhận quà tặng là 01 Mũ bảo hiểm Morinaga.
                                            <br/>
                                            + Phần thưởng dành cho ĐỘI GIẢI NHÌ là 2.000.000 VNĐ và mỗi thành viên trong đội được nhận quà tặng là 01 Mũ bảo hiểm Morinaga.
                                            <br/>
                                            + Phần thưởng dành cho ĐỘI GIẢI BA là 1.000.000 VNĐ và mỗi thành viên trong đội được nhận quà tặng là 01 Mũ bảo hiểm Morinaga.
                                            </div>
                                                Các cá nhân và đồng đội đạt giải sẽ được tuyên dương trên bản tin nội bộ, báo Cánh Sen.
                                        </div>
                                        <br/>
                                    <b>8. Cách thức công bố giải và cách thức nhận giải</b>
                                    <br/>
                                        <div class="content-padding">
                                        - Ngày 10/10/2019 Ban tổ chức sẽ công bố danh sách những Nhân viên và Đội đạt giải.
                                        <br/>
                                        - Danh sách đạt giải sẽ được cập nhật trên Website: https://chienbinh.morinagamilk.com.vn và Fanpage: Chiến binh Morinaga.
                                        <br/>
                                        - Giải thưởng sẽ được trao tới người đạt giải thông qua sự phối hợp của Team Morinaga và phòng Kinh Doanh.
                                        </div>
                                </div>
                            </div>                       
                        </div>
                        <!--END Thi-trung-bay_the-le-->
                        <!--Thi-trung-bay_giai-thuong-->
                        <div class="wrapper-prize" id="thong-tin-giai-thuong">
                            <h2 class="wrapper-prize__title">
                                <img src="<?php bloginfo('template_directory');?>/images/mori/prize-title.png" alt="">
                            </h2>
                            <img class="wrapper-prize__picture" src="<?php bloginfo('template_directory');?>/images/mori/prize-laptop.png"/>
                            <div class="hide-mb anm">
                                <div class="wrapper-prize__content-right">
                                <b class="rule_bold"> GIẢI ĐỒNG ĐỘI </b>
                                    <br/><br/>
                                    <b>ĐỘI NHẤT</b>
                                    <p class="prize_money">5.000.000 VNĐ</p>
                                    <b>ĐỘI NHÌ</b>
                                    <p class="prize_money">3.000.000 VNĐ</p>
                                    <b>ĐỘI BA</b>
                                    <p class="prize_money">2.000.000 VNĐ</p>
                                </div>
                                <div class="wrapper-prize__content-left">
                                    <b class="rule_bold"> GIẢI CÁ NHÂN </b>
                                </div>
                                <div class="wrapper-prize__content-left-trung-bay">
                                    <br/><br/>
                                    <b class="rule_bold">03 GIẢI: </b>
                                    <br/>
                                    <b class="rule_bold">Máy tỉnh bảng iPad 10.2</b>
                                    <br/>
                                    <b class="rule_bold">inch Wifi 32GB (2019)</b>
                                    <br/><br/>
                                    <b>trị giá <span class="prize_money">9.990.00đ</span></b>
                                </div>
                            </div>
                            <div class="hide-desk">
                                <div class="wrapper-prize__content-mb">
                                GIẢI ĐỒNG ĐỘI <br/>
                                GIẢI NHẤT: 3.000.000 VNĐ <br/>
                                GIẢI NHÌ: 2.000.000 VNĐ <br/>
                                GIẢI BA: 1.000.000 VNĐ <br/>
                                *Mỗi thành viên trong đội được nhận quà tặng là <br/> 01 mũ bảo hiểm Morinaga <br/><br/>
                                GIẢI CÁ NHÂN
                                <br/>
                                <b class="rule_bold">Top 60</b> nhân viên có điểm cao nhất <br/> <b>200.000 VNĐ</b> <br/>
                                    Và combo quà tặng <b> <br/>01 mũ bảo hiểm  Morinaga <br/> + 01 balo Morinaga</b>
                                </div>
                            </div>
                        </div>
                        <!--END Thi-trung-bay_giai-thuong-->
                        <!-- Thi-trung-bay_thu-vien-->
                        <div class="library-story">
                            <h2 class="wrapper-library__title">
                                <img src="<?php bloginfo('template_directory');?>/images/mori/library-title.png" alt="">
                            </h2>
                            <ul>
                                <?php
                                    global $wpdb;
                                    $users = $wpdb->get_results('select * from mr_register_info');
                                    // $customPagHTML       = "";
                                        $query           = "SELECT * FROM mr_register_info";
                                        $total_query     = "SELECT COUNT(1) FROM (${query}) AS combined_table";
                                        $total           = $wpdb->get_var( $total_query );
                                        $items_per_page  = 1;
                                        $page            = isset( $_GET['paged'] ) ? abs( (int) $_GET['paged'] ) : 1;
                                        $offset          = ( $page * $items_per_page ) - $items_per_page;
                                        $result          = $wpdb->get_results( $query . " ORDER BY ID DESC LIMIT ${offset}, ${items_per_page}" );
                                        $totalPage       = ceil($total / $items_per_page);
                                    foreach ($users as $user) {
                                        $albums = $wpdb->get_results($wpdb->prepare('select * from mr_register_image where info_ID = %d ',$user->ID));
                                        ?>
                                        <li>
                                            <div class="library-story-img">
                                                <img src="<?php bloginfo('template_directory');?>/images/mori/user-picture.png">
                                                <p><?php echo $user->user_name;?></p>                         
                                            </div>
                                            <div class="library-story-slideshow">
                                        <?php
                                        foreach ($albums as $album) {
                                            $group_image_id = explode(';',$album->group_image);
                                            ?>
                                            <div>
                                                <img data-toggle="modal" data-target="#myModal" class="list_image" data-id="<?php echo $album->ID;?>" src="<?php echo get_home_url().'/wp-content/uploads/'.get_post_meta($group_image_id[0],'_wp_attached_file', true);?>" />
                                                <p><?php echo $album->album_name; ?></p>        
                                            </div>
                                            <?php
                                        }
                                    }
                                        
                                        // if($totalPage > 1){
                                        //     $customPagHTML     =  '<div class="clearfix">'.paginate_links( array(
                                        //     'base'      => '%_%',
                                        //     'format'    => '?paged=%#%',
                                        //     'prev_text' => __('&laquo;'),
                                        //     'next_text' => __('&raquo;'),
                                        //     'total'     => $totalPage,
                                        //     'current'   => $page
                                        //     )).'</div>';
                                        //     }
                                        // echo $customPagHTML;
                                        ?>
                                        </div>
                                    </li>
                                        <?php
                                    
                                ?>      
                                    <div class="pagination">
    <?php 
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => $totalPage,
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '#paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
            'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
            'add_args'     => false,
            'add_fragment' => '',
        ) );
    ?>
</div>
                            </ul>                             
                        </div>
                        <!--END Thi-trung-bay_thu-vien-->
                    </div>
                    </div>
                    <!-- END Tác Phẩm Trưng Bày-->
                    <!-- Thi Kiến Thức-->
                    <div id="cat_ta_quan" class="cat-item cat-ta_quan">
                    <div class="item-list" data-slug="ta_quan">
                        <div class="wrapper-rule" id="about">
                            <h2 class="wrapper-rule__title">
                                <img src="<?php bloginfo('template_directory');?>/images/mori/rule-title.png" alt="">
                            </h2>
                            <img src="<?php bloginfo('template_directory');?>/images/mori/rule-bg.png" alt="Rule Background">
                            <div id="wrapper_rule_content" class="desc">
                                <h2 class="wrapper_rule_content_title">CUỘC THI CHIẾN BINH TRƯNG BÀY</h2>
                                <div class="wrapper_rule_content_wrapper-content">
                                    <b>1. Đối tượng dự thi </b>
                                    <br/>
                                    <div class="content-padding">
                                    Nhân viên bán hàng kênh GT, BS và MT, Y tế, OTC.
                                    </div>
                                    <br/>
                                    <b>2. Thời gian diễn ra</b> <br/>
                                    <div class="content-padding">
                                    Từ ngày 01/08/2019 đến hết 30/09/2019.
                                    </div>
                                    <br/>
                                    <b>3. Cách thức tham gia</b> <br/>
                                        <div class="content-padding">
                                            - Nhân viên đăng ký tham gia tại: https://chienbinh.morinagamilk.com.vn
                                            <br/>
                                            - Hình thức chia đội: 
                                            <br/>
                                            <div class="content-padding">
                                                + Nhân viên kinh doanh Kênh GT: Mỗi NPP là 01 Đội<br/>
                                                + Kênh BS: 01 Đội<br/>
                                                + Kênh MT: 01 Đội<br/>
                                                + Kênh OTC: 1 Đội<br/>
                                                + Kênh Y tế: 1 Đội<br/>
                                            </div>
                                        </div>
                                        <br/>
                                    <b>4. Nội dung cuộc thi</b> <br/>
                                        <div class="content-padding">
                                        - Bài dự thi bao gồm 02 phần: Phần thi Hình ảnh và Phần thi Câu chuyện
                                        <br/>
                                        <div class="content-padding">
                                        + Phần thi Hình ảnh: Nhân viên Upload trực tiếp lên Website của Cuộc thi 04 hình ảnh bao gồm
                                        <br/>
                                        • Hình ảnh cửa hàng.
                                        <br/>
                                        • Hình ảnh nhân viên làm việc tại cửa hàng.
                                        <br/>
                                        • Hình ảnh trưng bày sản phẩm (góc gần).
                                        <br/>
                                        • Hình ảnh trưng bày sản phẩm (góc xa).
                                        <br/>
                                        + Phần thi Câu chuyện: Nhân viên gửi câu chuyện dự thi với nội dung xoay quanh công việc làm trưng bày sản phẩm Morinaga tại cửa hàng. 
                                        </div>
                                        <br/>
                                        </div><br/>
                                    <b>5. Quy định chung</b> <br/>
                                        <div class="content-padding">
                                        - Bài dự thi không vi phạm thuần phong mỹ tục Việt Nam.
                                        <br/>
                                        - Bài dự thi không được sao chép từ nguồn khác. 
                                        <br/>
                                        - Bài dự thi phải là tác phẩm của người đăng ký dự thi, không bị tranh chấp tác quyền thương hiệu, quyền riêng tư và quyền sở hữu trí tuệ với bất kỳ ai.
                                        <br/>
                                        - Ban tổ chức được quyền sử dụng bài thi của người dự thi với mục đích quảng bá mà không phải trả bất kỳ khoản phí nào.
                                        </div>
                                        <br/>
                                    <b>6. Hình thức chấm giải</b> <br/>
                                        <div class="content-padding">
                                        - Bài thi sẽ được xét giải bằng thời gian làm bài và tổng số điểm của Phần thi Hình ảnh và Phần thi Câu chuyện. Tỷ lệ đóng góp điểm số của Phần thi Hình ảnh/Phần thi Câu chuyện là 70/30.
                                        <br/>
                                        - Phần thi Hình Ảnh sẽ được tổng hợp, lọc ra những phần dự thi đúng và đủ theo quy định và chấm điểm theo 03 tiêu chí sau đây:
                                        <br/>
                                        <div class="content-padding">
                                        • Vị trí dễ tiếp cận được khách hàng nhiều nhất, vị trí trưng bày tốt hơn so với đối thủ cạnh tranh, không có vật che khuất nhìn từ cửa chính. Đảm bảo dễ nhìn, dễ thấy, dễ lấy. Hàng cuối cùng của khối trưng bày thấp hơn 1,5m tính từ mặt đất. 
                                        <br/>
                                        • Cách thức trưng bày: Dựa theo Planogram.
                                        <br/>
                                        • Hình thức: Chỉn chu, sáng tạo trong cách trưng bày và bài dự thi (Đồng phục nhân viên, sử dụng POSM nhãn hàng cho kệ trưng bày, …).    
                                        <br/>
                                        • Phần thi Câu chuyện sẽ được chấm điểm theo tiêu chí: Nội dung thể hiện được tinh thần của cuộc thi và đem lại ấn tượng sâu sắc cho người đọc.
                                        <br/>
                                        </div>
                                        <i>Lưu ý:</i> Các bài thi có cùng điểm số sẽ được xét giải dựa trên điểm cộng. Điểm cộng được tính dựa trên lượt tương tác của nhân viên với các bài viết tại Fanpage “Chiến binh Morinaga”, cụ thể:
                                        <br/>
                                        <div class="content-padding">
                                        + 1 lượt chia sẻ: 2 điểm
                                        <br/>
                                        + 1 lượt thích: 1 điểm
                                        </div>
                                        <br/>
                                        </div>
                                    <b>7. Cơ cấu giải thưởng</b> <br/>
                                        <div class="content-padding">
                                            - GIẢI CÁ NHÂN: Top 60 nhân viên dự thi có điểm số cao nhất sẽ nhận được phần thưởng là 200.000 VNĐ và Combo quà tặng: 01 Mũ bảo hiểm Morinaga + 01 Balo Morinaga.
                                            <br/>
                                            - GIẢI ĐỒNG ĐỘI: Chung cuộc sẽ chọn ra 03 Đội xuất sắc có điểm trung bình toàn đội cao nhất.
                                            <br/>
                                            <div class="content-padding">
                                            + Phần thưởng dành cho ĐỘI GIẢI NHẤT là 3.000.000 VNĐ và mỗi thành viên trong đội được nhận quà tặng là 01 Mũ bảo hiểm Morinaga.
                                            <br/>
                                            + Phần thưởng dành cho ĐỘI GIẢI NHÌ là 2.000.000 VNĐ và mỗi thành viên trong đội được nhận quà tặng là 01 Mũ bảo hiểm Morinaga.
                                            <br/>
                                            + Phần thưởng dành cho ĐỘI GIẢI BA là 1.000.000 VNĐ và mỗi thành viên trong đội được nhận quà tặng là 01 Mũ bảo hiểm Morinaga.
                                            </div>
                                                Các cá nhân và đồng đội đạt giải sẽ được tuyên dương trên bản tin nội bộ, báo Cánh Sen.
                                        </div>
                                        <br/>
                                    <b>8. Cách thức công bố giải và cách thức nhận giải</b>
                                    <br/>
                                        <div class="content-padding">
                                        - Ngày 10/10/2019 Ban tổ chức sẽ công bố danh sách những Nhân viên và Đội đạt giải.
                                        <br/>
                                        - Danh sách đạt giải sẽ được cập nhật trên Website: https://chienbinh.morinagamilk.com.vn và Fanpage: Chiến binh Morinaga.
                                        <br/>
                                        - Giải thưởng sẽ được trao tới người đạt giải thông qua sự phối hợp của Team Morinaga và phòng Kinh Doanh.
                                        </div>
                                </div>
                            </div>                       
                        </div>

                        <div class="wrapper-prize" id="thong-tin-giai-thuong">
                            <div class="wrapper-prize-kien-thuc">
                                <h2 class="wrapper-prize__title">
                                    <img src="<?php bloginfo('template_directory');?>/images/mori/prize-title.png" alt="">
                                </h2>
                                <img class="wrapper-prize__picture" src="<?php bloginfo('template_directory');?>/images/mori/kien-thuc-prize.png"/>
                                <div class="hide-mb anm">
                                    <div class="wrapper-prize__content-right-kien-thuc">
                                        <b class="rule_bold"> GIẢI NHẤT (01 giải) </b>
                                        <br/><br/>
                                        <b class="rule_bold">01 Máy tính bảng iPad 10.2 <br/> inch Wifi 32GB (2019)</b>
                                        <br/>
                                        <b>trị giá <span class="prize_money">9.990.000đ</span></b>
                                        <br/><br/>
                                        <b class="rule_bold">GIẢI NHÌ (02 giải)</b>
                                        <br/><br/>
                                        <b class="rule_bold">01 Đồng hồ (???)</b>
                                        <br/>
                                        <b>trị giá <span class="prize_money">???đ</span></b>
                                    </div>
                                    <div class="wrapper-prize__content-left-kien-thuc">                                    
                                        <b class="rule_bold"> GIẢI BA (03 giải) </b>
                                        <br/><br/>
                                        <b class="rule_bold">01 Vali kéo (???)</b>
                                        <br/><br/>
                                        <b>trị giá <span class="prize_money">???đ</span></b>                                       
                                    </div>
                                </div>
                                <div class="hide-desk">
                                    <div class="wrapper-prize__content-mb">
                                    GIẢI ĐỒNG ĐỘI <br/>
                                    GIẢI NHẤT: 3.000.000 VNĐ <br/>
                                    GIẢI NHÌ: 2.000.000 VNĐ <br/>
                                    GIẢI BA: 1.000.000 VNĐ <br/>
                                    *Mỗi thành viên trong đội được nhận quà tặng là <br/> 01 mũ bảo hiểm Morinaga <br/><br/>
                                    GIẢI CÁ NHÂN
                                    <br/>
                                    <b class="rule_bold">Top 60</b> nhân viên có điểm cao nhất <br/> <b>200.000 VNĐ</b> <br/>
                                        Và combo quà tặng <b> <br/>01 mũ bảo hiểm  Morinaga <br/> + 01 balo Morinaga</b>
                                    </div>
                                </div>
                            </div>
                        </div>                      
                    </div>
                    </div>
                    <!-- END Câu Chuyện Chia Sẻ-->
                </div>
            </div>
        </div>
    </section>
    <!-- END section Library-->
    </div>
<!-- END main -->
<!-- START scroll to TOP-->
<a href="javascript:;" id="scrollTop">
    <i class="fa fa-chevron-circle-up" aria-hidden="true"></i>
</a>
<!-- END scroll to TOP-->
<!-- START Float Menu-->
<div id="floating_btns" class="floating-btns">
    <img class="register-btn" data-toggle="modal" data-target="#registerModal" src="<?php bloginfo('template_directory');?>/images/mori/register-btn.png" alt="">
</div>
<!-- END Float Menu-->
<!-- START popup image-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span id="img_popup_title"> </span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="model-list-image">
                <!-- <div class="list-picture-popup"> -->
            </div>
        </div>
    </div>
</div>
<!-- END popup image-->
<!-- START popup content-->
<div class="modal fade" id="modalShowStory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span id="img_popup_story_title"> </span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="content_popup_detail">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END popup content-->
<!--START register -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <!-- register info-->
                <div class="register-info register-show">
                    <div class="register-info-wrapper">
                        <form action="" method="post" enctype="multipart/form-data">
                            <div class="register_form">
                                <div class="row-form">
                                    <div class="form-group">
                                        <select id="area" name="area" v-model="area" class="form-control form-custom minimal" @change="getArea(area)">
                                            <option selected value="-1">Khu Vực</option>
                                            <option value="1">Miền Bắc</option>
                                            <option value="2">Miền Nam</option>
                                        </select>
                                    </div>
                                    <div v-if="area != -1" class="form-group">
                                        <select id="team_area" name="team_area" class="form-control form-custom minimal">
                                            <option selected value="-1">Chọn Đội</option>
                                            <option v-for="team in list_team" :value="team.team_id">{{team.team}} - {{team.team_area}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-custom" id="username" name="username" placeholder="Họ Tên">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-custom" id="email" name="email" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-custom" id="phone" name="phone" placeholder="Phone">
                                    </div>
                                    <div class="form-group">
                                        <select id="position" name="position" class="form-control form-custom minimal">
                                            <option selected value="-1">Chức Vụ</option>
                                            <option value="1">Trưởng vùng/ Quản lý bán hàng khu vực (RSM/ASM)</option>
                                            <option value="2">Giám sát kinh doanh (Sup)</option>
                                            <option value="3">Nhân viên kinh doanh (Sale)</option>
                                        </select>
                                    </div>
                                    <p class="err-msg"></p>
                                    <input type="hidden" id="res_id" name='res_id' value="0">
                                </div>
                            </div>
                            
                            </form>
                        <a id="step_1_btn" href="#" class="register-info-btn register-info-btn-center" tabindex="0">Đăng Ký</a>
                    </div>
                </div>
                <!-- END register info-->
                <!-- register picture-->
                <div class="register-picture">
                <div class="register-picture-wrapper">
                    <div class="hide-mb anm">
                        <div class="register-picture_left-content">
                            <img src="<?php bloginfo('template_directory');?>/images/mori/register-bear.png" alt="">
                        </div>
                        <div class="register-picture_preview">
                            
                        </div>
                    </div>
                    <div class="register-picture_right-content">
                        <p class="register-picture_right-content_title">Chọn ngay 4 hình ảnh dự thi của bạn!</p>
                        <form id="msform" action="#" method="post" enctype="multipart/form-data">
                            <label for="ablum_name">Tên ablum</label>
                            <input type="text" name="ablum_name" class="form-control input-circle" id="ablum_name" placeholder="Enter ablum name" />
                            <label for="main_image" class="register-picture_right-content_filecustom">   
                                <img for="main_image" id="img_upload" class="register-picture_right-content_upload" src="<?php bloginfo('template_directory');?>/images/mori/register-upload.png" alt="">
                            </label>
                            <!-- <input type="file" name="main_image[]" id="main_image"  multiple="true" value="" accept=".png, .jpg, .jpeg" onchange="loadFile(event)"/> -->
                            <input type="file" name="main_image[]" id="main_image"  multiple="true" value="" accept="image/*"/>
                        </form>
                        <p class="err-upload-msg"></p>
                        <a id="step_2_btn" href="#" class="register-info-btn register-info-btn-center" tabindex="0">Upload</a>
                    </div>
                </div>
                </div>
                <!-- END register picture-->
                <!-- register content-->
                <div class="register-content">
                <div class="register-content-wrapper">
                    <div class="hide-mb anm">
                        <div class="register-content_left-content">
                            <img src="<?php bloginfo('template_directory');?>/images/mori/register-bear.png" alt="">
                        </div>
                    </div>
                    <div class="register-content_right-content">
                        <p class="register-content_right-content_title">Bạn đã có bộ ảnh đẹp rồi, <br/>hãy chia sẻ câu chuyện hay nào!</p>
                        <form>
                            <div class="row-form">
                                <div class="form-group">
                                    <textarea class="form-control" id="your_story" name="your_story" rows="8" placeholder="Câu chuyện của bạn"></textarea>
                                </div>
                            </div>
                        </form>
                        <p class="err-story-msg"></p>
                        <div class="register-content_btn_wrapper">
                            <a id="step_3_btn" href="#" class="register-info-btn register-content_btn-content" tabindex="0">Chia Sẻ Ngay</a>
                            <a id="step_4_btn" href="#" class="register-info-btn register-content_btn-content" tabindex="0">Làm Lại</a>
                        </div>
                    </div>
                </div>
                </div>
                <!-- END register content-->
                <!-- register success-->
                <div class="register-success">
                <div class="register-success-wrapper">
                    <div class="hide-mb anm">
                        <div class="register-content_left-content">
                            <img src="<?php bloginfo('template_directory');?>/images/mori/register-bear.png" alt="">
                        </div>
                    </div>
                    <div class="register-content_right-content">
                        <p class="register-success_right-content_title">Chúc mừng bạn đã trở thành 1 trong những chiến binh Morinaga dũng mãnh!</p>
                        <a id="step_6_btn" href="/#thu-vien-bai-thi" class="register-info-btn register-success_btn register-info-btn-center" tabindex="0">Về Trang Chủ</a>
                    </div>
                </div>
                </div>
                <!-- END register success-->
            </div>
        </div>
    </div>
</div>
<!--END register-->
<script type="text/javascript">   
        var loadFile = function(event) {
            var output = document.getElementById('preview_img');
            output.src = URL.createObjectURL(event.target.files[0]);
        }; 
        
    (function($){
        
        $(document).ready(function(){
            $('form').on('keyup', function (e) {
                $("p.err-msg").html('');
                $("p.err-upload-msg").html('');
                $("p.err-story-msg").html('');
            });
            // Multiple images preview in browser
            var imagesPreview = function(input, placeToInsertImagePreview) {
                var filesToUpload = [];
                if (input.files) {
                    var filesAmount = input.files.length;
                    $('.register-picture_preview').html('');
                    if(filesAmount != 4){
                        $('.register-picture_left-content').removeClass('register-hide');
                        $('.register-picture_left-content').addClass('register-show');
                        $("p.err-upload-msg").html('Vui lòng chỉ chọn 4 hình ảnh.');
                    } else {
                        $('.register-picture_left-content').addClass('register-hide');
                        $('.register-picture_left-content').removeClass('register-show');
                        for (i = 0; i < filesAmount; i++) {
                            var reader = new FileReader();
                            reader.onload = function(event) {
                                $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            }
                            reader.readAsDataURL(input.files[i]);
                        }
                    }
                }
            };
            $('#main_image').on('change', function() {
                imagesPreview(this, 'div.register-picture_preview');
            });

            $('#step_3_btn').click(function(){
                var your_story = $('#your_story').val();
                var id = $('#res_id').val();
                if(your_story == ''){
                    $("p.err-story-msg").html('Vui lòng viết câu chuyện của bạn.');
                    $("#your_story").focus();
                } else {
                    // loading 
                    var $this = $(this);
                    var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
                        if ($(this).html() !== loadingText) {
                            $this.data('original-text', $(this).html());
                            $this.html(loadingText);
                        }
                        setTimeout(function() {
                            $this.html($this.data('original-text'));
                        }, 4000);
                        // end loading
                    $.ajax({
                            type:'post',
                            dataType:'json',
                            url:'<?php echo admin_url('admin-ajax.php');?>', 
                            data:{
                                action: 'submitStory',
                                your_story: your_story,
                                id: id
                            },
                            context:this,
                            beforeSend: function(){
                                //Làm gì đó trước khi gửi dữ liệu vào xử lý
                            },
                            success: function(response) {
                                //Làm gì đó khi dữ liệu đã được xử lý
                                if(response.success) {
                                    $('.register-content').removeClass('register-show');
                                    $('.register-content').addClass('register-hide');
                                    $('.register-success').addClass('register-show');
                                }
                                else {
                                    $("p.err-msg").html(response.message);
                                }
                            },
                            error: function( jqXHR, textStatus, errorThrown ){
                                //Làm gì đó khi có lỗi xảy ra
                                console.log( 'The following error occured: ' + textStatus, errorThrown );
                            }
                    })
                }
            });
        $('#step_2_btn').click(function(){
            if ($('#main_image').get(0).files.length < 4 | $('#main_image').get(0).files.length > 4) {
                $("p.err-upload-msg").html('Vui lòng chọn 4 hình ảnh.');
            } else {
                // loading 
                var $this = $(this);
                var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
                if ($(this).html() !== loadingText) {
                    $this.data('original-text', $(this).html());
                    $this.html(loadingText);
                }
                setTimeout(function() {
                    $this.html($this.data('original-text'));
                }, 8000);
                // end loading
                var id = $('#res_id').val();
                var ablum_name = $('#ablum_name').val();
                var fd = new FormData();
                //fd.append( "main_image", $('#main_image')[0].files[0]);
                for (i = 0; i < $('#main_image')[0].files.length; i++) {
                    fd.append( 'main_image[' + i + ']', $('#main_image')[0].files[i]);
                }
                fd.append( "action", 'submitUpload');   
                fd.append( "id", id);
                fd.append( "ablum_name", ablum_name);
                $.ajax({
                        type:'post',
                        dataType:'json',
                        url:'<?php echo admin_url('admin-ajax.php');?>', 
                        data:fd,
                        processData: false,
                        contentType: false,
                        beforeSend: function(){
                            //Làm gì đó trước khi gửi dữ liệu vào xử lý
                        },
                        success: function(data, textStatus, XMLHttpRequest) {
                            //Làm gì đó khi dữ liệu đã được xử lý
                            $('.register-picture').removeClass('register-show');
                            $('.register-picture').addClass('register-hide');
                            $('.register-content').addClass('register-show');
                        },
                        error: function( MLHttpRequest, textStatus, errorThrown ){
                            //Làm gì đó khi có lỗi xảy ra
                            console.log( 'The following error occured: ' + textStatus, errorThrown );
                        }
                    })
            }
        });
        $('#step_1_btn').click(function(){
                // check email
                
                
                var patternEmail = '/^[a-z0-9_\-\.]{2,}@[a-z0-9_\-\.]{2,}\.[a-z]{2,4}$/i';
                var area = $('#area').val();
                var team_area = $('#team_area').val();
                var username = $('#username').val();
                var email = $('#email').val();
                var position = $('#position').val();
                var phone = $('#phone').val();
                if(area == -1){
                    $("p.err-msg").html('(*) Vui lòng chọn khu vực.');
                    $this.html($this.data('original-text'));
                    $("#area").focus();
                } else if(team_area == -1) {
                    $("p.err-msg").html('Vui lòng chọn tên đội.');
                    $("#team_area").focus();
                } else if(username == '') {
                    $("p.err-msg").html('Vui lòng điền họ và tên.');
                    $("#username").focus();
                } else if(email == '') {
                    $("p.err-msg").html('Vui lòng điền email.');
                    $("#email").focus();
                } else if(!validateEmail(email)) {
                    $("p.err-msg").html('Email chưa đúng định dạng.');
                    $("#email").focus();
                } else if(phone == '') {
                    $("p.err-msg").html('vui lòng điền số điện thoại.');
                    $("#phone").focus();
                } else if(position == -1) {
                    $("p.err-msg").html('Vui lòng chọn chức vụ.');
                    $("#postion").focus();
                }  else {
                    // loading 
                    var $this = $(this);
                    var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
                    if ($(this).html() !== loadingText) {
                        $this.data('original-text', $(this).html());
                        $this.html(loadingText);
                    }
                    setTimeout(function() {
                        $this.html($this.data('original-text'));
                    }, 4000);
                    // end loading
                    $.ajax({
                        type:'post',
                        dataType:'json',
                        url:'<?php echo admin_url('admin-ajax.php');?>', 
                        data:{
                            action: 'submitInfo',
                            username: username,
                            area:area,
                            team_area:team_area,
                            email:email,
                            position:position,
                            phone: phone
                        },
                        context:this,
                        beforeSend: function(){
                            //Làm gì đó trước khi gửi dữ liệu vào xử lý
                        },
                        success: function(response) {
                            //Làm gì đó khi dữ liệu đã được xử lý
                            if(response.success) {
                                $('#res_id').val(response.id);
                                $('.register-info').addClass('register-hide');
                                $('.register-info').removeClass('register-show');
                                $('.register-picture').addClass('register-show');
                            }
                            else {
                                $("p.err-msg").html(response.message);
                            }
                        },
                        error: function( jqXHR, textStatus, errorThrown ){
                            //Làm gì đó khi có lỗi xảy ra
                            console.log( 'The following error occured: ' + textStatus, errorThrown );
                        }
                    })
                }
                });

                $('.list_image').click(function(){
                    var album_id = $(this).data('id');
                    $.ajax({
                        type:'post',
                        dataType:'json',
                        url:'<?php echo admin_url('admin-ajax.php');?>', 
                        data:{
                            action: 'submitImage',
                            album_id: album_id
                        },
                        context:this,
                        beforeSend: function(){
                            //Làm gì đó trước khi gửi dữ liệu vào xử lý
                        },
                        success: function(response) {
                            //Làm gì đó khi dữ liệu đã được xử lý
                            if(response.success) {
                                var list_image = response.list_image;
                                var string = "";
                                list_image.forEach(element =>{
                                    string = string + "<div><img src='"+element+"' alt='Chi tiết hình ảnh' ></div>";
                                });
                                string = "<div class='owl-carousel owl-theme'>"+string+"</div>";
                                $('#model-list-image').html(string);
                            }
                            else {
                                $("p.err-msg").html(response.message);
                            }
                        },
                        error: function( jqXHR, textStatus, errorThrown ){
                            //Làm gì đó khi có lỗi xảy ra
                            console.log( 'The following error occured: ' + textStatus, errorThrown );
                        }
                    })
                });
        });
        
        

    })(jQuery)
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

</script>
<?php get_footer();?>