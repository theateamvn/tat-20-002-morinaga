<?php
    /**
     * 
     * Template Name: Update Comment
     * 
     * @package twentynineteen
     */
    get_header();
?>
<style>
.popup{
background: white;
border: 1px solid gray;
z-index: 10000;
/* --box-shadow: 3px 3px gray; */
}

table{
    border:0px;
}

#background{
position: absolute;
}
</style>

<a href="#" class="click_popup"><button>Đăng kí</button></a>
<div id="popup1" class="popup" style="position:absolute;width:30rem;height:auto;top:30px">
<div style="text-align:right"><a href="#" style="color:red;font-family:arial" class="close"/>X</a></div>
<div>
    <?php custom_form_upload();?>
</div>
</div>
<script type="text/javascript">    
           
            $(document).ready(function(){
            $(".popup").hide();
            $(".close").click(function (e) {
            $("#background").fadeOut();
            $(".popup").hide();
            e.preventDefault();
            });

            $("#background").click(function () {

            $("#background").fadeOut();

            $(".popup").hide();
        });

                $('.click_popup').click(function(){
                var dheight = document.body.clientHeight;

                var dwidth = document.body.clientWidth;

                $("#background").width(dwidth).height(dheight);

                $("#background").fadeTo("slow",0.8);

                var $popup1=$("#popup1");

                $popup1.css("top", (dheight-$popup1.height())/2);

                $popup1.css("left",(dwidth-$popup1.width())/2);

                $popup1.fadeIn();

                $('#submit').click(function(){
                    $.ajax({
                    type:'post',
                    dataType:'json',
                    url:'<?php echo admin_url('admin-ajax.php');?>', 
                    data:{
                        action: 'upload',
                    },
                    context:this,
                    beforeSend: function(){
                        //Làm gì đó trước khi gửi dữ liệu vào xử lý
                    },
                    success: function(response) {
                        //Làm gì đó khi dữ liệu đã được xử lý
                        if(response.success) {
                            alert(response.data);
                        }
                        else {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function( jqXHR, textStatus, errorThrown ){
                        //Làm gì đó khi có lỗi xảy ra
                        console.log( 'The following error occured: ' + textStatus, errorThrown );
                    }
                })
                })
            })        
        }) 
        </script>

<?php get_footer();
function custom_form_upload(){
    global $reg_errors,$bio;
    echo' <form action="' . $_SERVER['REQUEST_URI'] . '" method="post" enctype="multipart/form-data">
        <div class="register_form">
        <table>
        <tr>
        <td><label for="image">Bài viết<strong>*</strong></label></td>
        <td><textarea name="bio" value="' . ( isset( $_POST['bio']) ? $bio : null ) . '"></textarea></td>
        </tr>
        </div>
        <tr>
        <td colspan="2"><input type="submit" id="submit" name="submit" value="Upload"/></td>
        </tr>
        </table>
        </form>';

    if(isset($_POST['submit'])){
        
        $reg_errors = new WP_Error;
        if(empty($_POST['bio'])){
            $reg_errors->add('Bio', 'Bio not blank');
        }
        update_user_meta(9, 'bio', $_POST['bio']);
    }
}