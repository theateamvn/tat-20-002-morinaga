<!--START Footer-->
<footer id="footer">
   <div class="container">
      <div class="ft-content">
      <div class="socials">
         <a href="https://www.facebook.com/chienbinhmorinaga/" target="_blank" class="fb">
         <i aria-hidden="true" class="fa fa-facebook"></i></a> 
         <a href="https://www.youtube.com/watch?v=Vc8NGQlScMI" target="_blank" class="yt">
         <i aria-hidden="true" class="fa fa-play"></i></a>
      </div>
         <p>Copyright © 2019 Morinaga. All Rights Reserved!</p>
      </div>
   </div>
</footer>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
   window.fbAsyncInit = function() {
      FB.init({
         xfbml            : true,
         version          : 'v4.0'
      });
   };

   (function(d, s, id) {
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) return;
   js = d.createElement(s); js.id = id;
   js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
   fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));</script>

   <!-- Your customer chat code -->
   <div class="fb-customerchat"
   attribution=setup_tool
   page_id="1845778882190536"
   theme_color="#0084ff"
   logged_in_greeting="Chiến binh có chi nói đi ngại gì!"
   logged_out_greeting="Chiến binh có chi nói đi ngại gì!">
   </div>
<!--END Footer-->
</div>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/bootstrap.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/slick.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/TweenMax.min.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/ScrollMagic.min.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/animation.gsap.min.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/owl.carousel.min.js"></script>
   <!-- scripts -->
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/vue.js?v=20190710"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/main.js?v=20190711"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/quiz.js"></script>
   <script type="text/javascript" src="https://www.jqueryscript.net/demo/Minimal-jQuery-Any-Date-Countdown-Timer-Plugin-countdown/dest/jquery.countdown.js"></script>
   <!-- end scripts -->
   <?php wp_footer();?>
</body>
</html>