<?php get_header(); ?>
<!-- START MAIN -->
<!-- START section Banner-->
<section id="banner">
    <ul class="banner-list">
        <li>
            <div class="hide-mb anm">
                <img src="<?php bloginfo('template_directory'); ?>/images/mori/banner-noi-bat.jpg" alt="Banner">
                <img class="banner-text" src="<?php bloginfo('template_directory'); ?>/images/mori/banner-library-story.png" alt="">
                <!-- <img class="banner-products" src="<?php bloginfo('template_directory'); ?>/images/mori/chien-binh.png" alt=""> -->
                <div class="btn-wrap">
                    <a href="javascript:;" data-href="thong-tin-the-le" class="buy-btn btn-join" tabindex="0">Tham Gia Ngay</a>
                </div>
            </div>
            <div class="hide-desk">
                <img src="<?php bloginfo('template_directory'); ?>/images/mori/banner-mb.png" alt="Banner">
                <div class="btn-wrap btn-wrap-mb">
                    <a href="javascript:;" data-href="thong-tin-the-le" class="buy-btn buy-btn-mb btn-join" tabindex="0">Tham Gia Ngay</a>
                </div>
            </div>
        </li>
        <!-- <img class="banner-cloud hide-mb" src="<?php bloginfo('template_directory'); ?>/images/cloud.png" alt=""> -->
    </ul>
</section>
<!-- END section Banner-->
<div class="main-bg">
    <!-- START section Library-->
    <section id="thu-vien-bai-thi">
        <div class="content-wrap">
            <h2 class="wrap-thong-tin-the-le">
                <img src="<?php bloginfo('template_directory'); ?>/images/mori/rule-title.png" alt="">
            </h2>
            <ul class="cat-nav cat-new-style-2">
                <li class="tab-kien-thuc active" data-href="#cat_ta_quan">Thi Kiến Thức</li>
                <li class="tab-trung-bay" data-href="#cat_ta_dan">Thi Trưng Bày</li>
            </ul>
            <div class="cat-list">
                <!-- Tác Phẩm Trưng Bày-->
                <div id="cat_ta_dan" class="cat-item cat-ta_dan">
                    <div class="item-list " data-slug="ta_dan">
                        <!--Thi-trung-bay_the-le-->
                        <div class="container">
                            <div class="container-90">
                                <div class="wrapper-rule" id="about">                                  
                                    <img src="<?php bloginfo('template_directory'); ?>/images/mori/rule-bg.png" alt="Rule Background">
                                    <div id="wrapper_rule_content" class="desc">
                                        <h2 class="wrapper_rule_content_title">CUỘC THI ĐỘ PHỦ & TRƯNG BÀY</h2>
                                        <div class="wrapper_rule_content_wrapper-content">
                                            <b>1. Đối tượng dự thi </b>
                                            <br />
                                            <div class="content-padding">
                                                Nhân viên kinh doanh Morinaga.
                                            </div>
                                            <br />
                                            <b>2. Thời gian diễn ra</b> <br />
                                            <div class="content-padding">
                                                Từ ngày 02/03 đến ngày 31/03/2020.
                                            </div>
                                            <br />
                                            <b>3. Cách thức tham gia</b> <br />
                                            <div class="content-padding">
                                                - Nhân viên đăng ký tham gia tại: https://chienbinh3.morinagamilk.com.vn
                                                <br />
                                            <b>4. Hình thức chia đội:</b> <br />
                                                Gồm 17 nhóm trên phạm vi toàn quốc:
                                                - Miền Bắc
                                                <div class="content-padding">
                                                    + GT Hà Nội (Trung tâm)<br />
                                                    + GT Hà Nội (Ven) + Tây Bắc 1<br />
                                                    + Tây Bắc 2<br />
                                                    + Đông Bắc 1<br />
                                                    + Đông Bắc 2<br />
                                                    + Duyên Hải<br />
                                                    + Bắc Miền Trung<br />
                                                    + BS<br />
                                                    + MT + MC
                                                </div>
                                                - Miền Nam:
                                                <div class="content-padding">
                                                    + GT Hồ Chí Minh + OTC<br />
                                                    + Miền Trung <br />
                                                    + Miền Trung 1<br />
                                                    + Miền  Đông 1<br />
                                                    + Miền  Đông 2<br />
                                                    + Nam Mê Kông<br />
                                                    + Bắc Mê Kông<br />
                                                    + BS + MT + MC<br />
                                                </div>
                                            </div>
                                            <br />
                                            <b>5. Nội dung cuộc thi</b> <br />
                                            <div class="content-padding">
                                                - Nhân viên upload trực tiếp lên website album ảnh bao gồm:
                                                <br />
                                                <div class="content-padding">
                                                    + Hình ảnh bảng hiệu cửa hàng
                                                    <br />
                                                    + Hình ảnh trưng bày sản phẩm
                                                </div>
                                            </div><br />
                                            <b>6. Quy định chung</b> <br />
                                            <div class="content-padding">
                                                - Với mỗi cửa hiệu, người tham gia chụp ít nhất là 2 tấm ảnh và tối đa là 4 tấm ảnh.
                                                <br />
                                                - Bài dự thi không được sao chép từ nguồn khác.
                                                <br />
                                                - Bài dự thi phải là tác phẩm của người đăng kí dự thi, không bị tranh chấp bản quyền thương hiệu, quyền riêng tư và quyền sở hữu trí tuệ với bất kì ai.
                                                <br />
                                                - Ban tổ chức được quyền sử dụng bài thi của người dự thi với mục đích quảng bá mà không cần xin phép hoặc trả bất kỳ khoản phí nào.
                                            </div>
                                            <br />
                                            <b>7. Hình thức chấm giải</b> <br />
                                            <div class="content-padding">
                                                - Bài thi được chấm điểm theo 4 tiêu chí:
                                                <br />
                                                <div class="content-padding">
                                                    • Số lượng cửa hàng nhiều nhất.
                                                    <br />
                                                    • Vị trí dễ tiếp cận được nhiều khách hàng nhất, vị trí trưng bày tốt hơn so với đối thủ cạnh tranh, không có vật che khuất tầm nhìn từ cửa chính.
                                                    <br />
                                                    • Đảm bảo dễ nhìn, dễ thấy, dễ lấy.
                                                    <br />
                                                    • Hàng cuối cùng của khối trưng bày thấp hơn 1,5m tính từ mặt đất.
                                                    <br />
                                                </div>
                                                + Cách thức trưng bày: Dựa theo Planogram
                                                <br />
                                                + Hình thức: Chỉn chu, sáng tạo trong cách trưng bày và bài dự thi (sử dụng POSM nhãn hàng cho kệ trưng bày là một điểm cộng).
                                                <br/>
                                                + Tính điểm theo trọng số: Độ phủ 60% - Trưng bày 40%
                                                <br/>
                                                + Các bài dự thi có cùng điểm số sẽ được xét bài dựa trên lượt tương tác của nhân viên với Fanpage “Chiến binh Morinaga”, cụ thể:
                                                <div class="content-padding">
                                                    • 1 lượt chia sẻ: 2 điểm <br/>
                                                    • 1 lượt thích: 1 điểm
                                                </div>
                                            </div>
                                            <br />
                                            <b>8. Cơ cấu giải thưởng</b> <br />
                                            <div class="content-padding">
                                                - GIẢI CÁ NHÂN: 3 nhân viên dự thi có số điểm cao nhất sẽ nhận được phần thưởng là 1 chiếc IPAD 10.2 inch Wifi 32GB (2019).
                                                <br />
                                                - GIẢI ĐỒNG ĐỘI: Chung cuộc chọn ra 3 đội xuất sắc có điểm trung bình cộng cao nhất. Ban tổ chức chọn ra 5 nhân viên có điểm cao nhất để tính điểm cho đội. (Mỗi đội thi phải có ít nhất 5 người tham gia).
                                                <br />
                                                <div class="content-padding">
                                                    + Giải nhất: (1 giải): Đội chơi có điểm số cao nhất sẽ nhận được phần thưởng trị giá 5,000,000vnđ.
                                                    <br />
                                                    + Giải nhì (1 giải): Đội chơi có điểm số cao thứ 2 sẽ nhận được phần thưởng trị giá 3,000,000vnđ.
                                                    <br />
                                                    + Giải ba (1 giải): Đội chơi có điểm số cao thứ 3 sẽ nhận được phần thưởng trị giá 2,000,000vnđ.
                                                </div>
                                                Các cá nhân và đồng đội đạt giải sẽ được tuyên dương trên bản tin nội bộ, báo Cánh Sen.
                                            </div>
                                            <br />
                                            <b>9. Cách thức công bố giải và cách thức nhận giải</b> <br />
                                            <div class="content-padding">
                                                + Ngày 15/04/2020 Ban tổ chức sẽ công bố danh sách những nhân viên đạt giải
                                                <br/>
                                                + Danh sách đạt giải sẽ được cập nhật trên website: https://chienbinh3.morinagamilk.com.vn/
                                                <br/>
                                                + Giải thưởng sẽ được trao tới người đạt giải thông qua sự phối hợp của Team Morinaga và phòng Kinh Doanh
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--END Thi-trung-bay_the-le-->
                        <!--Thi-trung-bay_giai-thuong-->
                        <div class="wrapper-prize" id="thong-tin-giai-thuong">
                            <h2 class="wrapper-prize__title">
                                <img src="<?php bloginfo('template_directory'); ?>/images/mori/prize-title.png" alt="">
                            </h2>
                            <img class="wrapper-prize__picture" src="<?php bloginfo('template_directory'); ?>/images/mori/prize-laptop-4.png" />
                            <div class="hide-mb anm">
                                <div class="wrapper-prize__content-right-kien-thuc">
                                    <b class="rule_bold"> GIẢI ĐỒNG ĐỘI </b>
                                    <br /><br />
                                    <b>1 GIẢI NHẤT</b>
                                    <p class="prize_money">5.000.000 VND</p>
                                    <b>1 GIẢI NHÌ</b>
                                    <p class="prize_money">3.000.000 VND</p>
                                    <b>1 GIẢI BA</b>
                                    <p class="prize_money">2.000.000 VND</p>
                                </div>
                                <div class="wrapper-prize__content-left-kien-thuc">
                                    <b class="rule_bold"> GIẢI CÁ NHÂN </b>
                                    <br />
                                    <b class="rule_bold">03 GIẢI</b>
                                    <br /><br />
                                    <p class="prize_money"><b>IPAD 10.2 inch <br/> Wifi 32GB (2019)</b></p>
                                </div>
                            </div>
                            <div class="hide-desk">
                                <div class="wrapper-prize__content-mb">
                                    GIẢI ĐỒNG ĐỘI <br />
                                    GIẢI NHẤT: 5.000.000 VNĐ <br />
                                    GIẢI NHÌ: 3.000.000 VNĐ <br />
                                    GIẢI BA: 2.000.000 VNĐ <br /><br />
                                    GIẢI CÁ NHÂN
                                    <br />
                                    <b class="rule_bold">03 GIẢI</b>
                                    <p class="prize_money"><b>IPAD 10.2 inch <br/> Wifi 32GB (2019)</b></p>
                                </div>
                            </div>
                            <ul class="cat-nav cat-new-style">
                                <li class="tab-kien-thuc active" data-href="#cat_ta_quan">Thi Kiến Thức</li>
                                <li class="tab-trung-bay " data-href="#cat_ta_dan">Thi Trưng Bày</li>
                            </ul>
                        </div>
                        <!--END Thi-trung-bay_giai-thuong-->
                        <!-- Thi-trung-bay_thu-vien-->
                        <div class="container">
                            <div class="container-90">
                                <div class="library-story">
                                    <h2 class="wrapper-library__title">
                                        <img src="<?php bloginfo('template_directory'); ?>/images/mori/library-title.png" alt="">
                                    </h2>
                                    <ul id="load-user">
                                        <?php
                                        global $wpdb;
                                        $users = $wpdb->get_results('select * from mr_register_info WHERE status = 1 ORDER BY id DESC limit 0,2');
                                        if (count($users) > 0) {
                                            foreach ($users as $user) {
                                                $albums = $wpdb->get_results($wpdb->prepare('select * from mr_register_image where info_id = %d ', $user->id));
                                        ?>
                                                <li>
                                                    <div class="library-story-img">
                                                        <img src="<?php bloginfo('template_directory'); ?>/images/mori/user-library-story-1.png">
                                                        <p><?php echo $user->user_name; ?></p>
                                                    </div>
                                                    <div class="library-story-slideshow">
                                                        <?php
                                                        foreach ($albums as $album) {
                                                            $group_image_id = explode(';', $album->group_image);
                                                        ?>
                                                            <div class="library-story-slideshow_item">
                                                                <img data-toggle="modal" data-target="#myModal" class="list_image" data-id="<?php echo $album->id; ?>" src="<?php echo get_home_url() . '/wp-content/uploads/' . get_post_meta($group_image_id[0], '_wp_attached_file', true); ?>" />
                                                                <p><?php echo $album->album_name; ?></p>
                                                            </div>
                                                    <?php
                                                        }
                                                    }
                                                ?>
                                                </div>
                                            </li><?php
                                        }else{
                                            ?>
                                            <p class="library-none-data">Chưa có dữ liệu!</p>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <div class="album-pagination">
                                    <?php
                                    $count = $wpdb->get_var('SELECT COUNT(id) AS NumberOfUser FROM mr_register_info WHERE mr_register_info.status = 1');
                                    $totalPage = $count / 2;
                                    for ($i = 0; $i < $totalPage; $i++) {
                                        echo '<a class="paginated_link action-links" data-paginate="' . ($i + 1) . '">' . ($i + 1) . '</a>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <!--END Thi-trung-bay_thu-vien-->
                    </div>
                </div>
                <!-- END Tác Phẩm Trưng Bày-->
                <!-- Thi Kiến Thức-->
                <div id="cat_ta_quan" class="cat-item cat-ta_quan active">
                    <div class="item-list" data-slug="ta_quan">
                        <div class="container">
                            <div class="container-90">
                                <div class="wrapper-rule" id="about">
                                    <img src="<?php bloginfo('template_directory'); ?>/images/mori/rule-bg.png" alt="Rule Background">
                                    <div id="wrapper_rule_content" class="desc">
                                        <h2 class="wrapper_rule_content_title">CUỘC THI KIẾN THỨC SẢN PHẨM</h2>
                                        <div class="wrapper_rule_content_wrapper-content">
                                            <b>1. Đối tượng dự thi </b>
                                            <br />
                                            <div class="content-padding">
                                                + Nội bộ Lê Mây (Không bao gồm những người có liên quan trong việc tổ chức, thúc đẩy và vận hành cuộc thi. Bao gồm: Ban giám khảo, team Morinaga, team Digital)
                                                <br/>
                                                + Nhà phân phối, buyers của các kênh BS, MT, MC, EC, OTC
                                                <br/>
                                                + Cửa hiệu bán sữa Morinaga
                                            </div>
                                            <br />
                                            <b>2. Thời gian diễn ra</b> <br />
                                            <div class="content-padding">
                                                Từ ngày 02/03 đến ngày 05/03/2020.
                                            </div>
                                            <br />
                                            <b>3. Cách thức tham gia</b> <br />
                                                <div class="content-padding">
                                                - Nhân viên đăng ký tham gia tại: https://chienbinh3.morinagamilk.com.vn
                                                </div>
                                                <br />
                                            <b>4. Hình thức chia đội: </b>
                                                <br />
                                                <div class="content-padding">
                                                    + Đội 1: Nhân viên kinh doanh Morinaga<br />
                                                    + Đội 2: Nhà phân phối, buyers của các kênh BS, MT, MC, EC, OTC và đại lý<br />
                                                    + Đội 3: Nội bộ Lê Mây (Trừ Nhân viên kinh doanh Morinaga)<br />
                                                </div>
                                            <br />
                                            <b>5. Nội dung cuộc thi</b> <br />
                                            <div class="content-padding">
                                                - Người chơi đăng nhập và trả lời 25 câu hỏi trắc nghiệm về sản phẩm mới. Ngay sau khi người chơi hoàn thành bài thi, kết quả thi và thứ hạng theo đội sẽ được thông báo và chia sẻ công khai trên website.
                                                <br />
                                            </div><br />
                                            <b>6. Quy định chung</b> <br />
                                            <div class="content-padding">
                                                - Mỗi người chơi được tham gia thi tối đa 2 lần và lấy điểm cao hơn làm kết quả cuối cùng.
                                                <br />
                                                - Trường hợp người chơi đang thi mà gặp sự cố về kĩ thuật, liên hệ ngay với ban tổ chức để sớm được hỗ trợ.
                                            </div>
                                            <br />
                                            <b>7. Hình thức chấm giải</b> <br />
                                            <div class="content-padding">
                                                - Bài thi được xét giải bằng thời gian làm bài và tổng số câu trả lời đúng của người chơi. Mỗi câu trả lời đúng người chơi sẽ nhận được 0.4 điểm. Những người chơi nào có số câu trả lời đúng nhiều nhất và nhanh nhất sẽ nhận được các phần quà của chương trình.
                                                <br/>
                                            </div>
                                            <br />
                                            <b>8. Cơ cấu giải thưởng</b> <br />
                                            Mỗi đội sẽ có 4 mức giải thưởng (mức giải thưởng này là ngang nhau giữa các đội):
                                            <div class="content-padding">
                                                - Giải nhất (1 giải/đội): Người chơi có câu trả lời đúng nhiều nhất và nhanh nhất trong bảng xếp hạng sẽ nhận được phần thưởng là 1 chiếc máy tính bảng IPAD 10.2 inch Wifi 32GB (2019) 
                                                <br />
                                                - Giải nhì (2 giải/đội): Người chơi có câu trả lời đúng nhiều và nhanh thứ 2, thứ 3 trong bảng xếp hạng sẽ nhận được phần thưởng là 1 chiếc đồng hồ Xiaomi Fitbit Versa Lite
                                                <br />
                                                - Giải ba (3 giải/đội): Người chơi có câu trả lời đúng nhiều và nhanh thứ 4, thứ 5, thứ 6 trong bảng xếp hạng sẽ nhận được phần thưởng là một va li kéo Morinaga
                                                <br/>
                                                - Giải Khuyến khích (Không giới hạn số lượng): Tất cả những người chơi có số câu trả lời đúng trên 80% sẽ nhận được combo quà tặng là Bút chì + Sổ tay Morinaga
                                                <br/>
                                                Các cá nhân đạt giải sẽ được tuyên dương trên bản tin nội bộ, báo Cánh Sen.
                                            </div>
                                            <br />
                                            <b>9. Cách thức công bố giải và cách thức nhận giải</b> <br />
                                            <div class="content-padding">
                                                + Ngày 15/04/2020 Ban tổ chức sẽ công bố danh sách những nhân viên đạt giải
                                                <br/>
                                                + Danh sách đạt giải sẽ được cập nhật trên website: https://chienbinh3.morinagamilk.com.vn/
                                                <br/>
                                                + Giải thưởng sẽ được trao tới người đạt giải thông qua sự phối hợp của Team Morinaga và phòng Kinh Doanh
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-prize" id="thong-tin-giai-thuong">
                            <div class="wrapper-prize-kien-thuc">
                                <h2 class="wrapper-prize__title">
                                    <img src="<?php bloginfo('template_directory'); ?>/images/mori/prize-title.png" alt="">
                                </h2>
                                <img class="wrapper-prize__picture-2" src="<?php bloginfo('template_directory'); ?>/images/mori/prize-laptop-5.png" />
                                <div class="hide-mb anm">
                                    <div class="wrapper-prize__content-right">
                                        <b class="rule_bold"> GIẢI THƯỞNG </b>
                                        <br /><br />
                                        <b>3 GIẢI NHẤT</b>
                                        <p class="prize_money">IPAD 10.2 inch <br/> Wifi 32GB (2019)</p>
                                        <b>6 GIẢI NHÌ</b>
                                        <p class="prize_money">Đồng hồ Xiaomi <br/> Fitbit Versa Lite</p>
                                        <b>9 GIẢI BA</b>
                                        <p class="prize_money">Va li kéo Morinaga</p>
                                    </div>
                                    <div class="wrapper-prize__content-left">
                                        <b class="rule_bold"> GIẢI KHUYẾN KHÍCH </b>
                                        <br /><br />
                                        <b class="rule_bold">KHÔNG GIỚI HẠN: </b>
                                        <br />
                                        <b class="rule_bold">+ Bút chì</b>
                                        <br />
                                        <b class="rule_bold">+ Sổ tay Morinaga</b>
                                        <br />
                                    </div>
                                </div>
                                <div class="hide-desk">
                                    <div class="wrapper-prize__content-mb">
                                        GIẢI ĐỒNG ĐỘI <br />
                                        1 GIẢI NHẤT: <br />
                                        IPAD 10.2 inch Wifi 32GB (2019) <br/>
                                        2 GIẢI NHÌ: <br />
                                        Đồng hồ Xiaomi Fitbit Versa Lite <br/>
                                        3 GIẢI BA: <br/>Va li kéo Morinaga  <br /><br />
                                        GIẢI KHUYẾN KHÍCH
                                        <br />
                                        KHÔNG GIỚI HẠN:
                                        <br />
                                        combo quà tặng là <b>Bút chì</b> + <b>Sổ tay Morinaga</b>
                                    </div>
                                </div>
                                <ul class="cat-nav cat-new-style">
                                    <li class="tab-kien-thuc active" data-href="#cat_ta_quan">Thi Kiến Thức</li>
                                    <li class="tab-trung-bay" data-href="#cat_ta_dan">Thi Trưng Bày</li>
                                </ul>
                            </div>
                        </div>
                        <div class="container" id="cuoc-thi-kien-thuc">
                            <div class="container-90">
                                <div class="library-story">
                                    <h2 class="wrapper-library__title-tai-lieu">
                                        <img src="<?php bloginfo('template_directory'); ?>/images/mori/cuoc-thi-kien-thuc.png" alt="">
                                    </h2>
                                    <ul class="nav nav-tabs tab-cuoc-thi-ket-thuc" role="tablist" id="tabs-kien-thuc">
                                        <li class="nav-item active">
                                            <a class="nav-link active" data-toggle="tab" href="#home">TÀI LIỆU THAM KHẢO</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#menu1">THI NGAY</a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div id="home" class="tab-pane active"><br>
                                            <a id="link_tltk" href="https://onedrive.live.com/?authkey=%21AHyLlq6ABuxVnr8&cid=C99AD410712CB0F0&id=C99AD410712CB0F0%2144065&parId=C99AD410712CB0F0%2144064&o=OneUp" target="_blank"><img src="<?php bloginfo('template_directory');?>/images/mori/tham-khao-video.jpg"/></a>
                                            <h2 class="wrapper-library__title-tai-lieu">
                                                <img src="<?php bloginfo('template_directory'); ?>/images/mori/bang-xep-hang_title.png" alt="">
                                            </h2>
                                            <div class="bxh-filter">
                                                <div class="form-group">
                                                    <!-- <input type="text" class="form-control form-custom" id="quiz_phone_filter" name="quiz_phone_filter" placeholder="Tìm theo số điện thoại"> -->
                                                    <select id="quiz_team_filter" name="quiz_team_filter" class="form-control form-custom minimal">
                                                        <option selected value="-1">Chọn Đội</option>
                                                        <option v-for="team in list_quiz_team" :value="team.team_id">{{team.team}}</option>
                                                    </select>
                                                </div>
                                                <p class="err-filter-msg"></p>
                                            </div>
                                            <div class="wrapper-desktop-bxh">
                                            
                                            <?php 
                                            global $wpdb;
                                                $top_quiz = $wpdb->get_results('SELECT mr_quiz_info.*, m.user_id, m.finish_time_format AS finish_time_format ,m.finish_time , m.total_correct AS Total_correct,
                                                    q_area.area_name, q_team.team AS team_name, m.created_at
                                                FROM mr_quiz_info
                                                LEFT JOIN mr_quiz_user_contest m ON mr_quiz_info.id = m.user_id 
                                                LEFT JOIN mr_teams AS q_area ON mr_quiz_info.quiz_area = q_area.area
                                                LEFT JOIN mr_teams AS q_team ON mr_quiz_info.quiz_team = q_team.team_id
                                                WHERE mr_quiz_info.status = 1 AND Total_correct > 0 
                                                -- AND q_area.area_name IS NOT NULL
                                                AND Total_correct in (SELECT MAX(m1.total_correct) as total_correct FROM mr_quiz_user_contest m1 WHERE m1.user_id = m.user_id)
                                                AND finish_time_format in (SELECT MIN(m2.finish_time_format) as finish_time_format FROM mr_quiz_user_contest m2 WHERE m2.user_id = m.user_id AND Total_correct in (SELECT MAX(m1.total_correct) as total_correct FROM mr_quiz_user_contest m1 WHERE m1.user_id = m.user_id) )
                                                -- AND m.created_at in (SELECT MIN(m3.created_at) as created_at FROM mr_quiz_user_contest m3 WHERE m3.user_id = m.user_id AND Total_correct in (SELECT MAX(m4.total_correct) as Total_correct FROM mr_quiz_user_contest m4 WHERE m4.user_id = m.user_id) AND finish_time_format in (SELECT MIN(m5.finish_time_format) as finish_time_format FROM mr_quiz_user_contest m5 WHERE m5.user_id = m.user_id))
                                                Group by m.user_id
                                                ORDER BY Total_correct DESC, finish_time_format ASC, m.created_at ASC
                                                LIMIT 0,20',ARRAY_A);
                                            ?>
                                                <ul class="bxh-list bxh-list-title">
                                                    <li>THỨ TỰ</li>
                                                    <li class="bxh-list-text-left">TÊN</li>
                                                    <li>KHU VỰC</li>
                                                    <li>ĐỘI</li>
                                                    <li>THỜI GIAN</li>
                                                    <li>SỐ CÂU TRẢ LỜI CHÍNH XÁC</li>
                                                </ul>
                                                <div class="bxh-list-content">
                                                <?php 
                                                    for ($i=0;$i < count($top_quiz); $i++) {
                                                        ?>
                                                        <ul class="bxh-list">
                                                        <li><?php echo ($i+1)?></li>
                                                        <li class="bxh-list-text-left">
                                                            <?php echo $top_quiz[$i]['quiz_name']?>
                                                        </li>
                                                        <li>
                                                            <?php echo $top_quiz[$i]['area_name']?>
                                                        </li>
                                                        <li>
                                                            <?php echo $top_quiz[$i]['team_name']?>
                                                        </li>
                                                        <li><?php echo $top_quiz[$i]['finish_time']?></li>
                                                        <li><?php echo $top_quiz[$i]['Total_correct']?></li>
                                                    </ul>
                                                        <?php
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                            <div class="wrapper-mb-bxh">
                                            <?php 
                                                    for ($i=0;$i < count($top_quiz); $i++) {
                                                        ?>
                                                        <div class="wrapper-mb-bxh_item">
                                                        <ul class="bxh-list bxh-list-title-mb">
                                                            <li>THỨ TỰ</li>
                                                            <li><?php echo ($i+1)?></li>
                                                        </ul>
                                                        <ul class="bxh-list bxh-list-title-mb">
                                                        <li>TÊN</li>
                                                        <li><?php echo $top_quiz[$i]['quiz_name']?></li>
                                                        </ul>
                                                        <ul class="bxh-list bxh-list-title-mb">
                                                        <li>KHU VỰC</li>
                                                        <li><?php echo $top_quiz[$i]['area_name']?></li>
                                                        </ul>
                                                        <ul class="bxh-list bxh-list-title-mb">
                                                        <li>ĐỘI</li>
                                                        <li><?php echo $top_quiz[$i]['team_name']?></li>
                                                        </ul>
                                                        <ul class="bxh-list bxh-list-title-mb">
                                                        <li>THỜI GIAN</li>
                                                        <li><?php echo $top_quiz[$i]['finish_time']?></li>
                                                        </ul>
                                                        <ul class="bxh-list bxh-list-title-mb">
                                                        <li>SỐ CÂU TRẢ LỜI CHÍNH XÁC</li>
                                                        <li><?php echo $top_quiz[$i]['Total_correct']?></li>
                                                        </ul>
                                                        </div>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <div id="menu1" class="tab-pane fade">
                                            <!-- Quiz -->
                                            <div id="quiz_step_alert" style="display:none">
                                                <h4>Thi Trắc Nghiệm Kiến Thức</h4>
                                                <p>
                                                    Thời gian tham gia cuộc thi:
                                                    <br />
                                                    Từ ngày 02/03 đến ngày 05/03/2020
                                                </p>
                                                <a class="buy-btn buy-btn-mb quiz-btn" href="<?php bloginfo('template_directory');?>/video/" target="_blank" id="btn_tai_">Tài liệu tham khảo</a>
                                            </div>
                                            <div id="quiz_step_1">
                                                <h4>Thi Trắc Nghiệm Kiến Thức</h4>
                                                <p> Bạn có 2 lượt tham gia với thời gian với mỗi lượt trong vòng 5 phút</p>
                                                <p>Chúc các chiến binh gặp nhiều may mắn</p>
                                                <a class="btn-quiz_step_1 quiz-btn buy-btn buy-btn-mb" href="javascript:;">Bắt Đầu Thi</a>
                                            </div>
                                            <div id="quiz_step_2" class="hide">
                                                <h4>Đăng Ký Thông Tin</h4>
                                                <div class="quiz-info-wrapper">
                                                    <form action="" method="post" enctype="multipart/form-data">
                                                        <div class="quiz_register_form">
                                                            <div class="row-form">
                                                                <div class="form-group">
                                                                    <select id="quiz_group" name="quiz_group" v-model="quiz_group" class="form-control form-custom minimal" @change="getAreaQuiz(quiz_group)">
                                                                        <option selected value="-1">Chọn nhóm đối tượng</option>
                                                                        <option value="1">Nhân viên kinh doanh Morinaga</option>
                                                                        <option value="2">Nội bộ Lê Mây (trừ kinh doanh Morinaga)</option>
                                                                        <option value="3">Nhà phân phối, Đại Lý, Buyers của BS, MT, MC, OTC, EC</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <select id="quiz_area" name="quiz_area" v-model="quiz_area" class="form-control form-custom minimal" @change="getGroup(quiz_area)">
                                                                        <option selected value="-1">Khu Vực</option>
                                                                        <option v-for="area in list_area" :value="area.area_id">{{area.area_name}}</option>
                                                                    </select>
                                                                </div>
                                                                <div v-if="quiz_area != -1 && (quiz_group == 1)" class="form-group">
                                                                    <select id="quiz_team" name="quiz_team" v-model="quiz_team" class="form-control form-custom minimal">
                                                                        <option selected value="-1">Chọn Đội</option>
                                                                        <option v-for="team in list_team_quiz" :value="team.team_id">{{team.team}}</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group" v-if="quiz_group == 3">
                                                                    <input type="text" class="form-control form-custom" id="npp_name" name="npp_name" placeholder="Tên Nhà Phân Phối">
                                                                </div>
                                                                <div class="form-group" v-if="quiz_group == 4">
                                                                    <input type="text" class="form-control form-custom" id="cch_name" name="cch_name" placeholder="Tên Cửa Hàng">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-custom" id="quiz_name" name="quiz_name" placeholder="Họ Tên">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-custom" id="quiz_phone" name="quiz_phone" placeholder="Phone">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-custom" id="quiz_email" name="quiz_email" placeholder="Email">
                                                                </div>
                                                            </div>
                                                            <p class="quiz-err-msg"></p>
                                                            <input type="hidden" id="quiz_res_id" name='quiz_res_id' value="0">
                                                            <a id="btn_quiz_step_2" class="quiz-btn buy-btn buy-btn-mb" href="javascript:;">Đăng Ký</a>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div id="quiz_step_3">
                                                <div class="timeline-wrapper">
                                                </div>
                                                <div class="quiz-test">
                                                </div>
                                            </div>
                                            <div id="quiz_step_4">
                                                <div id="quiz_step_4_result">
                                                </div>
                                                <p><i>Tải lại trang và đăng ký lại cùng thông tin để thi lần 2!</i></p>
                                                <div id="btn_bxh" class="btn-answer btn-answer-finish"> Bảng Xếp Hạng </div>
                                            </div> 
                                            <input type="hidden" id="url_admin_ajax" name='url_admin_ajax' value="<?php echo admin_url('admin-ajax.php'); ?>">
                                            <!-- END Quiz-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Câu Chuyện Chia Sẻ-->
            </div>
        </div>
    </section>
    <!-- END section Library-->
</div>
<!-- END main -->
<!-- START scroll to TOP-->
<a href="javascript:;" id="scrollTop">
    <i class="fa fa-chevron-circle-up" aria-hidden="true"></i>
</a>
<!-- END scroll to TOP-->
<!-- START Float Menu-->
<div id="floating_btns" class="floating-btns" style="display:none">
    <img class="register-btn" data-toggle="modal" data-target="#registerModal" src="<?php bloginfo('template_directory'); ?>/images/mori/register-btn.png" alt="">
</div>
<!-- END Float Menu-->
<!-- START popup image-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span id="img_popup_title"> </span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="model-list-image">
                <!-- <div class="list-picture-popup"> -->
            </div>
        </div>
    </div>
</div>
<!-- END popup image-->
<!-- START popup content-->
<div class="modal fade" id="modalShowStory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span id="img_popup_story_title"> </span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="content_popup_detail">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- END popup content-->
<!--START register -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <img class="regist-img" src="<?php bloginfo('template_directory'); ?>/images/mori/form.png" alt="Banner">
                <!-- register info-->
                <div class="register-info register-show">
                    <div class="register-info-wrapper">
                        <p class="register-picture_right-content_title">Đăng Ký Thông Tin Của Bạn</p>
                        <form action="" method="post" enctype="multipart/form-data">
                            <div class="register_form">
                                <div class="row-form">
                                    <div class="form-group">
                                        <select id="area" name="area" v-model="area" class="form-control form-custom minimal" @change="getArea(area)">
                                            <option selected value="-1">Khu Vực</option>
                                            <option value="1">Miền Bắc</option>
                                            <option value="2">Miền Nam</option>
                                        </select>
                                    </div>
                                    <div v-if="area != -1" class="form-group">
                                        <select id="team_area" name="team_area" class="form-control form-custom minimal">
                                            <option selected value="-1">Chọn Đội</option>
                                            <option v-for="team in list_team" :value="team.team_id">{{team.team}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-custom" id="username" name="username" placeholder="Họ Tên">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-custom" id="email" name="email" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-custom" id="phone" name="phone" placeholder="Phone">
                                    </div>
                                    <p class="err-msg"></p>
                                    <input type="hidden" id="res_id" name='res_id' value="0">
                                </div>
                            </div>

                        </form>
                        <a id="step_1_btn" href="javascript:;" class="register-info-btn register-info-btn-center" tabindex="0">Đăng Ký</a>
                    </div>
                </div>
                <!-- END register info-->
                <!-- register picture-->
                <div class="register-picture">
                    <div class="register-picture-wrapper">
                        <!-- <div class="hide-mb anm">-->
                        <div class="anm">
                            <div class="register-picture_left-content">
                                <img src="<?php bloginfo('template_directory'); ?>/images/mori/register-bear.png" alt="">
                            </div>
                            <div class="register-picture_preview">

                            </div>
                        </div>
                        <div class="register-picture_right-content">
                            <p class="register-picture_right-content_title">Chọn ngay từ 2-4 hình ảnh dự thi của bạn!</p>
                            <form id="msform" action="#" method="post" enctype="multipart/form-data">
                                <input type="text" name="ablum_name" class="form-control input-circle" id="ablum_name" placeholder="Tên album của bạn" />
                                <label for="main_image" class="register-picture_right-content_filecustom">
                                    <img for="main_image" id="img_upload" class="register-picture_right-content_upload" src="<?php bloginfo('template_directory'); ?>/images/mori/register-upload.png" alt="">
                                </label>
                                <!-- <input type="file" name="main_image[]" id="main_image"  multiple="true" value="" accept=".png, .jpg, .jpeg" onchange="loadFile(event)"/> -->
                                <input type="file" name="main_image[]" id="main_image" multiple="true" value="" accept="image/*" />
                            </form>
                            <p class="err-upload-msg"></p>
                            <a id="step_2_btn" href="javascript:;" class="register-info-btn register-info-btn-center" tabindex="0">Upload</a>
                        </div>
                    </div>
                </div>
                <!-- END register picture-->
                <!-- register content-->
                <div class="register-content">
                    <div class="register-content-wrapper">
                        <div class="hide-mb anm">
                            <div class="register-content_left-content">
                                <img src="<?php bloginfo('template_directory'); ?>/images/mori/register-bear.png" alt="">
                            </div>
                        </div>
                        <div class="register-content_right-content">
                            <p class="register-content_right-content_title">Bạn đã có bộ ảnh đẹp rồi, <br />hãy chia sẻ câu chuyện hay nào!</p>
                            <form>
                                <div class="row-form">
                                    <div class="form-group">
                                        <textarea class="form-control" id="your_story" name="your_story" rows="8" placeholder="Câu chuyện của bạn"></textarea>
                                    </div>
                                </div>
                            </form>
                            <p class="err-story-msg"></p>
                            <div class="register-content_btn_wrapper">
                                <a id="step_3_btn" href="javascript:;" class="register-info-btn register-content_btn-content" tabindex="0">Chia Sẻ Ngay</a>
                                <a id="step_4_btn" href="javascript:;" class="register-info-btn register-content_btn-content" tabindex="0">Làm Lại</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END register content-->
                <!-- register success-->
                <div class="register-success">
                    <div class="register-success-wrapper">
                        <div class="hide-mb anm">
                            <div class="register-content_left-content">
                                <img src="<?php bloginfo('template_directory'); ?>/images/mori/register-bear.png" alt="">
                            </div>
                        </div>
                        <div class="register-content_right-content">
                            <p class="register-success_right-content_title">Chúc mừng bạn đã trở thành 1 trong những chiến binh Morinaga dũng mãnh!</p>
                            <a id="step_6_btn" href="/" class="register-info-btn register-success_btn register-info-btn-center" tabindex="0">Về Trang Chủ</a>
                        </div>
                    </div>
                </div>
                <!-- END register success-->
            </div>
        </div>
    </div>
</div>
<!--END register-->
<script type="text/javascript">
    var loadFile = function(event) {
        var output = document.getElementById('preview_img');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
    (function($) {
        $(document).ready(function() {
            $('form').on('keyup', function(e) {
                $("p.err-msg").html('');
                $("p.err-upload-msg").html('');
                $("p.err-story-msg").html('');
            });
            // Multiple images preview in browser
            var imagesPreview = function(input, placeToInsertImagePreview) {
                var filesToUpload = [];
                if (input.files) {
                    var filesAmount = input.files.length;
                    console.log(filesAmount);
                    $("p.err-upload-msg").html('');
                    $('.register-picture_preview').html('');
                    if (filesAmount > 4) {
                        $('.register-picture_left-content').removeClass('register-hide');
                        $('.register-picture_left-content').addClass('register-show');
                        $("p.err-upload-msg").html('Vui lòng chọn nhiều nhất 4 hình ảnh.');
                    }else if (filesAmount < 2) {
                        $('.register-picture_left-content').removeClass('register-hide');
                        $('.register-picture_left-content').addClass('register-show');
                        $("p.err-upload-msg").html('Vui lòng chọn ít nhất 2 hình ảnh.');
                    } else {
                        $('.register-picture_left-content').addClass('register-hide');
                        $('.register-picture_left-content').removeClass('register-show');
                        for (i = 0; i < filesAmount; i++) {
                            var reader = new FileReader();
                            reader.onload = function(event) {
                                $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            }
                            reader.readAsDataURL(input.files[i]);
                        }
                    }
                }
            };
            $('#main_image').on('change', function() {
                imagesPreview(this, 'div.register-picture_preview');
            });

            $('#btn_tai_').click(function () {
                $('#link_tltk').show();
                $('#tabs-kien-thuc li:nth-of-type(2)').removeClass('active');
                $('#menu1').removeClass('active in');
                $('#tabs-kien-thuc li:nth-of-type(2)').attr("aria-expanded","false");
                $('#tabs-kien-thuc li:first-of-type').addClass('active');
                $('#home').addClass('active in');
                $('#tabs-kien-thuc li:first-of-type').attr("aria-expanded","true");
            });

            $('.btn-quiz_step_1').click(function () {
                $('#quiz_step_2').removeClass('hide');
                $('#quiz_step_1').addClass('hide');
                $('#quiz_step_2').focus();
            });
            $('#btn_bxh').click(function () {
                $('#link_tltk').hide();
                $('#tabs-kien-thuc li:nth-of-type(2)').removeClass('active');
                $('#menu1').removeClass('active in');
                $('#tabs-kien-thuc li:nth-of-type(2)').attr("aria-expanded","false");
                $('#tabs-kien-thuc li:first-of-type').addClass('active');
                $('#home').addClass('active in');
                $('#tabs-kien-thuc li:first-of-type').attr("aria-expanded","true");
            });

        });
    })(jQuery)

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validatePhone(phone) {
        var re = /^[0]{1}[0-9]{9,10}$/;
        return re.test(phone);
    }
</script>
<?php get_footer(); ?>