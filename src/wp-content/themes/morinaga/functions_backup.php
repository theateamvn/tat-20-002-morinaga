<?php
/**
 * back end
 */
include( plugin_dir_path( __FILE__ ) . 'ilc_settings_page.php');
// Update CSS within in Admin
function admin_style() {
    wp_enqueue_style('admin-styles', get_template_directory_uri().'/css/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');
/**
 * START #function
 *  */
// register info
add_action( 'wp_ajax_submitInfo', 'submitInfo_init' );
add_action( 'wp_ajax_nopriv_submitInfo', 'submitInfo_init' );
function submitInfo_init() {
    // xử lý hàm
    $result = array();
    $username           = $_REQUEST['username'];
    $area               = $_REQUEST['area'];
    $team_area          = $_REQUEST['team_area'];
    $email              = $_REQUEST['email'];
    $position           = $_REQUEST['position'];
    // check email uique
    global $wpdb;
    $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->users WHERE user_email = %s AND user_status = 1", $email)); // get_results
    if($count == 1){
        $result= [
            "success" => false,
            "message" => '(*) Email này đã tham gia rồi!'
        ];
    } else {
        $userdata = array(
            'user_login'    =>   $email.'-'.current_time('timestamp', 1 ),
            'user_email'    =>   $email
        );
        $id = wp_insert_user( $userdata );
        if(isset($id->errors)){
            if(isset($id->errors['existing_user_email']) || isset($id->errors['existing_user_login'])) {
                $id_ = email_exists($email);
                $userdata_ = array(
                    'ID' => $id_,
                    //'user_login'    =>   $username.'-'.current_time('timestamp', 1 ),
                    'user_email'    =>   $email.'-'.current_time('timestamp', 1 )
                );
                $update = wp_update_user($userdata_);
                // create new user
                $userdata = array(
                    'user_login'    =>   $email.'-'.current_time('timestamp', 1 ),
                    'user_email'    =>   $email
                );
                $id_new = wp_insert_user( $userdata );
                // update tên đội và vị trí
                update_user_meta($id_new, 'position', $position);
                update_user_meta($id_new, 'first_name', $username);
                update_user_meta($id_new, 'team_area', $team_area);
                update_user_meta($id_new, 'area', $area);
                update_user_meta($id_new, 'fullname_show', $username);
                $result= [
                    "success" => true,
                    "message" => 'Đăng Ký Thành Công',
                    'id'      => $id_new
                ];
            } else {
                $result= [
                    "success" => false,
                    "message" => 'Xảy ra lỗi trong quá trình đăng ký!'
                ];
            }
            
        } else {
            //update_user_meta($user, 'img', $img);
            update_user_meta($id, 'fullname_show', $username);
            update_user_meta($id, 'position', $position);
            update_user_meta($id, 'first_name', $username);
            update_user_meta($id, 'team_area', $team_area);
            update_user_meta($id, 'area', $area);
            
            $result= [
                "success" => true,
                "message" => 'Đăng Ký Thành Công',
                'id'      => $id
            ];
        }
    }
    echo json_encode($result);
    die();//bắt buộc phải có khi kết thúc
}
// upload img
add_action( 'wp_ajax_submitUpload', 'submitUpload_init' );
add_action( 'wp_ajax_nopriv_submitUpload', 'submitUpload_init' );
function submitUpload_init() {
    $id           = $_REQUEST['id'];
    $result = array();
    require( dirname(__FILE__) . '/../../../wp-load.php' );
    $wordpress_upload_dir = wp_upload_dir();

    $imgs = $_FILES['main_image'];
    //echo count($imgs);die;
    foreach ( $_FILES['main_image']['name'] as $f => $name ) {
       // echo $f;die;
        $extension = pathinfo( $name, PATHINFO_EXTENSION );
        // Generate new file
        $new_filename = $id.'-'.generate_random_code( 20 )  . '.' . $extension;
        $new_file_path = $wordpress_upload_dir['path'] . '/' . $new_filename;
        $new_file_mime = mime_content_type( $_FILES["main_image"]["tmp_name"][$f] );
        if( move_uploaded_file( $_FILES["main_image"]["tmp_name"][$f], $new_file_path ) ) {
            $upload_id = wp_insert_attachment( array(
            'guid'           => $new_file_path,
            'post_mime_type' => $new_file_mime,
            'post_title'     => preg_replace( '/\.[^.]+$/', '', $new_filename ),
            'post_content'   => '',
            'post_status'    => 'inherit'
            ), $new_file_path );
            update_user_meta($id, 'img_id_'.($f+1), $upload_id);
            // wp_generate_attachment_metadata() won't work if you do not include this file
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            
            // Generate and save the attachment metas into the database
            wp_update_attachment_metadata( $upload_id, wp_generate_attachment_metadata( $upload_id, $new_file_path ) );
            
            // Show the uploaded file in browser
            wp_redirect( $wordpress_upload_dir['url'] . '/' . basename( $new_file_path ) ); 
        }
        //$new_file_path=str_replace('/var/www/public/tat-19-019-morinaga/src','http://dev.morinaga.com.vn',$new_file_path);
        //$new_file_path=str_replace('/var/www/public/tat-19-019-morinaga/src','',$new_file_path);
       //update_user_meta($id, 'img', $new_file_path);
        
    }
    return true;
    die();
}
function generate_random_code($length=10) {
    $string = '';
    $characters = "23456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";
    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters)-1)];
    }
    return $string;
 }
// submit story
add_action( 'wp_ajax_submitStory', 'submitStory_init' );
add_action( 'wp_ajax_nopriv_submitStory', 'submitStory_init' );
function submitStory_init() {
    // xử lý hàm
    $result = array();
    $id           = $_REQUEST['id'];
    $your_story           = $_REQUEST['your_story'];
    update_user_meta($id, 'your_story', $your_story);
    // update status user
    $updateData = array(
        'ID' => $id,
        'user_status' => 1
    );
    global $wpdb;
    $wpdb->query('UPDATE mr_users SET user_status = 1 WHERE ID = '.$id);
    /*$update = wp_update_user($updateData);*/
    $result= [
        "success" => true,
        "message" => 'User updated',
        'id'      => $id
    ];
    echo json_encode($result);
    die();//bắt buộc phải có khi kết thúc
}
