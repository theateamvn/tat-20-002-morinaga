<?php

/**
 * back end
 */
include(plugin_dir_path(__FILE__) . 'ilc_settings_page.php');
// Update CSS within in Admin
function admin_style()
{
    wp_enqueue_style('admin-styles', get_template_directory_uri() . '/css/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');
/**
 * START #function
 *  */
// register info
add_action('wp_ajax_submitInfo', 'submitInfo_init');
add_action('wp_ajax_nopriv_submitInfo', 'submitInfo_init');
function submitInfo_init()
{
    // xử lý hàm
    $result = array();
    $username           = $_REQUEST['username'];
    $area               = $_REQUEST['area'];
    $team_area          = $_REQUEST['team_area'];
    $email              = $_REQUEST['email'];
    $phone              = $_REQUEST['phone'];
    //$position           = $_REQUEST['position'];
    // check email uique
    global $wpdb;
    //$count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM mr_register_info WHERE email = %s AND status = 1", $email));

    $user_data = array(
        'user_name' => $username,
        'email'     => $email,
        'phone'     => $phone,
        'area'      => $area,
        'team_area' => $team_area,
        // 'position'  => $position
    );
    //$check_user = $wpdb->get_results($wpdb->prepare("SELECT * FROM mr_register_info WHERE email = %s", $email));
    $check_user = $wpdb->get_results($wpdb->prepare("SELECT * FROM mr_register_info WHERE phone = %s or email = %s", $phone,$email));
    if (count($check_user) == 0) {
        $wpdb->insert('mr_register_info', $user_data);
        $id = $wpdb->insert_id;
    } else {
        $wpdb->update('mr_register_info', $user_data, array('id' => $check_user[0]->id));
        $id = $check_user[0]->id;
    }
    $result = [
        "success" => true,
        "message" => 'Đăng kí thành viên thành công',
        "id"      => $id
    ];
    echo json_encode($result);
    die(); //bắt buộc phải có khi kết thúc
}
// upload img
add_action('wp_ajax_submitUpload', 'submitUpload_init');
add_action('wp_ajax_nopriv_submitUpload', 'submitUpload_init');
function submitUpload_init()
{
    $id           = $_REQUEST['id'];
    $ablum_name   = $_REQUEST['ablum_name'];
    $group_image = "";
    $result = array();
    global $wpdb;
    require(dirname(__FILE__) . '/../../../wp-load.php');
    $wordpress_upload_dir = wp_upload_dir();

    $imgs = $_FILES['main_image'];
    foreach ($_FILES['main_image']['name'] as $f => $name) {
        $extension = pathinfo($name, PATHINFO_EXTENSION);
        
        // Generate new file
        $new_filename = $id . '-' . generate_random_code(20)  . '.' . $extension;
        $new_file_path = $wordpress_upload_dir['path'] . '/' . $new_filename;
        //$new_file_mime = mime_content_type($_FILES["main_image"]["tmp_name"][$f]);
        $filetype = wp_check_filetype( basename( $new_filename ), null );
        if (move_uploaded_file($_FILES["main_image"]["tmp_name"][$f], $new_file_path)) {
            $upload_id = wp_insert_attachment(array(
                'guid'           => $new_file_path,
                //'post_mime_type' => $new_file_mime,
                'post_mime_type' => $filetype['type'],
                'post_title'     => preg_replace('/\.[^.]+$/', '', $new_filename),
                'post_content'   => '',
                'post_status'    => 'inherit'
            ), $new_file_path);
            $group_image = $group_image . $upload_id . ';';
            // wp_generate_attachment_metadata() won't work if you do not include this file
            require_once(ABSPATH . 'wp-admin/includes/image.php');

            // Generate and save the attachment metas into the database
            wp_update_attachment_metadata($upload_id, wp_generate_attachment_metadata($upload_id, $new_file_path));

            // Show the uploaded file in browser
            //wp_redirect($wordpress_upload_dir['url'] . '/' . basename($new_file_path));
        }
        //$new_file_path=str_replace('/var/www/public/tat-19-019-morinaga/src','http://dev.morinaga.com.vn',$new_file_path);
        //$new_file_path=str_replace('/var/www/public/tat-19-019-morinaga/src','',$new_file_path);
        //update_user_meta($id, 'img', $new_file_path);

    }
    $group_image = substr($group_image, 0, strlen($group_image) - 1);
    $image_data = array(
        'info_id'    => $id,
        'album_name' => $ablum_name,
        'group_image' => $group_image,
    );
    //print_r($image_data);
    $wpdb->insert('mr_register_image', $image_data);
    // update status 
    $user_data_update = array(
        'status' => 1,
    );
    $wpdb->update('mr_register_info', $user_data_update, array('id' => $id));
    return true;
    die();
}
function generate_random_code($length = 10)
{
    $string = '';
    $characters = "23456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";
    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }
    return $string;
}
// submit story
add_action('wp_ajax_submitStory', 'submitStory_init');
add_action('wp_ajax_nopriv_submitStory', 'submitStory_init');
function submitStory_init()
{
    // xử lý hàm
    $result = array();
    $id           = $_REQUEST['id'];
    $your_story           = $_REQUEST['your_story'];
    update_user_meta($id, 'your_story', $your_story);
    // update status user
    $updateData = array(
        'id' => $id,
        'user_status' => 1
    );
    global $wpdb;
    $wpdb->query('UPDATE mr_users SET user_status = 1 WHERE id = ' . $id);
    /*$update = wp_update_user($updateData);*/
    $result = [
        "success" => true,
        "message" => 'User updated',
        'id'      => $id
    ];
    echo json_encode($result);
    die(); //bắt buộc phải có khi kết thúc
}
//get list image in ablum
add_action('wp_ajax_submitImage', 'submitImage_init');
add_action('wp_ajax_nopriv_submitImage', 'submitImage_init');
function  submitImage_init()
{
    $album_id = $_REQUEST['album_id'];
    global $wpdb;
    $album = $wpdb->get_row($wpdb->prepare('select * from mr_register_image where id = %d', $album_id));
    $group_image = explode(';', $album->group_image);
    $num = 0;
    $html = '';
    foreach ($group_image as $image_id) {
        $html .= '<div><h2>' . $album->album_name . '</h2><img src="' . get_home_url() . '/wp-content/uploads/' . get_post_meta($image_id, '_wp_attached_file', true) . '" alt="Chi tiết hình ảnh" ></div>';
        //$group_image[$num] = get_home_url().'/wp-content/uploads/'.get_post_meta($image_id,'_wp_attached_file', true);
        //$num++;
    }
    $html = '<div class="owl-carousel owl-theme">' . $html . '</div>';
    $result = [
        "success"       => true,
        "message"       => 'retrived success',
        // 'list_image'    => $group_image,
        // 'album_name'    => $album->album_name
        'html'          => $html
    ];
    echo json_encode($result);
    die();
}

add_action('wp_ajax_paginateInfo', 'paginateInfo_init');
add_action('wp_ajax_nopriv_paginateInfo', 'paginateInfo_init');
function  paginateInfo_init()
{
    $paginate_id = $_REQUEST['paginate_id'];
    global $wpdb;
    $offset = ($paginate_id - 1) * 2;
    if ($paginate_id == 1) {
        $offset = 0;
    }
    $limit = 2;
    $users = $wpdb->get_results($wpdb->prepare('select * from mr_register_info ORDER BY id DESC limit %d,%d', $offset, $limit), ARRAY_A);
    $list_user = array();
    for ($i = 0; $i < count($users); $i++) {
        $albums = $wpdb->get_results($wpdb->prepare('select * from mr_register_image where info_id = %d ', $users[$i]['id']), ARRAY_A);
        $list_album = array();
        for ($y = 0; $y < count($albums); $y++) {
            //$list_image = array();
            $group_image_id = explode(';', $albums[$y]['group_image']);
            // for ($z=0 ;$z < count($group_image_id); $z++) {
            //     $list_image[$z] = get_home_url().'/wp-content/uploads/'.get_post_meta($group_image_id[$z],'_wp_attached_file', true);
            // }
            $albums[$y]['image'] = get_home_url() . '/wp-content/uploads/' . get_post_meta($group_image_id[0], '_wp_attached_file', true);
            $users[$i]['list_album'][$y] = $albums[$y];
        }
        $list_user[$i] = $users[$i];
    }

    $users = $wpdb->get_results($wpdb->prepare('select * from mr_register_info WHERE status = 1 ORDER BY id DESC limit %d,%d', $offset, $limit));
    $html = '';
    foreach ($users as $user) {
        $html .=
            '<li><div class="library-story-img">'
            . '<img src="'.get_home_url().'/wp-content/themes/morinaga/images/mori/user-library-story-1.png" />'
            . '<p>' . $user->user_name . '</p>'
            . '</div><div class="library-story-slideshow">';
        $albums = $wpdb->get_results($wpdb->prepare('select * from mr_register_image where info_id = %d ', $user->id));
        foreach ($albums as $album) {
            $group_image_id = explode(';', $album->group_image);
            $html .= '<div class="library-story-slideshow_item"><img src="' . get_home_url() . '/wp-content/uploads/' . get_post_meta($group_image_id[0], '_wp_attached_file', true) . '" data-toggle="modal" data-target="#myModal" class="list_image" data-id="' . $album->id . '" />';
            $html .= '<p>' . $album->album_name . '</p></div>';
        }
    }
    $html = $html . '</div></li>';

    $result = [
        "success"       => true,
        "message"       => 'retrived success',
        'html'          => $html,
        'list_user'     => $list_user
    ];
    echo json_encode($result);
    die();
}
/** đăng ký quiz */
add_action('wp_ajax_submitQuizInfo', 'submitQuizInfo_init');
add_action('wp_ajax_nopriv_submitQuizInfo', 'submitQuizInfo_init');
function  submitQuizInfo_init()
{
    $quiz_group     = $_REQUEST['quiz_group'];
    $quiz_area      = $_REQUEST['quiz_area'];
    $quiz_team      = $_REQUEST['quiz_team'];
    $quiz_name      = $_REQUEST['quiz_name'];
    $quiz_phone     = $_REQUEST['quiz_phone'];
    $quiz_email     = $_REQUEST['quiz_email'];
    $npp_name       = $_REQUEST['npp_name'];
    $cch_name       = $_REQUEST['cch_name'];
    global $wpdb;
    $quiz_info = array(
        'quiz_group'    => $quiz_group,
        'quiz_area'     => $quiz_area,
        'quiz_team'     => $quiz_team,
        'quiz_name'     => $quiz_name,
        'quiz_phone'    => $quiz_phone,
        'quiz_email'    => $quiz_email,
        'npp_name'      => $npp_name,
        'cch_name'      => $cch_name,
        'ip'            => get_client_ip(),
        'created_at'    => date("Y-m-d h:i:s",time())

    );
    $quiz_info_check = $wpdb->get_results($wpdb->prepare("SELECT * FROM mr_quiz_info WHERE quiz_phone = %s", $quiz_phone));
    if(count($quiz_info_check) != 0){
        $check_quiz_count = $wpdb->get_results($wpdb->prepare("SELECT * FROM mr_quiz_user_contest WHERE status = 1 AND user_id = %d", $quiz_info_check[0]->id));
    } else {
        $check_quiz_count = 0;
    }
    /*if ($count == 0 ) {
        $result = [
            "success" => false,
            "message" => '(*) Email này không được tham gia chương trình!'
        ];
    }*/
    // if($quiz_info_count == 1 && $quiz_email != ""){
    //     $result = [
    //         "success" => false,
    //         "message" => '(*) Email này đã tham gia chương trình!'
    //     ];
    // } else
    if(count($check_quiz_count) == 2 ){
        $result = [
            "success" => false,
            "message" => 'Số điện thoại này đã hết lượt tham gia chương trình!'
        ];
    } else {
        $quiz_contest_finish = $wpdb->get_results($wpdb->prepare("SELECT * FROM mr_quiz_user_contest WHERE user_id = %d AND status = 1", $quiz_info_check[0]->id));
        $quiz_contest_update = $wpdb->get_results($wpdb->prepare("SELECT * FROM mr_quiz_user_contest WHERE user_id = %d AND status = 0", $quiz_info_check[0]->id));
        if (count($quiz_info_check) > 0 && count($quiz_contest_update) > 0) {
            $wpdb->update('mr_quiz_info', $quiz_info, array('id' => $quiz_info_check[0]->id));
            $id = $quiz_info_check[0]->id;
        }else if(count($quiz_info_check) > 0 && count($quiz_contest_finish) > 0){
            $wpdb->update('mr_quiz_info', $quiz_info, array('id' => $quiz_info_check[0]->id)); 
            $id = $quiz_info_check[0]->id;
        }else if(count($quiz_info_check) == 0 ) {
            $wpdb->insert('mr_quiz_info', $quiz_info);
            $id = $wpdb->insert_id;
        }
        // create log
        $wpdb->insert('mr_quiz_info_log', $quiz_info);

        $result = [
            "success"       => true,
            "message"       => 'retrived success',
            'id'            => $id
        ];
    }
    echo json_encode($result);
    die();
}
/** End đăng ký quiz */
/** Get question theo user id */
add_action('wp_ajax_getQuestion', 'getQuestion_init');
add_action('wp_ajax_nopriv_getQuestion', 'getQuestion_init');
function getQuestion_init()
{
    // xử lý hàm
    $result = array();
    global $wpdb;
    $user_id           = $_REQUEST['user_id'];
    $start_time           = date("Y-m-d h:i:s",time());
    //get question set nếu record đã được tạo
    $quiz_user_contest = $wpdb->get_results($wpdb->prepare('select * from mr_quiz_user_contest where user_id = %d and status = 0', $user_id));
    $quiz_user_contest_ = $wpdb->get_results($wpdb->prepare('select * from mr_quiz_user_contest where user_id = %d and status = 1', $user_id));
    // get random question set trừ question set nếu có record đã được tạo
    if (count($quiz_user_contest_) > 0) {
        while (true) {
            $quest_set_rand = rand(1, 4);
            if ($quest_set_rand != $quiz_user_contest_[0]->question_set) {
                break;
            }
        }
    } else {
        $quest_set_rand = rand(1, 4);
    }
    $quest_set_rand = 11;
    // tạo record mới gồm user id và question set
    $quiz_user_data = array(
        'user_id'       => $user_id,
        'question_set'  => $quest_set_rand,
        'start_time'    => $start_time,
        'created_at'    => date("Y-m-d h:i:s",time())
    );
    if (count($quiz_user_contest) < 1) {
        $wpdb->insert('mr_quiz_user_contest', $quiz_user_data);
        $id = $wpdb->insert_id;
    } else {
        $wpdb->update('mr_quiz_user_contest', $quiz_user_data, array('user_id' => $quiz_user_contest[0]->id));
        $id = $quiz_user_contest[0]->id;
    }
    // insert log 
    $wpdb->insert('mr_quiz_user_contest_log', $quiz_user_data);
    // lấy list question theo format vuejs
    $question = $wpdb->get_results($wpdb->prepare('select * from mr_question where quest_set = %d and status = 1', $quest_set_rand), ARRAY_A);
    $html = '';
    $html .= '<div id="progressBar">
    <div></div>
    <input type="hidden" id="finish_time" value="192">
    </div>
    <div class="quiz-slideshow">';
    for ($i = 0; $i < count($question); $i++) {
        $html .= '<div class="quiz-test-list" id="wrapper_item_' . ($i + 1) . '"> 
        <div class="quiz-test-item">
        <div class="quiz-test-question">Câu hỏi số ' . ($i + 1) . '</div>
        <div class="quiz-test-title">' . $question[$i]['quest_name'] . '</div>
        <div class="quiz-test-answer">';
        $answer = $wpdb->get_results($wpdb->prepare('select * from mr_answer where quest_id = %d', $question[$i]['id']), ARRAY_A);
        for ($y = 0; $y < count($answer); $y++) {
            $html .= '<div class="radio" data-quest="' . ($i + 1) . '">
            <input type="radio" class="answer_' . ($i + 1) . '" 
            name="answer_' . ($i + 1) . '" id="answer_' . ($i + 1) . '_' . ($y + 1) . '" value="' . $answer[$y]['id'] . '">
            <label class="radio-button-custom-cc radio-button-custom-label" for="answer_' . ($i + 1) . '_' . ($y + 1) . '"></label>
            <label class="question-answer__ans" for="answer_' . ($i + 1) . '_' . ($y + 1) . '">' . $answer[$y]['answer_name'] . '</label>
            </div>';
        }
        $html .= '</div>
        <div class="btn-answer-wrapper">';
        if ($i > 0 && $i+1 <= count($question)) {
            $html .= '
        <div class="btn-next-prev btn-prev btn-answer" data-quest="'.($i-1).'">
        Quay Lại
        </div>';
        }
        if($i+1 < count($question)){
         $html .= '
        <div class="btn-next btn-next-prev btn-answer" data-quest="'.($i+1).'">
        Tiếp Theo
        </div>
        ';   
        }
        if (($i + 1) == count($question)) {
        $html .= '<div id="btn_finish" class="btn-answer btn-answer-finish"> 
        Hoàn Thành</div>';
        }
        $html .='</div>
        </div>
        </div>';
    }
    $html .= '</div><input type="hidden" id="quiz_res_id" name="quiz_res_id" value="0"></div>';
    $html .='<script>
    $(".btn-next-prev").click(function() {
        var current_quest = $(this).attr( "data-quest" );
        var index = parseInt(current_quest);
        let slickObj = $(".quiz-slideshow").slick("getSlick");
        slickObj.slickGoTo(index);
     });
    </script>';
    $result = [
        "success"           => true,
        "message"           => 'User updated',
        "user_id"           => $user_id,
        'quiz_contest_id'   => $id,
        "html"              => $html
    ];
    echo json_encode($result);
    die(); //bắt buộc phải có khi kết thúc
}
/**END  Get question theo user id */
/** submit câu trả lời */
add_action('wp_ajax_submitQuestion', 'submitQuestion_init');
add_action('wp_ajax_nopriv_submitQuestion', 'submitQuestion_init');
function submitQuestion_init()
{
    // xử lý hàm
    $result = array();
    global $wpdb;
    $user_id    = $_REQUEST['user_id'];
    $answer     = $_REQUEST['answer'];
    $end_time   = date("Y-m-d h:i:s",time());
    $finish_time = (300 - $_REQUEST['finish_time']);
    //$finish_time = ceil((300/192)*$finish_time);
    // check câu trả lời đúng với vong for và lưu id câu trả lời 
    $answer_correct = 0;
    $list_answer= '';
    foreach ($answer as $answer_id) {
        $correct = $wpdb->get_results($wpdb->prepare('select * from mr_answer where id = %d and status = 1',$answer_id));
        if($correct[0]->answer_correct == 1){
            $answer_correct++;
        }
        $list_answer .= $answer_id .';';
    }
    // update lại câu trả lời đúng và các câu trả lời, status = 1
    $quiz_user_contest_data = array(
        'total_correct' => $answer_correct,
        'user_answer'   => $list_answer,
        'end_time'      => $end_time,
        'status'        => 1,
        //'finish_time'   => gmdate("i:s", 300-$finish_time),
        'finish_time_format'   => $finish_time,
        'finish_time'   => gmdate("i:s", $finish_time),
        'ip'            => get_client_ip(),
        'updated_at'    => date("Y-m-d h:i:s",time())
    );
    $wpdb->update('mr_quiz_info',array('status' => 1,'updated_at'=>date("Y-m-d h:i:s",time())),array('id'=> $user_id));
    // update lại bảng user status = 1 : đã hoàn thành lượt chơi
    $wpdb->update('mr_quiz_user_contest',$quiz_user_contest_data,array('user_id' => $user_id, 'status' => 0));
    // update log
    $wpdb->update('mr_quiz_user_contest_log',$quiz_user_contest_data,array('user_id' => $user_id, 'status' => 0));
    $quiz_user_contest = $wpdb->get_results($wpdb->prepare('select * from mr_quiz_user_contest where user_id = %d',$user_id));
    $html = '
    <h4>Chúc mừng bạn đã hoàn thành bài thi</h4>
    <p>Thời gian hoàn thành: '.gmdate("i:s", $finish_time).' <br />
    Số Câu Trả Lời Đúng: '.$answer_correct.'</p>'
    ;
    $result = [
        "success"           => true,
        "message"           => 'User updated',
        "correct_answer"    => $answer_correct,
        "html"              => $html
    ];
    echo json_encode($result);
    die(); //bắt buộc phải có khi kết thúc
}
/** END submit câu trả lời */
/** quiz filter */
add_action('wp_ajax_filterQuiz', 'filterQuiz_init');
add_action('wp_ajax_nopriv_filterQuiz', 'filterQuiz_init');
function  filterQuiz_init()
{
    global $wpdb;
    $quiz_phone_filter     = $_REQUEST['phone_filter'];
    if(!$quiz_phone_filter){
        $result = [
            "success"       => false,
            "message"       => 'Vui lòng nhập số điện thoại!'
        ];
    } else {
        $all_quiz = $wpdb->get_results('SELECT mr_quiz_info.*, m.user_id, m.finish_time_format AS finish_time_format ,m.finish_time , m.total_correct AS Total_correct,
                        q_area.area_name, q_team.team AS team_name, m.created_at
                    FROM mr_quiz_info
                    LEFT JOIN mr_quiz_user_contest m ON mr_quiz_info.id = m.user_id 
                    LEFT JOIN mr_teams AS q_area ON mr_quiz_info.quiz_area = q_area.area
                    LEFT JOIN mr_teams AS q_team ON mr_quiz_info.quiz_team = q_team.team_id
                    WHERE mr_quiz_info.status = 1 
                    -- AND mr_quiz_info.quiz_phone = %s
                    AND Total_correct in (SELECT MAX(m1.total_correct) as total_correct FROM mr_quiz_user_contest m1 WHERE m1.user_id = m.user_id)
                    AND finish_time_format in (SELECT MIN(m2.finish_time_format) as finish_time_format FROM mr_quiz_user_contest m2 WHERE m2.user_id = m.user_id AND Total_correct in (SELECT MAX(m1.total_correct) as total_correct FROM mr_quiz_user_contest m1 WHERE m1.user_id = m.user_id) )
                    -- AND m.created_at in (SELECT MIN(m3.created_at) as created_at FROM mr_quiz_user_contest m3 WHERE m3.user_id = m.user_id AND Total_correct in (SELECT MAX(m4.total_correct) as Total_correct FROM mr_quiz_user_contest m4 WHERE m4.user_id = m.user_id) AND finish_time_format in (SELECT MIN(m5.finish_time_format) as finish_time_format FROM mr_quiz_user_contest m5 WHERE m5.user_id = m.user_id))
                    Group by m.user_id
                    ORDER BY Total_correct DESC, finish_time_format ASC, m.created_at ASC', ARRAY_A);
        $search_quiz = $wpdb->get_results($wpdb->prepare('SELECT mr_quiz_info.*, m.user_id, m.finish_time_format AS finish_time_format ,m.finish_time , m.total_correct AS Total_correct,
                        q_area.area_name, q_team.team AS team_name, m.created_at
                    FROM mr_quiz_info
                    LEFT JOIN mr_quiz_user_contest m ON mr_quiz_info.id = m.user_id 
                    LEFT JOIN mr_teams AS q_area ON mr_quiz_info.quiz_area = q_area.area
                    LEFT JOIN mr_teams AS q_team ON mr_quiz_info.quiz_team = q_team.team_id
                    WHERE mr_quiz_info.status = 1 
                    AND mr_quiz_info.quiz_phone = %s
                    AND Total_correct in (SELECT MAX(m1.total_correct) as total_correct FROM mr_quiz_user_contest m1 WHERE m1.user_id = m.user_id)
                    AND finish_time_format in (SELECT MIN(m2.finish_time_format) as finish_time_format FROM mr_quiz_user_contest m2 WHERE m2.user_id = m.user_id AND Total_correct in (SELECT MAX(m1.total_correct) as total_correct FROM mr_quiz_user_contest m1 WHERE m1.user_id = m.user_id) )
                    -- AND m.created_at in (SELECT MIN(m3.created_at) as created_at FROM mr_quiz_user_contest m3 WHERE m3.user_id = m.user_id AND Total_correct in (SELECT MAX(m4.total_correct) as Total_correct FROM mr_quiz_user_contest m4 WHERE m4.user_id = m.user_id) AND finish_time_format in (SELECT MIN(m5.finish_time_format) as finish_time_format FROM mr_quiz_user_contest m5 WHERE m5.user_id = m.user_id))
                    Group by m.user_id
                    ORDER BY Total_correct DESC, finish_time_format ASC, m.created_at ASC', $quiz_phone_filter), ARRAY_A);
        if(count($search_quiz) > 0 ){
            $stt = 0;
            for ($i=0;$i < count($all_quiz); $i++) {
                if($all_quiz[$i]['quiz_phone'] == $search_quiz[0]['quiz_phone']){
                    $stt = $i;
                    break;
                }
            }
            $html = '';
            $html_mobile = '';
            for ($i=0;$i < count($search_quiz); $i++) {
                $html.= '<ul class="bxh-list">
                        <li>'.($stt+1).'</li>
                        <li class="bxh-list-text-left">
                            '.$search_quiz[$i]['quiz_name'].'
                        </li>
                        <li>
                            '.$search_quiz[$i]['area_name'].'
                        </li>
                        <li>
                            '.$search_quiz[$i]['team_name'].'
                        </li>
                        <li>'.$search_quiz[$i]['finish_time'].'</li>
                        <li>'.$search_quiz[$i]['Total_correct'].'</li>
                    </ul>';
                $html_mobile = '<div class="wrapper-mb-bxh_item">
                        <ul class="bxh-list bxh-list-title-mb">
                            <li>THỨ TỰ</li>
                            <li>'.($stt+1).'</li>
                        </ul>
                        <ul class="bxh-list bxh-list-title-mb">
                        <li>TÊN</li>
                        <li>'.$search_quiz[$i]['quiz_name'].'</li>
                        </ul>
                        <ul class="bxh-list bxh-list-title-mb">
                        <li>KHU VỰC</li>
                        <li>'.$search_quiz[$i]['area_name'].'</li>
                        </ul>
                        <ul class="bxh-list bxh-list-title-mb">
                        <li>ĐỘI</li>
                        <li>'.$search_quiz[$i]['team_name'].'</li>
                        </ul>
                        <ul class="bxh-list bxh-list-title-mb">
                        <li>THỜI GIAN</li>
                        <li>'.$search_quiz[$i]['finish_time'].'</li>
                        </ul>
                        <ul class="bxh-list bxh-list-title-mb">
                        <li>SỐ CÂU TRẢ LỜI CHÍNH XÁC</li>
                        <li>'.$search_quiz[$i]['Total_correct'].'</li>
                        </ul>
                    </div>';
            }
            $result = [
                "success"       => true,
                "html"          => $html,
                "html_mobile"   => $html_mobile,
                "message"       => 'Get dữ liệu thành công!'
            ];
        } else {
            for ($i=0;$i < 21; $i++) {
                $html.= '<ul class="bxh-list">
                        <li>'.($i+1).'</li>
                        <li class="bxh-list-text-left">
                            '.$all_quiz[$i]['quiz_name'].'
                        </li>
                        <li>
                            '.$all_quiz[$i]['area_name'].'
                        </li>
                        <li>
                            '.$all_quiz[$i]['team_name'].'
                        </li>
                        <li>'.$all_quiz[$i]['finish_time'].'</li>
                        <li>'.$all_quiz[$i]['Total_correct'].'</li>
                    </ul>';
                $html_mobile = '<div class="wrapper-mb-bxh_item">
                        <ul class="bxh-list bxh-list-title-mb">
                            <li>THỨ TỰ</li>
                            <li>'.($stt+1).'</li>
                        </ul>
                        <ul class="bxh-list bxh-list-title-mb">
                        <li>TÊN</li>
                        <li>'.$all_quiz[$i]['quiz_name'].'</li>
                        </ul>
                        <ul class="bxh-list bxh-list-title-mb">
                        <li>KHU VỰC</li>
                        <li>'.$all_quiz[$i]['area_name'].'</li>
                        </ul>
                        <ul class="bxh-list bxh-list-title-mb">
                        <li>ĐỘI</li>
                        <li>'.$all_quiz[$i]['team_name'].'</li>
                        </ul>
                        <ul class="bxh-list bxh-list-title-mb">
                        <li>THỜI GIAN</li>
                        <li>'.$all_quiz[$i]['finish_time'].'</li>
                        </ul>
                        <ul class="bxh-list bxh-list-title-mb">
                        <li>SỐ CÂU TRẢ LỜI CHÍNH XÁC</li>
                        <li>'.$all_quiz[$i]['Total_correct'].'</li>
                        </ul>
                    </div>';
            }
            $result = [
                "success"       => false,
                "html"          => $html,
                "html_mobile"   => $html_mobile,
                "message"       => 'Không tìm thấy dữ liệu!'
            ];
        }
    }
    echo json_encode($result);
    die();
}
add_action('wp_ajax_filterTeamQuiz', 'filterTeamQuiz_init');
add_action('wp_ajax_nopriv_filterTeamQuiz', 'filterTeamQuiz_init');
function  filterTeamQuiz_init()
{
    global $wpdb;
    $team_id     = $_REQUEST['team_id'];
    $all_quiz = $wpdb->get_results('SELECT mr_quiz_info.*, m.user_id, m.finish_time_format AS finish_time_format ,m.finish_time , m.total_correct AS Total_correct,
                    q_area.area_name, q_team.team AS team_name, m.created_at
                FROM mr_quiz_info
                LEFT JOIN mr_quiz_user_contest m ON mr_quiz_info.id = m.user_id 
                LEFT JOIN mr_teams AS q_area ON mr_quiz_info.quiz_area = q_area.area
                LEFT JOIN mr_teams AS q_team ON mr_quiz_info.quiz_team = q_team.team_id
                WHERE mr_quiz_info.status = 1 
                -- AND mr_quiz_info.quiz_phone = %s
                AND Total_correct in (SELECT MAX(m1.total_correct) as total_correct FROM mr_quiz_user_contest m1 WHERE m1.user_id = m.user_id)
                AND finish_time_format in (SELECT MIN(m2.finish_time_format) as finish_time_format FROM mr_quiz_user_contest m2 WHERE m2.user_id = m.user_id AND Total_correct in (SELECT MAX(m1.total_correct) as total_correct FROM mr_quiz_user_contest m1 WHERE m1.user_id = m.user_id) )
                -- AND m.created_at in (SELECT MIN(m3.created_at) as created_at FROM mr_quiz_user_contest m3 WHERE m3.user_id = m.user_id AND Total_correct in (SELECT MAX(m4.total_correct) as Total_correct FROM mr_quiz_user_contest m4 WHERE m4.user_id = m.user_id) AND finish_time_format in (SELECT MIN(m5.finish_time_format) as finish_time_format FROM mr_quiz_user_contest m5 WHERE m5.user_id = m.user_id))
                Group by m.user_id
                ORDER BY Total_correct DESC, finish_time_format ASC, m.created_at ASC', ARRAY_A);
    $search_quiz = $wpdb->get_results($wpdb->prepare('SELECT mr_quiz_info.*, m.user_id, m.finish_time_format AS finish_time_format ,m.finish_time , m.total_correct AS Total_correct,
                    q_area.area_name, q_team.team AS team_name, m.created_at
                FROM mr_quiz_info
                LEFT JOIN mr_quiz_user_contest m ON mr_quiz_info.id = m.user_id 
                LEFT JOIN mr_teams AS q_area ON mr_quiz_info.quiz_area = q_area.area
                LEFT JOIN mr_teams AS q_team ON mr_quiz_info.quiz_team = q_team.team_id
                WHERE mr_quiz_info.status = 1 
                AND mr_quiz_info.quiz_team = %d
                AND Total_correct in (SELECT MAX(m1.total_correct) as total_correct FROM mr_quiz_user_contest m1 WHERE m1.user_id = m.user_id)
                AND finish_time_format in (SELECT MIN(m2.finish_time_format) as finish_time_format FROM mr_quiz_user_contest m2 WHERE m2.user_id = m.user_id AND Total_correct in (SELECT MAX(m1.total_correct) as total_correct FROM mr_quiz_user_contest m1 WHERE m1.user_id = m.user_id) )
                -- AND m.created_at in (SELECT MIN(m3.created_at) as created_at FROM mr_quiz_user_contest m3 WHERE m3.user_id = m.user_id AND Total_correct in (SELECT MAX(m4.total_correct) as Total_correct FROM mr_quiz_user_contest m4 WHERE m4.user_id = m.user_id) AND finish_time_format in (SELECT MIN(m5.finish_time_format) as finish_time_format FROM mr_quiz_user_contest m5 WHERE m5.user_id = m.user_id))
                Group by m.user_id
                ORDER BY Total_correct DESC, finish_time_format ASC, m.created_at ASC', $team_id), ARRAY_A);
    if($team_id != -1){
        $html = '';
        $html_mobile = '';
        for ($i=0;$i < count($search_quiz); $i++) {
            $html.= '<ul class="bxh-list">
                    <li>'.($i+1).'</li>
                    <li class="bxh-list-text-left">
                        '.$search_quiz[$i]['quiz_name'].'
                    </li>
                    <li>
                        '.$search_quiz[$i]['area_name'].'
                    </li>
                    <li>
                        '.$search_quiz[$i]['team_name'].'
                    </li>
                    <li>'.$search_quiz[$i]['finish_time'].'</li>
                    <li>'.$search_quiz[$i]['Total_correct'].'</li>
                </ul>';
            $html_mobile = '<div class="wrapper-mb-bxh_item">
                    <ul class="bxh-list bxh-list-title-mb">
                        <li>THỨ TỰ</li>
                        <li>'.($stt+1).'</li>
                    </ul>
                    <ul class="bxh-list bxh-list-title-mb">
                    <li>TÊN</li>
                    <li>'.$search_quiz[$i]['quiz_name'].'</li>
                    </ul>
                    <ul class="bxh-list bxh-list-title-mb">
                    <li>KHU VỰC</li>
                    <li>'.$search_quiz[$i]['area_name'].'</li>
                    </ul>
                    <ul class="bxh-list bxh-list-title-mb">
                    <li>ĐỘI</li>
                    <li>'.$search_quiz[$i]['team_name'].'</li>
                    </ul>
                    <ul class="bxh-list bxh-list-title-mb">
                    <li>THỜI GIAN</li>
                    <li>'.$search_quiz[$i]['finish_time'].'</li>
                    </ul>
                    <ul class="bxh-list bxh-list-title-mb">
                    <li>SỐ CÂU TRẢ LỜI CHÍNH XÁC</li>
                    <li>'.$search_quiz[$i]['Total_correct'].'</li>
                    </ul>
                </div>';
        }
        $msg = "";
        if(count($search_quiz) < 0){
            $msg = "Chưa có dữ liệu cho đội bạn chọn!";
        }
        $result = [
            "success"       => true,
            "html"          => $html,
            "html_mobile"   => $html_mobile,
            "message"       => $msg
        ];
    } else {
        for ($i=0;$i < 21; $i++) {
            $html.= '<ul class="bxh-list">
                    <li>'.($i+1).'</li>
                    <li class="bxh-list-text-left">
                        '.$all_quiz[$i]['quiz_name'].'
                    </li>
                    <li>
                        '.$all_quiz[$i]['area_name'].'
                    </li>
                    <li>
                        '.$all_quiz[$i]['team_name'].'
                    </li>
                    <li>'.$all_quiz[$i]['finish_time'].'</li>
                    <li>'.$all_quiz[$i]['Total_correct'].'</li>
                </ul>';
            $html_mobile = '<div class="wrapper-mb-bxh_item">
                    <ul class="bxh-list bxh-list-title-mb">
                        <li>THỨ TỰ</li>
                        <li>'.($stt+1).'</li>
                    </ul>
                    <ul class="bxh-list bxh-list-title-mb">
                    <li>TÊN</li>
                    <li>'.$all_quiz[$i]['quiz_name'].'</li>
                    </ul>
                    <ul class="bxh-list bxh-list-title-mb">
                    <li>KHU VỰC</li>
                    <li>'.$all_quiz[$i]['area_name'].'</li>
                    </ul>
                    <ul class="bxh-list bxh-list-title-mb">
                    <li>ĐỘI</li>
                    <li>'.$all_quiz[$i]['team_name'].'</li>
                    </ul>
                    <ul class="bxh-list bxh-list-title-mb">
                    <li>THỜI GIAN</li>
                    <li>'.$all_quiz[$i]['finish_time'].'</li>
                    </ul>
                    <ul class="bxh-list bxh-list-title-mb">
                    <li>SỐ CÂU TRẢ LỜI CHÍNH XÁC</li>
                    <li>'.$all_quiz[$i]['Total_correct'].'</li>
                    </ul>
                </div>';
        }
        $result = [
            "success"       => false,
            "html"          => $html,
            "html_mobile"   => $html_mobile
        ];
    }
    echo json_encode($result);
    die();
}
/** END quiz filter */
// Function to get the client IP address
function get_client_ip() {
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}