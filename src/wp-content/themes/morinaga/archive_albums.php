<?php
/* Template Name: Archive-Album */

global $wpdb;
$album_id = $_GET['album_id'];
$albums = $wpdb->get_results($wpdb->prepare("select * from mr_register_image where info_id = %d", $album_id));
get_header();
?>
<div class="main-bg">
    <div class="container">
        <div class="container-90">
            <div class="wrapper-rule" id="about">
                <div class="portlet-body">
                    <?php
                    if (count($albums) < 1) {
                    ?>
                        <p style="color:red">Album không tồn tại</p>
                    <?php
                    } else {
                    ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Tên Album</th>
                                        <th>Hình 1</th>
                                        <th>Hình 2</th>
                                        <th>Hình 3</th>
                                        <th>Hình 4</th>
                                    </tr>
                                </thead>
                                <?php
                                foreach ($albums as $album) {
                                    $group_image = explode(';', $album->group_image);
                                ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $album->album_name ?> </td>
                                            <td> 
                                                <a href="<?php echo get_home_url() . '/wp-content/uploads/' . get_post_meta($group_image[0], '_wp_attached_file', true) ?>" target="_blank">
                                                    <img width="160" height="160" src="<?php echo get_home_url() . '/wp-content/uploads/' . get_post_meta($group_image[0], '_wp_attached_file', true) ?>" />
                                                </a>
                                            </td>
                                            <td> 
                                                <a href="<?php echo get_home_url() . '/wp-content/uploads/' . get_post_meta($group_image[1], '_wp_attached_file', true) ?>" target="_blank">
                                                    <img width="160" height="160" src="<?php echo get_home_url() . '/wp-content/uploads/' . get_post_meta($group_image[1], '_wp_attached_file', true) ?>" />
                                                </a>
                                            </td>
                                            <td> 
                                                <?php if(isset($group_image[2])) {?> 
                                                    <a href="<?php echo get_home_url() . '/wp-content/uploads/' . get_post_meta($group_image[2], '_wp_attached_file', true) ?>" target="_blank">
                                                        <img width="160" height="160" src="<?php echo get_home_url() . '/wp-content/uploads/' . get_post_meta($group_image[2], '_wp_attached_file', true) ?>" /></td>
                                                    </a>
                                                <?php }?>
                                            <td>
                                                <?php if(isset($group_image[3])) {?> 
                                                    <a href="<?php echo get_home_url() . '/wp-content/uploads/' . get_post_meta($group_image[3], '_wp_attached_file', true) ?>" target="_blank">
                                                        <img width="160" height="160" src="<?php echo get_home_url() . '/wp-content/uploads/' . get_post_meta($group_image[3], '_wp_attached_file', true) ?>" />
                                                    </a>
                                                <?php }?>
                                            </td>
                                        </tr>
                                    </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
// get_footer();
