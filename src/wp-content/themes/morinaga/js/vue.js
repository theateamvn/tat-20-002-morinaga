var app = new Vue({
   el: '#app',
   data: {
      list_mienbac: [
         {
            area:1,
            team_id:2,
            team_area:'Điện Biên',
            team:'Thanh Biệt'
         },
         {
            area:1,
            team_id:3,
            team_area:'Sơn La',
            team:'Tuấn Anh'
         },
         {
            area:1,
            team_id:4,
            team_area:'Hòa Bình',
            team:'Quốc Sơn'
         },
         {
            area:1,
            team_id:5,
            team_area:'Hà Nội-Long Biên',
            team:'Trường Lê'
         },
         {
            area:1,
            team_id:6,
            team_area:'Hà Nội-Sơn Tây',
            team:'Chi Thảo'
         },
         {
            area:1,
            team_id:7,
            team_area:'Quảng Ninh-Hạ Long + Cẩm Phả',
            team:'Hoàng Gia'
         },
         {
            area:1,
            team_id:8,
            team_area:'Quảng Ninh-Móng Cái',
            team:'Thùy Linh'
         },
         {
            area:1,
            team_id:9,
            team_area:'Quảng Ninh-Uông Bí',
            team:'Lại Huy Dũng'
         },
         {
            area:1,
            team_id:10,
            team_area:'Hưng Yên',
            team:'Đình Hiến'
         },
         {
            area:1,
            team_id:11,
            team_area:'Hải Dương',
            team:'Nguyễn Thanh'
         },
         {
            area:1,
            team_id:12,
            team_area:'Hải Phòng 1',
            team:'Toàn Sóng'
         },
         {
            area:1,
            team_id:13,
            team_area:'Hải Phòng 2',
            team:'Tuấn Tuyết'
         },
         {
            area:1,
            team_id:14,
            team_area:'Nam Định 1',
            team:'Tuấn Yến'
         },
         {
            area:1,
            team_id:15,
            team_area:'Nam Định 2',
            team:'Đức Cường'
         },
         {
            area:1,
            team_id:16,
            team_area:'Thái Bình',
            team:'Anh Đào'
         },
         {
            area:1,
            team_id:17,
            team_area:'Hà Đông',
            team:'Anh Cường'
         },
         {
            area:1,
            team_id:18,
            team_area:'Bắc Giang',
            team:'Chị Hoàn'
         },
         {
            area:1,
            team_id:19,
            team_area:'Bắc Ninh',
            team:'Anh Đạt'
         },
         {
            area:1,
            team_id:20,
            team_area:'Lạng Sơn',
            team:'Đăng Đàm'
         },
         {
            area:1,
            team_id:21,
            team_area:'Thái Nguyên',
            team:'Quyền Lê'
         },
         {
            area:1,
            team_id:22,
            team_area:'Hà Nam',
            team:'Anh Dương'
         },
         {
            area:1,
            team_id:23,
            team_area:'Ninh Bình',
            team:'Phạm Gia'
         },
         {
            area:1,
            team_id:24,
            team_area:'Thanh Hóa',
            team:'Sáu Tâm'
         },
         {
            area:1,
            team_id:25,
            team_area:'Nghệ An',
            team:'Nam Thanh'
         },
         {
            area:1,
            team_id:26,
            team_area:'Hà Tĩnh',
            team:'Đức Dũng'
         },
         {
            area:1,
            team_id:27,
            team_area:'Quảng Bình',
            team:'Sáu Hạnh'
         },
         {
            area:1,
            team_id:28,
            team_area:'Quảng Trị',
            team:'Phùng Châu'
         },
         {
            area:1,
            team_id:29,
            team_area:'TT Huế',
            team:'Trần Khơi'
         },
         {
            area:1,
            team_id:30,
            team_area:'Vĩnh Phúc',
            team:'Lan Tuyến'
         },
         {
            area:1,
            team_id:31,
            team_area:'Phú Thọ',
            team:'Quang Tuấn'
         },
         {
            area:1,
            team_id:32,
            team_area:'Hà Giang',
            team:'Vũ Thị Huấn'
         },
         {
            area:1,
            team_id:33,
            team_area:'Tuyên Quang',
            team:'Hoàng Lan'
         },
         {
            area:1,
            team_id:34,
            team_area:'Yên Bái',
            team:'Chị Vân'
         },
         {
            area:1,
            team_id:35,
            team_area:'Lào Cai',
            team:'Thanh Tuyên'
         },
         {
            area:1,
            team_id:36,
            team_area:'Lai Châu',
            team:'Quang Nguyễn'
         },
         {
            area:1,
            team_id:37,
            team_area:'Hà Nội',
            team:'Hệ thống Babyshop_MB'
         },
         {
            area:1,
            team_id:38,
            team_area:'Hà Nội',
            team:'Hệ thống siêu thị_MB'
         },
         {
            area:1,
            team_id:39,
            team_area:'Hà Nội',
            team:'GT Hà Nội'
         }
      ],
      list_miennam: [
         {
            area:2,
            team_id:1,
            team_area:'Miền Nam',
            team:'Hệ Thống Siêu Thị _MN'
         },
         {
            area:2,
            team_id:2,
            team_area:'Miền Nam',
            team:'Hệ thống Babyshop_MN'
         },
         {
            area:2,
            team_id:3,
            team_area:'Miền Nam',
            team:'Kênh Y tế _ MN'
         },
         {
            area:2,
            team_id:4,
            team_area:'Miền Nam',
            team:'Kênh OTC_ MN'
         },
         {
            area:2,
            team_id:5,
            team_area:'Khánh Hòa',
            team:'NPP An Phát'
         },
         {
            area:2,
            team_id:6,
            team_area:'Khánh Hòa',
            team:'NPP Kim Anh '
         },
         {
            area:2,
            team_id:7,
            team_area:'Ninh Thuận',
            team:'NPP Bảo Tín'
         },
         {
            area:2,
            team_id:8,
            team_area:'Bình Thuận',
            team:'NPP Minh Minh Hoàn'
         },
         {
            area:2,
            team_id:9,
            team_area:'Bảo Lộc',
            team:'NPP Bảo Trâm'
         },
         {
            area:2,
            team_id:10,
            team_area:'Đà Lạt ',
            team:'NPP Vọng Dung'
         },
         {
            area:2,
            team_id:11,
            team_area:'Biên Hòa',
            team:'NPP Minh Tấn'
         },
         {
            area:2,
            team_id:12,
            team_area:'Long Khánh',
            team:'NPP Hoàng Kim'
         },
         {
            area:2,
            team_id:13,
            team_area:'Bà Rịa',
            team:'NPP Anh Tuấn'
         },
         {
            area:2,
            team_id:14,
            team_area:'Vũng Tàu',
            team:'NPP Huyền Phương'
         },
         {
            area:2,
            team_id:15,
            team_area:'Bình Dương',
            team:'NPP Huỳnh Dũng'
         },
         {
            area:2,
            team_id:16,
            team_area:'Bắc Bình Dương',
            team:'NPP Thảo Chi '
         },
         {
            area:2,
            team_id:17,
            team_area:'Đồng Xoài',
            team:'NPP Quân Nguyễn'
         },
         {
            area:2,
            team_id:18,
            team_area:'Lộc Linh',
            team:'NPP Anh Thư'
         },
         {
            area:2,
            team_id:19,
            team_area:'Tây Ninh',
            team:'NPP Thành Phát'
         },
         {
            area:2,
            team_id:20,
            team_area:'Cần Thơ',
            team:'NPP Minh Kim '
         },
         {
            area:2,
            team_id:21,
            team_area:'Hậu Giang',
            team:'NPP Thu Cúc'
         },
         {
            area:2,
            team_id:22,
            team_area:'Cà Mau',
            team:'NPP Ngọc Nhãn'
         },
         {
            area:2,
            team_id:23,
            team_area:'Sóc Trăng',
            team:'NPP Mỹ Oanh'
         },
         {
            area:2,
            team_id:24,
            team_area:'An Giang',
            team:'NPP Trần Văn Tiếp'
         },
         {
            area:2,
            team_id:25,
            team_area:'An Giang',
            team:'NPP Hoàng Quyên'
         },
         {
            area:2,
            team_id:26,
            team_area:'Phú Quốc',
            team:'NPP Việt Khoa'
         },
         {
            area:2,
            team_id:27,
            team_area:'Phú Quốc',
            team:'NPP Quách Vũ Minh'
         },
         {
            area:2,
            team_id:28,
            team_area:'NPP Thuận Lợi',
            team:'Bạc Liêu'
         },
         {
            area:2,
            team_id:29,
            team_area:'Long An',
            team:'NPP Gia Phát'
         },
         {
            area:2,
            team_id:30,
            team_area:'Tiền Giang',
            team:'NPP Hoàng Bách'
         },
         {
            area:2,
            team_id:31,
            team_area:'Bến Tre',
            team:'NPP Hoàng Vy'
         },
         {
            area:2,
            team_id:32,
            team_area:'Đồng Tháp',
            team:'NPP Hồng Hà'
         },
         {
            area:2,
            team_id:33,
            team_area:'Vĩnh Long',
            team:'NPP Dũng Thảo'
         },
         {
            area:2,
            team_id:34,
            team_area:'Trà Vinh',
            team:'NPP Minh Thuận '
         },
         {
            area:2,
            team_id:35,
            team_area:'Đắc Lắc',
            team:'NPP Mai Hằng'
         },
         {
            area:2,
            team_id:36,
            team_area:'Đắc Nông',
            team:'NPP Đỗ Quyên'
         },
         {
            area:2,
            team_id:37,
            team_area:'Đà Nẵng',
            team:'NPP Tuấn Ny'
         },
         {
            area:2,
            team_id:38,
            team_area:'Kon Tum',
            team:'NPP Lam Dy'
         },
         {
            area:2,
            team_id:39,
            team_area:'Quảng Nam',
            team:'NPP Thanh Bình An'
         },
         {
            area:2,
            team_id:40,
            team_area:'Quảng Nam',
            team:'NPP Tám Phương'
         },
         {
            area:2,
            team_id:40,
            team_area:'Quảng Ngãi',
            team:'NPP Hoàng Sơn'
         },
         {
            area:2,
            team_id:41,
            team_area:'Phú Yên',
            team:'NPP Huy Hoàng'
         },
         {
            area:2,
            team_id:42,
            team_area:'Bình Định',
            team:'NPP Toàn Nguyên'
         },
         {
            area:2,
            team_id:43,
            team_area:'Bình Định',
            team:'NPP Hùng Hiển'
         },
         {
            area:2,
            team_id:44,
            team_area:'Gia Lai',
            team:'NPP Thủy Trâm'
         },
         {
            area:2,
            team_id:45,
            team_area:'Gia Lai',
            team:'NPP Thủy Trâm'
         },
         {
            area:2,
            team_id:46,
            team_area:'Gia Lai',
            team:'NPP Như Hà'
         },
         {
            area:2,
            team_id:47,
            team_area:'HCM',
            team:'GT Hồ Chí Minh'
         }
      ],
      list_team: {},
      seen:false,
      area: -1,
      question:[
         {
         qest_id:1,
         quest_name:'Khởi động nào, hãy thử nhắm mắt và nghĩ về ngày lễ trọng đại dời mình, bạn đang khoác tay "nửa kia" bước qua cổng chào tiến vào lễ đường, hãy miêu tả chiếc cổng được trang trí ra sao nhé?',
         answers: [
            {
               'ans_id':1,
               'ans_name':'Đơn giản, tinh tế và toát lên những đường nét tươi sáng, thanh lịch. và toát lên những đường nét tươi sáng, thanh lịc'
            },
            {
               'ans_id':2,
               'ans_name':'Hiện đại, độc đáo và ấn tượng như tính cách của bạn. và toát lên những đường nét tươi sáng, thanh lịc'
            },
            {
               'ans_id':3,
               'ans_name':'Ngọt ngào, nhẹ nhàng với những đóa hoa hồng kết lãng mạn. và toát lên những đường nét tươi sáng, thanh lịc'
            },
            {
               'ans_id':4,
               'ans_name':'Rực rỡ và choáng ngợp là điểm nhấn chính của cả buổi tiệc. và toát lên những đường nét tươi sáng, thanh lịc'
            }
         ]
      },
      {
            qest_id:2,
            quest_name:'Địa điểm tổ chức đám cưới trong mơ của hai bạn được diễn ra ở đâu?',
            answers: [
               {
                  'ans_id':5,
                  'ans_name':'Tại một nhà thờ cổ kính, trang trọng và thiêng liêng.'
               },
               {
                  'ans_id':6,
                  'ans_name':'Trung tâm tiệc cưới sang chảnh, hiện đại, có view rooftop "triệu đô" bậc nhất thành phố.'
               },
               {
                  'ans_id':7,
                  'ans_name':'Tiệc ngoài trời ở một bãi biển thơ mộng.'
               },
               {
                  'ans_id':8,
                  'ans_name':'Không gian lung linh với sảnh tiệc sang trọng, mang âm hưởng thiết kế Tây phương với nhạc "đu đưa" cực chất.'
               }
            ]
      },
      {
            qest_id:3,
            quest_name:'Còn khách mời thì sao, toog màu dress-code nào đã được bạn yêu cầu phải mặc vào ngày cưới?',
            answers: [
               {
                  'ans_id':9,
                  'ans_name':'Tất nhiên là tông trắng đen vừa đơn giản mà vẫn rất sang rồi.'
               },
               {
                  'ans_id':10,
                  'ans_name':'Chắc chắn là gam màu hiện đại, thời thượng như xanh dương, hay Green Midnight đang hot chẳng hạn.'
               },
               {
                  'ans_id':11,
                  'ans_name':'Nhẹ nhàng thôi, khách mời diện tông pastel để chụp hình chung mới lung linh chứ.'
               },
               {
                  'ans_id':12,
                  'ans_name':'Không cần thiết đâu, mặc gì cũng được miễ là phải quẫy hết mình.'
               }
            ]
      },
      {
            qest_id:4,
            quest_name:'Thành phần khách mời nào sẽ được bạn ưu tiên mời tham gia lễ cưới nhỉ?',
            answers: [
               {
                  'ans_id':13,
                  'ans_name':'Gia đình và một số ít bạn bè thân thiết, mình muốn phải thật mật và ấm cúng.'
               },
               {
                  'ans_id':14,
                  'ans_name':'Không thể thiếu bạn bè, anh em bốn phương được.'
               },
               {
                  'ans_id':15,
                  'ans_name':'Gia đình, bạn bè và một số em bé làm phù đâu phù rể thì đáng yêu lắm đấy.'
               },
               {
                  'ans_id':16,
                  'ans_name':'Càng đông càng vui thôi, nào nào mình cùng lại đây phê pha.'
               }
            ]
      },
      {
            qest_id:5,
            quest_name:'Vậy thì thử làm một phép tính xem đám cưới của bạn quy mô nhường nào nè?',
            answers: [
               {
                  'ans_id':17,
                  'ans_name':'Khoảng dưới 100 khách thôi, mình muốn nó thật nhỏ gọn, vừa phải.'
               },
               {
                  'ans_id':18,
                  'ans_name':'Mình đoán là trên dưới 200 khách.'
               },
               {
                  'ans_id':19,
                  'ans_name':'Để xem nào, tầm 200 đến 300 khách đấy, bạn bè, họ hàng của hai đứa mình không ít đâu.'
               },
               {
                  'ans_id':20,
                  'ans_name':'Tất nhiên phải trên 300 khách hơn đây, đông vui mới là ngày cưới linh đình chứ.'
               }
            ]
      }
      ],
      quiz_group:-1,
      quiz_area:-1,
      quiz_team:-1,
      list_team_quiz: {},
      list_quiz_mienbac: [
         {
            area:1,
            team_id:2,
            team:'GT Hà Nội (Trung tâm)'
         },
         {
            area:1,
            team_id:3,
            team:'GT Hà Nội (Ven) + Tây Bắc 1'
         },
         {
            area:1,
            team_id:4,
            team:'Tây Bắc 2'
         },
         {
            area:1,
            team_id:5,
            team:'Đông Bắc 1'
         },
         {
            area:1,
            team_id:6,
            team:'Đông Bắc 2'
         },
         {
            area:1,
            team_id:7,
            team:'Duyên Hải'
         },
         {
            area:1,
            team_id:8,
            team:'Bắc Miền Trung'
         },
         {
            area:1,
            team_id:9,
            team:'BS'
         },
         {
            area:1,
            team_id:10,
            team:'MT + MC'
         }
      ],
      list_quiz_miennam: [
         {
            area:2,
            team_id:52,
            team:'GT Hồ Chí Minh + OTC'
         },
         {
            area:2,
            team_id:53,
            team:'Miền Trung'
         },
         {
            area:2,
            team_id:54,
            team:'Miền Trung 1'
         },
         {
            area:2,
            team_id:55,
            team:'Miền  Đông 1'
         },
         {
            area:2,
            team_id:56,
            team:'Miền  Đông 2'
         },
         {
            area:2,
            team_id:57,
            team:'Nam Mê Kông'
         },
         {
            area:2,
            team_id:58,
            team:'Bắc Mê Kông'
         },
         {
            area:2,
            team_id:59,
            team:'BS + MT + MC'
         },
      ],
      list_quiz_npp: [
         {
            group:3,
            area_id:108,
            area_name:'Miền Bắc'
         },
         {
            group:3,
            area_id:109,
            area_name:'Miền Nam'
         }
      ],
      list_area_bn: [
         {
            area_id:1,
            area_name:'Miền Bắc'
         },
         {
            area_id:2,
            area_name:'Miền Nam'
         }
      ],
      list_quiz_team: [
         {
            area:1,
            team_id:2,
            team:'GT Hà Nội (Trung tâm)'
         },
         {
            area:1,
            team_id:3,
            team:'GT Hà Nội (Ven) + Tây Bắc 1'
         },
         {
            area:1,
            team_id:4,
            team:'Tây Bắc 2'
         },
         {
            area:1,
            team_id:5,
            team:'Đông Bắc 1'
         },
         {
            area:1,
            team_id:6,
            team:'Đông Bắc 2'
         },
         {
            area:1,
            team_id:7,
            team:'Duyên Hải'
         },
         {
            area:1,
            team_id:8,
            team:'Bắc Miền Trung'
         },
         {
            area:1,
            team_id:9,
            team:'BS'
         },
         {
            area:1,
            team_id:10,
            team:'MT + MC'
         },
         {
            area:2,
            team_id:52,
            team:'GT Hồ Chí Minh + OTC'
         },
         {
            area:2,
            team_id:53,
            team:'Miền Trung'
         },
         {
            area:2,
            team_id:54,
            team:'Miền Trung 1'
         },
         {
            area:2,
            team_id:55,
            team:'Miền  Đông 1'
         },
         {
            area:2,
            team_id:56,
            team:'Miền  Đông 2'
         },
         {
            area:2,
            team_id:57,
            team:'Nam Mê Kông'
         },
         {
            area:2,
            team_id:58,
            team:'Bắc Mê Kông'
         },
         {
            area:2,
            team_id:59,
            team:'BS + MT + MC'
         },
      ],
      list_area:{}
   },
   mounted: function () {
      console.log('mouted ---------');
   },
   methods: {
      getArea: function (id) {
         var _self = this;
         _self.team_area = -1;
         if(id == 1){
            _self.list_team = _self.list_quiz_mienbac;
         } else {
            _self.list_team = _self.list_quiz_miennam;
         }
      },
      getAreaQuiz: function (id) {
         var _self = this;
         _self.quiz_area = -1;
         if(id == 3){
            _self.list_area = _self.list_quiz_npp;
         } else {
            _self.list_area = _self.list_area_bn;
         }
      },
      getGroup: function (id) {
         var _self = this;
         _self.quiz_team = -1;
         if(id == 1){
            _self.list_team_quiz = _self.list_quiz_mienbac;
         } else {
            _self.list_team_quiz = _self.list_quiz_miennam;
         }
      }
   }
})