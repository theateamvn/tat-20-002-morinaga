$(function () {
   var url_admin_ajax = $('#url_admin_ajax').val();
   var stop_ = false;
   $('.quiz-test').on('click', '#btn_finish', function () {
      stop_ = true;
      var answer = [];
      var finish_time = $('#finish_time').val();
      for (let index = 0; index < 2; index++) {
         answer[index] = $("input[name='answer_" + (index + 1) + "']:checked").val();
      }
      var user_id = $('#quiz_res_id').val();
      var $this = $(this);
         var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
         if ($(this).html() !== loadingText) {
            $this.data('original-text', $(this).html());
            $this.html(loadingText);
         }
         setTimeout(function () {
            $this.html($this.data('original-text'));
         }, 4000);
      $.ajax({
         type: 'post',
         dataType: 'json',
         url: url_admin_ajax,
         data: {
            action: 'submitQuestion',
            user_id: user_id,
            answer: answer,
            finish_time: finish_time
         },
         context: this,
         beforeSend: function () {
            //Làm gì đó trước khi gửi dữ liệu vào xử lý
         },
         success: function (response) {
            //Làm gì đó khi dữ liệu đã được xử lý
            if (response.success) {
               $('#quiz_step_3').hide();
               $('#quiz_step_4').html(response.html);
            }
            else {
               $("p.err-msg").html(response.message);
            }
         },
         error: function (jqXHR, textStatus, errorThrown) {
            //Làm gì đó khi có lỗi xảy ra
            console.log('The following error occured: ' + textStatus, errorThrown);
         }
      });
   })

   $('#btn_quiz_step_2').click(function () {
      var quiz_group = $('#quiz_group').val();
      var quiz_area  = $('#quiz_area').val();
      var quiz_team  = $('#quiz_team').val();
      var quiz_name = $('#quiz_name').val();
      var quiz_phone = $('#quiz_phone').val();
      var quiz_email = $('#quiz_email').val();
      if (quiz_group == -1) {
         $("p.quiz-err-msg").html('(*) Vui lòng chọn đối tượng.');
         $this.html($this.data('original-text'));
         $("#quiz_group").focus();
      } else if (quiz_area == -1) {
         $("p.quiz-err-msg").html('(*) Vui lòng chọn khu vực.');
         $this.html($this.data('original-text'));
         $("#quiz_area").focus();
      } else if (quiz_team == -1 && (quiz_group == 1 || quiz_group == 2)) {
         $("p.quiz-err-msg").html('(*) Vui lòng chọn đội.');
         $this.html($this.data('original-text'));
         $("#quiz_area").focus();
      } else if (quiz_name == '') {
         $("p.quiz-err-msg").html('(*) Vui lòng điền họ và tên.');
         $("#quiz_name").focus();
      } else if (quiz_phone == '') {
         $("p.quiz-err-msg").html('Vui lòng điền số điện thoại.');
         $("#quiz_phone").focus();
      } else if (!validatePhone(quiz_phone)) {
         $("p.quiz-err-msg").html('Số điện thoại chưa đúng định dạng.');
         $("#quiz_phone").focus();
      } else if (quiz_email == '' && (quiz_group == 1 || quiz_group == 2)) {
         $("p.quiz-err-msg").html('Vui lòng điền email.');
         $("#quiz_email").focus();
      } else if (quiz_email != '' && !validateEmail(quiz_email)) {
         $("p.quiz-err-msg").html('Email chưa đúng định dạng.');
         $("#quiz_email").focus();
      } else {
         // loading 
         var $this = $(this);
         var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
         if ($(this).html() !== loadingText) {
            $this.data('original-text', $(this).html());
            $this.html(loadingText);
         }
         setTimeout(function () {
            $this.html($this.data('original-text'));
         }, 4000);
         $.ajax({
            type: 'post',
            dataType: 'json',
            url: url_admin_ajax,
            data: {
               action: 'submitQuizInfo',
               quiz_group: quiz_group,
               quiz_area: quiz_area,
               quiz_team: quiz_team,
               quiz_name: quiz_name,
               quiz_phone: quiz_phone,
               quiz_email: quiz_email
            },
            context: this,
            beforeSend: function () {
               //Làm gì đó trước khi gửi dữ liệu vào xử lý
            },
            success: function (response) {
               //Làm gì đó khi dữ liệu đã được xử lý
               if (response.success) {
                  $('#quiz_res_id').val(response.id);
                  $('.quiz-info-wrapper').hide();
                  $('#quiz_step_alert').hide();
                  $('#quiz_step_2').hide();
                  load_Question();
               }
               else {
                  $("p.quiz-err-msg").html(response.message);
               }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               //Làm gì đó khi có lỗi xảy ra
               console.log('The following error occured: ' + textStatus, errorThrown);
            }
         });
      }
   });

   function load_Question() {
      var start_time = new Date();
      var user_id = $('#quiz_res_id').val();
      $.ajax({
         type: 'post',
         dataType: 'json',
         url: url_admin_ajax,
         data: {
            action: 'getQuestion',
            user_id: user_id,
            start_time: formatDate(start_time)
         },
         context: this,
         beforeSend: function () {
            //Làm gì đó trước khi gửi dữ liệu vào xử lý
         },
         success: function (response) {
            //Làm gì đó khi dữ liệu đã được xử lý
            if (response.success) {
               $('#quiz_res_id').val(response.user_id);
               $('.quiz-slideshow').slick("unslick");
               $('.quiz-test').html(response.html);
               $('.quiz-slideshow').slick({
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  nextArrow: $('#next'),
                  prevArrow: $('#btn_prev'),
               });
               progress(300, 300, $("#progressBar"));
            }
            else {
               $("p.err-msg").html(response.message);
            }
         },
         error: function (jqXHR, textStatus, errorThrown) {
            //Làm gì đó khi có lỗi xảy ra
            console.log('The following error occured: ' + textStatus, errorThrown);
         }
      });
   };

   $('#step_2_btn').click(function () {
      if ($('#main_image').get(0).files.length < 4 | $('#main_image').get(0).files.length > 4) {
         $("p.err-upload-msg").html('Vui lòng chọn 4 hình ảnh.');
      } else {
         // loading 
         var $this = $(this);
         var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
         if ($(this).html() !== loadingText) {
            $this.data('original-text', $(this).html());
            $this.html(loadingText);
         }
         setTimeout(function () {
            $this.html($this.data('original-text'));
         }, 8000);
         // end loading
         var id = $('#res_id').val();
         var ablum_name = $('#ablum_name').val();
         var fd = new FormData();
         //fd.append( "main_image", $('#main_image')[0].files[0]);
         for (i = 0; i < $('#main_image')[0].files.length; i++) {
            fd.append('main_image[' + i + ']', $('#main_image')[0].files[i]);
         }
         fd.append("action", 'submitUpload');
         fd.append("id", id);
         fd.append("ablum_name", ablum_name);
         $.ajax({
            type: 'post',
            dataType: 'json',
            url: url_admin_ajax,
            data: fd,
            processData: false,
            contentType: false,
            beforeSend: function () {
               //Làm gì đó trước khi gửi dữ liệu vào xử lý
            },
            success: function (data, textStatus, XMLHttpRequest) {
               //Làm gì đó khi dữ liệu đã được xử lý
               $('.register-picture').removeClass('register-show');
               $('.register-picture').addClass('register-hide');
               //$('.register-content').addClass('register-hide');
               $('.register-success').addClass('register-show');
            },
            error: function (MLHttpRequest, textStatus, errorThrown) {
               //Làm gì đó khi có lỗi xảy ra
               console.log('The following error occured: ' + textStatus, errorThrown);
            }
         })
      }
   });
   $('#step_1_btn').click(function () {
      // check email
      var patternEmail = '/^[a-z0-9_\-\.]{2,}@[a-z0-9_\-\.]{2,}\.[a-z]{2,4}$/i';
      var area = $('#area').val();
      var team_area = $('#team_area').val();
      var username = $('#username').val();
      var email = $('#email').val();
      var position = $('#position').val();
      var phone = $('#phone').val();
      if (area == -1) {
         $("p.err-msg").html('(*) Vui lòng chọn khu vực.');
         $this.html($this.data('original-text'));
         $("#area").focus();
      } else if (team_area == -1) {
         $("p.err-msg").html('Vui lòng chọn tên đội.');
         $("#team_area").focus();
      } else if (username == '') {
         $("p.err-msg").html('Vui lòng điền họ và tên.');
         $("#username").focus();
      } else if (email == '') {
         $("p.err-msg").html('Vui lòng điền email.');
         $("#email").focus();
      } else if (!validateEmail(email)) {
         $("p.err-msg").html('Email chưa đúng định dạng.');
         $("#email").focus();
      } else if (!validatePhone(phone)) {
         $("p.err-msg").html('Số điện thoại chưa đúng địng dạng.');
         $("#phone").focus();
      } else if (phone == '') {
         $("p.err-msg").html('vui lòng điền số điện thoại.');
         $("#phone").focus();
      } else if (position == -1) {
         $("p.err-msg").html('Vui lòng chọn chức vụ.');
         $("#postion").focus();
      } else {
         // loading 
         var $this = $(this);
         var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
         if ($(this).html() !== loadingText) {
            $this.data('original-text', $(this).html());
            $this.html(loadingText);
         }
         setTimeout(function () {
            $this.html($this.data('original-text'));
         }, 4000);
         // end loading
         $.ajax({
            type: 'post',
            dataType: 'json',
            url: url_admin_ajax,
            data: {
               action: 'submitInfo',
               username: username,
               area: area,
               team_area: team_area,
               email: email,
               position: position,
               phone: phone
            },
            context: this,
            beforeSend: function () {
               //Làm gì đó trước khi gửi dữ liệu vào xử lý
            },
            success: function (response) {
               //Làm gì đó khi dữ liệu đã được xử lý
               if (response.success) {
                  $('#res_id').val(response.id);
                  $('.register-info').addClass('register-hide');
                  $('.register-info').removeClass('register-show');
                  $('.register-picture').addClass('register-show');
               } else {
                  $this.html($this.data('original-text'));
                  $("p.err-msg").html(response.message);
               }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               //Làm gì đó khi có lỗi xảy ra
               console.log('The following error occured: ' + textStatus, errorThrown);
            }
         })
      }
   });

   $('#load-user').on('click', '.list_image', function () {
      var album_id = $(this).data('id');
      console.log(album_id);
      $.ajax({
         type: 'post',
         dataType: 'json',
         url: url_admin_ajax,
         data: {
            action: 'submitImage',
            album_id: album_id
         },
         context: this,
         beforeSend: function () {
            //Làm gì đó trước khi gửi dữ liệu vào xử lý
         },
         success: function (response) {
            //Làm gì đó khi dữ liệu đã được xử lý
            if (response.success) {
               $('#model-list-image').html(response.html);
               $('.owl-carousel').owlCarousel({
                  loop: true,
                  margin: 0,
                  responsiveClass: true,
                  responsive: {
                     0: {
                        items: 1,
                        nav: true
                     },
                     600: {
                        items: 1,
                        nav: true
                     },
                     1000: {
                        items: 1,
                        nav: true,
                        loop: false,
                        margin: 0
                     }
                  }
               })
            } else {
               $("p.err-msg").html(response.message);
            }
         },
         error: function (jqXHR, textStatus, errorThrown) {
            //Làm gì đó khi có lỗi xảy ra
            console.log('The following error occured: ' + textStatus, errorThrown);
         }
      })
   });

   $('.paginated_link').click(function () {
      var paginate_id = $(this).data('paginate');
      $.ajax({
         type: 'post',
         dataType: 'json',
         url: url_admin_ajax,
         data: {
            action: 'paginateInfo',
            paginate_id: paginate_id
         },
         context: this,
         beforeSend: function () {
            //Làm gì đó trước khi gửi dữ liệu vào xử lý
         },
         success: function (response) {
            //Làm gì đó khi dữ liệu đã được xử lý
            if (response.success) {
               $(".library-story-slideshow").slick("unslick");
               $('#load-user').html(response.html);
               $('.library-story-slideshow').slick({
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  prevArrow: "<img class='a-left control-c prev slick-prev' src='http://dev.morinaga.vn/wp-content/themes/morinaga/images/mori/prev-library-story.png'>",
                  nextArrow: "<img class='a-right control-c next slick-next' src='http://dev.morinaga.vn/wp-content/themes/morinaga/images/mori/next-library-story.png'>",
               });
            } else {
               $this.html($this.data('original-text'));
               $("p.err-msg").html(response.message);
            }
         },
         error: function (jqXHR, textStatus, errorThrown) {
            //Làm gì đó khi có lỗi xảy ra
            console.log('The following error occured: ' + textStatus, errorThrown);
         }
      })
   });

   $('.btn-next-prev').click(function() {
      var current_quest = $(this).attr( "data-quest" );
      var index = parseInt(current_quest);
      let slickObj = $(".quiz-slideshow").slick('getSlick');
      slickObj.slickGoTo(index);
   });

   function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
   }

   function validatePhone(phone) {
      var re = /^[0]{1}[0-9]{9,10}$/;
      return re.test(phone);
   }

   function formatDate(date) {
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var seconds = date.getSeconds();
      var strTime = hours + ':' + minutes + ':' + seconds;
      return date.getDate() + "-" + date.getMonth() + 1 + "-" + date.getFullYear() + "  " + strTime;
   }

   function progress(timeleft, timetotal, $element) {
      let progressBarWidth = timeleft / timetotal * $element.width();
      let days = parseInt(timeleft / 86400);
      let hoursLeft = parseInt(timeleft - days * 86400);
      let hours = parseInt(hoursLeft / 3600);
      let minutesLeft = parseInt(hoursLeft - hours * 3600);
      let minutes = parseInt(minutesLeft / 60);
      let seconds = parseInt(timeleft % 60);
      $element
         .find("div")
         .animate(
            { width: progressBarWidth },
            timeleft == timetotal ? 0 : 1000,
            "linear"
         ).html(
            `${minutes} minutes, ${seconds} seconds`
         );
      if (timeleft >= 0 && !stop_){
         setTimeout(() => progress(timeleft - 1, timetotal, $element), 1000);
         $('#finish_time').val(timeleft);
      }  else {
         $('.quiz-test #btn_finish').trigger('click');
      }
         
   }
});
