$(window).on('beforeunload', function() {
   $(window).scrollTop(0);
});

$(function () {
   var controller = new ScrollMagic.Controller();
   var winWidth = window.innerWidth;
   var prevWidth = winWidth;
   var winHeight = window.innerHeight;
   var s2Height      = $('#about').offset().top - winHeight/2;
   var aboutHeight   = $('#about').height();
   var s3Height      = $('#thu-vien-bai-thi').offset().top - winHeight/2;
   var productHeight = $('#thu-vien-bai-thi').height();
   //var s4Height      = $('#x6_cups').offset().top - winHeight/2;
   var s5Height      = $('#thong-tin-giai-thuong').offset().top - winHeight/2;
   var s2AnmActive = false;
   var s3AnmActive = false;
   var s4AnmActive = false;
   var s5AnmActive = false;
   var scrollHeight = winHeight/2;
   var scroll = $(window).scrollTop();
   var isDetailSlick = false;

   var listSlideSlickOptions = {
      slidesToShow: 3,
      arrows: true,
      slidesToScroll: 3,
      dots: true,
      responsive: [{ 
         breakpoint: 500,
         settings: {
         arrows: true,
         infinite: false,
         slidesToShow: 2,
         slidesToScroll: 2,
         } 
      }],
      dotsClass: 'library_slick-dots',
     // asNavFor: '.list-picture-popup',
      prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>'
   }
   // set 2 row
   if (winWidth >= 768) {
      var x =  $('.list-story').children();
      for (i = 0; i < x.length ; i += 2) {
         x.slice(i,i+2).wrapAll('<li class="'+ i +'"></div>');
      }
      var y =  $('.list-picture').children();
      for (i = 0; i < y.length ; i += 2) {
         y.slice(i,i+2).wrapAll('<li class="'+ i +'"></div>');
      }
   }
   // slide for detail TPTB
  /* $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      //asNavFor: '.list-slide',
      prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>'
   });
*/
   var detailSlideSlickOptions = {
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      dots: false,
      //asNavFor: '.list-slide',
      prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>'
   }

   //if (winWidth >= 768) {
      $('.list-slide').slick(listSlideSlickOptions);
   //}

   //library-story-slideshow
   $('.library-story-slideshow').slick({
      slidesToShow: 3,
      slidesToScroll: 3,
      prevArrow:"<img class='a-left control-c prev slick-prev' src='http://chienbinh3.morinagamilk.com.vn/wp-content/themes/morinaga/images/mori/prev-library-story.png'>",
      nextArrow:"<img class='a-right control-c next slick-next' src='http://chienbinh3.morinagamilk.com.vn/wp-content/themes/morinaga/images/mori/next-library-story.png'>",
      responsive: [{ 
         breakpoint: 500,
         settings: {
         arrows: true,
         infinite: false,
         slidesToShow: 1,
         slidesToScroll: 1,
         } 
      },
      {breakpoint: 991,
         settings: {
         arrows: true,
         infinite: false,
         slidesToShow: 1,
         slidesToScroll: 1,
         }}],
   });

    //library-story-slideshow
   $('.quiz-slideshow').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: $('#next'),
      prevArrow: $('#btn_prev'),
   });


   if ($('.banner-list li').length > 1) {
      $('.banner-list').slick({
         arrows: false,
         dots: true
      });
   }

   function s2AnmPlay() {
      var tlS2Anm = new TimelineMax();
      tlS2Anm.to('#about', 2, {
         alpha: 1
      });
   }
 
   function s3AnmPlay() {
      var tlS3Anm = new TimelineMax();
      tlS3Anm.to('#thu-vien-bai-thi', 1.5, {
         alpha: 1
      });
   }
   function s4AnmPlay() {
      var tlS4Anm = new TimelineMax();
      tlS4Anm.to('#x6_cups', 1.5, {
         alpha: 1
      });
   }
   function s5AnmPlay() {
      var tlS5Anm = new TimelineMax();
      tlS5Anm.to('#thong-tin-giai-thuong', 1.5, {
         alpha: 1
      });
   }

   function checkActiveSection() {
      if (scroll >= s5Height) {
         $('.main-nav a').removeClass('active');
         $('.main-nav a[data-href="#thong-tin-giai-thuong"]').addClass('active');
      }
      /* else if (scroll >= s4Height) {
         $('.main-nav a').removeClass('active');
         $('.main-nav a[data-href="#x6_cups"]').addClass('active');
      } */
      else if (scroll >= s3Height) {
         $('.main-nav a').removeClass('active');
         $('.main-nav a[data-href="#thu-vien-bai-thi"]').addClass('active');
      }
      else {
         $('.main-nav a').removeClass('active');
         $('.main-nav a[data-href="#banner"]').addClass('active');
      }

      if (scroll >= scrollHeight) $('#scrollTop').fadeIn(300);
      else $('#scrollTop').fadeOut(300);
   }

   function checkProductsSlick() {
      if (prevWidth < winWidth && winWidth >= 768) {
         $('.list-slide').slick(listSlideSlickOptions);
      }
      else if (prevWidth > winWidth && winWidth < 768) {
         $('.list-slide').slick('unslick');
      }
   }

   //checkActiveSection();
   //checkProductsSlick();
   
   $(window).on('resize', function() {
      prevWidth = winWidth;
      winWidth = window.innerWidth;
      winHeight = window.innerHeight;
      s2Height = $('#about').offset().top - winHeight/2;
      aboutHeight = $('#about').height();
      s3Height = $('#thu-vien-bai-thi').offset().top - winHeight/2;
      productHeight = $('#thu-vien-bai-thi').height();
     // s4Height = $('#x6_cups').offset().top - winHeight/2;
      s5Height = $('#thong-tin-giai-thuong').offset().top - winHeight/2;
      scrollHeight = winHeight/2;
      scroll = $(window).scrollTop();
      //checkActiveSection();
      //checkProductsSlick();
     // sceneBanner.duration(aboutHeight * 1.5);
      //sceneProduct.duration(productHeight * 1.5);
   });

   var firstScroll = true;
   $(window).on('scroll', function() {
      if (firstScroll) {
         firstScroll = false;
         s2Height = $('#about').offset().top - winHeight/2;
         s3Height = $('#thu-vien-bai-thi').offset().top - winHeight/2;
        // s4Height = $('#x6_cups').offset().top - winHeight/2;
         s5Height = $('#thong-tin-giai-thuong').offset().top - winHeight/2;
         scrollHeight = winHeight/2;
      }
      scroll = $(window).scrollTop();
      checkActiveSection();
      
      if (scroll >= s5Height && !s5AnmActive) {
         s5AnmActive = true;
         s3AnmPlay();
         s5AnmPlay();
      }
      /* else if (scroll >= s4Height && !s4AnmActive) {
         s4AnmActive = true;
         s4AnmPlay();
      }*/
      else if (scroll >= s3Height && !s3AnmActive) {
         s3AnmActive = true;
         s3AnmPlay();
      }
      else if (scroll >= s2Height && !s2AnmActive) {
         s2AnmActive = true;
         s2AnmPlay();
      }
   });

   $('.cat-nav').on('click', 'li[data-href]', function() {
      var href = $(this).data('href');
      if ($(href).length > 0) {
         if(href == "#cat_ta_dan"){
            $('.tab-kien-thuc').removeClass('active');
            $('.tab-trung-bay').addClass('active');
            $('#floating_btns').show();
            //$('#library-story-slideshow').slick('slickGoTo', index);
            //$('#library-story-slideshow').slick('setPosition'); 
            $('.paginated_link').first().trigger('click');
         }
         else{
            $('.tab-trung-bay').removeClass('active');
            $('.tab-kien-thuc').addClass('active');
            $('#floating_btns').hide();
         }
         //$('.cat-nav li').removeClass('active');
         //$(this).addClass('active');
         $('.cat-list .cat-item, .cat-list .item-detail').hide();
         if(href=='#thong-tin-the-le'){
            href = '#about';
         } else if(href=='#giai-thuong'){
            href = '#thong-tin-giai-thuong';
         } else if(href=='#trang-chu'){
            href = '#banner';
         } 
         $(href).fadeIn(300);
         $('.cat-list .item-list').fadeIn(300);
         $('.list-slide').slick('unslick');
         $('.list-slide').slick(listSlideSlickOptions);
         /*if (winWidth >= 768) {
            $('.list-slide').slick('unslick');
            $('.list-slide').slick(listSlideSlickOptions);
         }
         else {
            $('body, html').stop().animate({
               scrollTop: $('#thu-vien-bai-thi').offset().top - 55
            }, 700, 'swing');
         }*/
      }
   });

   $('.cat-item').on('click', '.view-btn1', function() {
      var index = $(this).data('index');
      var slug = $(this).data('slug');
      var $parent = $(this).parents('.cat-item');
      var $list = $parent.find('.item-list');
      var $detail = $parent.find('.item-detail[data-slug="' + slug + '"]');
      $list.hide();
      $detail.fadeIn(300);
      if (!isDetailSlick) {
         isDetailSlick = true;
         $('.detail-slide').slick(detailSlideSlickOptions);
         $('.detail-slide').slick('slickGoTo', index);
      }
      else {
         $('.detail-slide').slick('unslick');
         $('.detail-slide').slick(detailSlideSlickOptions);
         $('.detail-slide').slick('slickGoTo', index);
      }

      if (winWidth >= 1200) {
         $('body, html').stop().animate({
            scrollTop: $('#thu-vien-bai-thi').offset().top - 132
         }, 700, 'swing');
      }
      else if (winWidth >= 992) {
         $('body, html').stop().animate({
            scrollTop: $('#thu-vien-bai-thi').offset().top - 114
         }, 700, 'swing');
      }
      else if (winWidth >= 768) {
         $('body, html').stop().animate({
            scrollTop: $('#thu-vien-bai-thi').offset().top - 92
         }, 700, 'swing');
      }
      else {
         $('body, html').stop().animate({
            scrollTop: $('#thu-vien-bai-thi').offset().top - 55
         }, 700, 'swing');
      }
   });

   $('#header .nav-control').on('click', function() {
      $('body').toggleClass('nav-active');
   });

   $('.main-nav a').on('click', function() {
      var href = $(this).data('href');
      // set new url
      window.history.pushState({url: "" + href + ""}, '', href);
      if(href=='#thong-tin-the-le' || href=='#cat_ta_quan' || href=='#cat_ta_quan'){
         href = '#about';
      } else if(href=='#giai-thuong'){
         href = '#thong-tin-giai-thuong';
      } else if(href=='#trang-chu'){
         href = '#banner';
      } else if(href=='#thu-vien-bai-thi'){
         href = '#thu-vien-bai-thi';
      }  
      animationScroll(href);
   });
   /**
    * scroll animation by href
    */
   function animationScroll(href) {
      if ($(href).length > 0) {
         $('body').removeClass('nav-active');
         if (winWidth >= 1200) {
            $('body, html').stop().animate({
               scrollTop: $(href).offset().top - 192
            }, 700, 'swing');
         }
         else if (winWidth >= 992) {
            $('body, html').stop().animate({
               scrollTop: $(href).offset().top - 174
            }, 700, 'swing');
         }
         else if (winWidth >= 768) {
            $('body, html').stop().animate({
               scrollTop: $(href).offset().top - 152
            }, 700, 'swing');
         }
         else {
            $('body, html').stop().animate({
               scrollTop: $(href).offset().top - 145
            }, 700, 'swing');
         }
      }
      else {
         $(this).parents('.has-sub').toggleClass('expand');
      }
   }
   $('#scrollTop').on('click', function() {
      $('html, body').stop().animate({scrollTop : 0}, 800);
      return false;
   });

   var tlHome = new TimelineMax();
if (winWidth > 768) {
   tlHome.to('#header', 1, {
      alpha: 1
   }, .2)
   .from('#header .logo', .3, {
      scale: 1.8,
      alpha: 0,
      ease: Back.easeOut
   }, '-=.5')
   .from('.nav-control', .5, {
      opacity: 0
   }, '-=.1')
   .staggerFrom('.main-nav li:not(.socials)', .3, {
      x: 20,
      alpha: 0,
      ease: Power1.easeOut
   }, .12, '-=.6')
   .from('.main-nav li.socials', .3, {
      alpha: 0
   })
   .to('#banner', 1.2, {
      alpha: 1
   }, '-=1')
   .to('.banner-products', 1, {
      alpha: 1
   }, '-=1')
   .fromTo('.banner-text', .4, {
      alpha: 0,
      scale: .4
   }, {
      alpha: 1,
      scale: 1,
      ease: Back.easeOut
   }, '-=.6')
   .to('.btn-wrap', 1.2, {
      alpha: 1
   }, '-=1')
   .fromTo('#floating_btns', 1, {
      yPercent: 15,
      alpha: 0
   }, {
      yPercent: 0,
      alpha: 1,
      ease: Power1.easeOut
   }, '-=.3');
} else {
   tlHome.to('#header', 1, {
      alpha: 1
   }, .2)
   .from('#header .logo', .3, {
      scale: 1.8,
      alpha: 0,
      ease: Back.easeOut
   }, '-=.5')
   .from('.nav-control', .5, {
      opacity: 0
   }, '-=.1')
   .staggerFrom('.main-nav li:not(.socials)', .3, {
      x: 20,
      alpha: 0,
      ease: Power1.easeOut
   }, .12, '-=.6')
   .from('.main-nav li.socials', .3, {
      alpha: 0
   })
   .to('#banner', 1.2, {
      alpha: 1
   }, '-=1')
   .to('.banner-products', 1, {
      alpha: 1
   }, '-=1')
   .fromTo('.banner-text', .4, {
      alpha: 0,
      scale: .4
   }, {
      alpha: 1,
      scale: 1,
      ease: Back.easeOut
   }, '-=.6')
   .fromTo('#floating_btns', 1, {
      yPercent: 15,
      alpha: 0
   }, {
      yPercent: 0,
      alpha: 1,
      ease: Power1.easeOut
   }, '-=.3')
   .to('#thu-vien-bai-thi', 1, {
      alpha: 1
   }, '-=1');
}
   $('.size-list a').on('click', function() {
      if (!$(this).hasClass('active')) {
         var size = $(this).data('size');
         var parent = $(this).parents('.detail-item');
         $(parent).find('[data-size]').removeClass('active');
         $(parent).find('[data-size="' + size + '"]').addClass('active');
      }
   });
   $('.new__button').on('click', function() {
      $(this).hide();
   });
   /**
    * link js
    */
   $(document).ready(function(){
      // your code here
      var url=window.location.href;
      var href;
      var param = url.split('#')[1];
      if ( typeof(param) !== "undefined" && param !== null ) {
         /*if(param == "thu-vien-bai-thi"){
            // scroll to 
            animationScroll('#thu-vien-bai-thi');
            // check active
         } else if (param == "giai-thuong") {
            // scroll to 
            animationScroll('#thong-tin-giai-thuong');
         }*/
         if(param=='thong-tin-the-le'){
            href = '#about';
         } else if(param=='giai-thuong'){
            href = '#thong-tin-giai-thuong';
         } else if(param=='trang-chu'){
            href = '#banner';
         } else if(param=='thu-vien-bai-thi'){
            href = '#thu-vien-bai-thi';
         } 
         animationScroll(href);
      }
   }, false);
   /**
    * js step register
    */
   $('#step_1_bt1n').on('click', function() {
      $('.register-info').addClass('register-hide');
      $('.register-picture').addClass('register-show');
   });
   $('#step_2_btn1').on('click', function() {
      $('.register-picture').removeClass('register-show');
      $('.register-picture').addClass('register-hide');
      $('.register-content').addClass('register-show');
   });
   $('#step_3_btn1').on('click', function() {
      $('.register-content').removeClass('register-show');
      $('.register-content').addClass('register-hide');
      $('.register-success').addClass('register-show');
   });
   $('#step_4_btn').on('click', function() {
      $('.register-content').removeClass('register-show');
      $('.register-content').addClass('register-hide');
      $('.register-picture').addClass('register-show');
   });
   $('.btn-join').on('click', function() {
      if (winWidth >= 1200) {
         $('body, html').stop().animate({
            scrollTop: $('#thu-vien-bai-thi').offset().top - 132
         }, 700, 'swing');
      }
      else if (winWidth >= 992) {
         $('body, html').stop().animate({
            scrollTop: $('#thu-vien-bai-thi').offset().top - 114
         }, 700, 'swing');
      }
      else if (winWidth >= 768) {
         $('body, html').stop().animate({
            scrollTop: $('#thu-vien-bai-thi').offset().top - 92
         }, 700, 'swing');
      }
      else {
         $('body, html').stop().animate({
            scrollTop: $('#thu-vien-bai-thi').offset().top - 55
         }, 700, 'swing');
      }
   });
   $('.view-detail-picture').on('click', function() {
      var src_ = $(this).data('img');
      var id_ = $(this).data('id');
      var list_img = src_.split(",");
      for (i = 0; i < list_img.length; i++) {
         $("#img_popup_detail_"+i).attr("src", list_img[i]);
      }
      var title_ = $('#title_picture_'+id_).html();
      $("#img_popup_title").html(title_);
   });
   $('.view-btn').on('click', function() {
      var id_ = $(this).data('id');
     // var html_ = $('#wrapper_content_'+id_).html();
      var title_ = $('#title_story_'+id_).html();
      var html_ = $('#content_full_wrapper_'+id_).html();
      $("#img_popup_story_title").html(title_);
      $("#content_popup_detail").html(html_);
   });
   
   $('.owl-carousel').owlCarousel({
      loop: true,
      margin: 0,
      responsiveClass: true,
      autoHeight : true,
      responsive: {
         0: {
         items: 1,
         nav: true
         },
         600: {
         items: 1,
         nav: true
         },
         1000: {
         items: 1,
         nav: true,
         loop: false,
         margin: 0
         }
         }
   })
});